use {
    crate::{operation::Operation, resource::Resource},
    petgraph::graph::NodeIndex,
};

pub type Index = NodeIndex;

pub(super) enum IndexOrWeight {
    Index(NodeIndex),
    Weight(Weight),
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Weight {
    Operation(Operation),
    Resource(Resource),
}

impl From<Operation> for Weight {
    #[inline]
    fn from(operation: Operation) -> Self {
        Self::Operation(operation)
    }
}

impl From<Resource> for Weight {
    #[inline]
    fn from(resource: Resource) -> Self {
        Self::Resource(resource)
    }
}
