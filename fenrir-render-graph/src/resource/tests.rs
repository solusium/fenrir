use {
    crate::{
        operation::Operation, resource::Resource, test::instances, tests::generate_resource_data,
    },
    enum_derived::Rand,
    fenrir_hal::{
        geometry::{Extent2D, Point2D, Rect2D},
        pipeline::BindPoint,
    },
    tokio::test,
};

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn multiple_writers() {
    for instance in instances("resource::remove_resource").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let resource = Resource::new(
                generate_resource_data(device.clone(), "resource::remove_resource").await,
                crate::resource::Metadata::new(None, None, None),
            )
            .await;

            let metadata = fenrir_hal::render_pass::Metadata::new(
                device,
                Vec::new(),
                Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0)),
                0,
                0,
                BindPoint::rand(),
            );

            let first_operation_result = Operation::new(metadata.clone()).await;

            assert!(first_operation_result.is_ok());

            let first_operation = first_operation_result.unwrap();

            assert!(first_operation.write(resource.clone()).await.is_ok());

            let second_operation_result = Operation::new(metadata).await;

            assert!(second_operation_result.is_ok());

            let second_operation = second_operation_result.unwrap();

            assert_ne!(first_operation, second_operation);

            let write_result = second_operation.write(resource.clone()).await;

            assert!(write_result.is_err());

            assert_eq!(
                resource.already_has_writer().await,
                write_result.unwrap_err()
            );
        }
    }
}
