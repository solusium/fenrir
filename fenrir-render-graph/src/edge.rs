use crate::node::Weight;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Edge {
    pub from: Weight,
    pub to: Weight,
}
