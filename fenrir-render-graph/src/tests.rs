use {
    crate::{
        edge::Edge,
        node::Weight,
        operation::tests::generate_operation,
        resource::{Data, Resource},
        test::instances,
        Graph,
    },
    fenrir_hal::texture::Texture,
    itertools::Itertools,
};

#[inline]
pub async fn generate_resource_data(device: fenrir_hal::device::Device, name: &str) -> Data {
    Data::Texture(generate_texture(device, name).await)
}

#[inline]
pub async fn generate_texture(device: fenrir_hal::device::Device, name: &str) -> Texture {
    let texture_result = device
        .clone()
        .new_texture(fenrir_hal::texture::Metadata::new(
            device,
            1,
            1,
            1,
            Vec::new(),
            name.to_string(),
        ))
        .await;

    assert!(texture_result.is_ok());

    texture_result.unwrap()
}

#[sleipnir::test]
async fn empty_graph(_: sleipnir::runtime::Handle) {
    let operations = Vec::new();
    let resources = Vec::new();

    let graph_result = Graph::new(&operations, &resources).await;

    assert!(graph_result.is_ok());

    let graph = graph_result.unwrap();

    assert_eq!(0, graph.nodes().count());
    assert!(graph.edges().is_empty());

    assert!(graph.iterations().is_empty());
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn simple_graph() {
    for instance in instances("graph::simple_graph").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operations = [generate_operation(device.clone()).await];

            let resource_metadata = crate::resource::Metadata::new(None, None, None);

            let resources = [
                Resource::new(
                    generate_resource_data(device.clone(), "graph::simple_graph - first resource")
                        .await,
                    resource_metadata.clone(),
                )
                .await,
                Resource::new(
                    generate_resource_data(device, "graph::simple_graph - second resource").await,
                    resource_metadata,
                )
                .await,
            ];

            operations[0].read(resources[0].clone()).await;

            assert!(operations[0].write(resources[1].clone()).await.is_ok());

            let graph_result = Graph::new(&operations, &resources).await;

            assert!(graph_result.is_ok());

            let graph = graph_result.unwrap();

            assert_eq!(
                &operations,
                &graph.operations().map(Clone::clone).collect_vec()[..]
            );
            assert_eq!(
                &resources,
                &graph.resources().map(Clone::clone).collect_vec()[..]
            );

            let edges = [
                Edge {
                    from: resources[0].clone().into(),
                    to: operations[0].clone().into(),
                },
                Edge {
                    from: operations[0].clone().into(),
                    to: resources[1].clone().into(),
                },
            ];

            assert_eq!(&edges, &graph.edges()[..]);

            let iterations = graph.iterations();

            assert_eq!(1, iterations.len());

            let iteration = &iterations[0];

            assert_eq!(1, iteration.len());

            assert!(if let Weight::Operation(operation) = &graph[iteration[0]] {
                operations[0] == *operation
            } else {
                false
            });
        }
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn cycle() {
    for instance in instances("graph::cycle").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operations = [generate_operation(device.clone()).await];

            let resources = [Resource::new(
                generate_resource_data(device, "graph::cycle").await,
                crate::resource::Metadata::new(None, None, None),
            )
            .await];

            operations[0].read(resources[0].clone()).await;

            assert!(operations[0].write(resources[0].clone()).await.is_ok());

            let graph_result = Graph::new(&operations, &resources).await;

            assert!(graph_result.is_err());
        }
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn multiple_iterations() {
    for instance in instances("graph::multiple_iterations").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operations = [
                generate_operation(device.clone()).await,
                generate_operation(device.clone()).await,
            ];

            let resources = [Resource::new(
                generate_resource_data(device, "graph::multiple_iterations").await,
                crate::resource::Metadata::new(None, None, None),
            )
            .await];

            assert!(operations[0].write(resources[0].clone()).await.is_ok());

            operations[1].read(resources[0].clone()).await;

            let graph_result = Graph::new(&operations, &resources).await;

            assert!(graph_result.is_ok());

            let graph = graph_result.unwrap();

            assert_eq!(2, graph.iterations().len());
        }
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn multiple_operations_one_iteration() {
    for instance in instances("graph::multiple_operations_one_iteration").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operations = [
                generate_operation(device.clone()).await,
                generate_operation(device.clone()).await,
            ];

            let metadata = crate::resource::Metadata::new(None, None, None);

            let resources = [
                Resource::new(
                    generate_resource_data(
                        device.clone(),
                        "graph::multiple_operations_one_iteration - first resource",
                    )
                    .await,
                    metadata.clone(),
                )
                .await,
                Resource::new(
                    generate_resource_data(
                        device.clone(),
                        "graph::multiple_operations_one_iteration - second resource",
                    )
                    .await,
                    metadata.clone(),
                )
                .await,
                Resource::new(
                    generate_resource_data(
                        device,
                        "graph::multiple_operations_one_iteration - third resource",
                    )
                    .await,
                    metadata,
                )
                .await,
            ];

            operations[0].read(resources[0].clone()).await;

            assert!(operations[0].write(resources[1].clone()).await.is_ok());
            assert!(operations[1].write(resources[2].clone()).await.is_ok());

            let graph_result = Graph::new(&operations, &resources).await;

            assert!(graph_result.is_ok());

            let graph = graph_result.unwrap();

            assert_eq!(1, graph.iterations().len());
        }
    }
}
