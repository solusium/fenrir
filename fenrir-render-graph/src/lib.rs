#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate,
    clippy::type_complexity
)]

use {
    crate::{
        edge::Edge,
        node::{Index, Weight},
    },
    alloc::{format, sync::Arc, vec::Vec},
    async_lock::RwLock,
    fenrir_error::{invalid_argument, operation_cancelled, state_not_recoverable, Error},
    futures::{future::join_all, stream, StreamExt, TryStreamExt},
    itertools::concat,
    node::IndexOrWeight,
    operation::Operation,
    petgraph::{
        algo::{is_cyclic_directed, is_cyclic_undirected},
        graph::DiGraph,
        visit::EdgeRef,
        Graph as PetGraph,
    },
    resource::Resource,
    sleipnir::stream::{MoreTryStreamExt, TryForEachParallelError},
    smallvec::{smallvec, SmallVec},
};

extern crate alloc;

pub mod operation;
pub mod resource;

mod edge;
mod node;

#[cfg(test)]
pub(crate) mod tests;

#[derive(Clone, Default)]
pub struct Graph {
    graph: Arc<PetGraph<Weight, ()>>,
    iterations: Vec<Vec<Index>>,
}

impl Graph {
    /// Construct a new render graph from operations and resources.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if the resulting graph
    /// would contain a cycle or if the graph does not exhibit bipartitenes between operations and
    /// resources.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new(operations: &[Operation], resources: &[Resource]) -> Result<Self, Error> {
        let operation_weights = operations
            .iter()
            .map(|operation| Weight::Operation(operation.clone()))
            .collect::<Vec<Weight>>();

        let resource_weights = resources
            .iter()
            .map(|resource| Weight::Resource(resource.clone()))
            .collect::<Vec<Weight>>();

        let graph_arc = Arc::new(RwLock::new(DiGraph::<Weight, ()>::new()));

        let graph_ref = &graph_arc;

        stream::iter(concat([operation_weights, resource_weights]).into_iter())
            .for_each(|weight| async move {
                let _ = graph_ref.clone().write().await.add_node(weight);
            })
            .await;

        {
            let result_futures = stream::iter(graph_arc.read().await.node_indices())
                .map(|node_index| async move {
                    let graph_arc = graph_ref.clone();

                    let node = graph_arc.read().await[node_index].clone();

                    match node {
                        Weight::Operation(operation) => {
                            Self::add_edges_to_operation(&graph_arc, operation, node_index).await
                        }
                        Weight::Resource(resource) => {
                            Self::add_edges_to_resource(&graph_arc, resource, node_index).await
                        }
                    }
                })
                .collect::<Vec<_>>()
                .await;

            join_all(result_futures)
                .await
                .into_iter()
                .try_fold((), |(), next| next)?;
        }

        let graph = Arc::new(graph_arc.read().await.clone());

        let iterations = Self::build_iterations(&graph).await?;

        Ok(Self { graph, iterations })
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn add_edges_to_operation(
        graph: &Arc<RwLock<PetGraph<Weight, ()>>>,
        operation: Operation,
        index: Index,
    ) -> Result<(), Error> {
        enum ResourceType {
            Read,
            Written,
        }

        assert!(match &graph.read().await[index] {
            Weight::Operation(operation_) => operation == *operation_,
            Weight::Resource(_) => false,
        });

        let result_futures = stream::iter([ResourceType::Read, ResourceType::Written])
            .zip(stream::iter([
                operation.read_resources().await,
                operation.written_resources().await,
            ]))
            .map(|(resource_type, resources)| async move {
                let result_futures_ = stream::iter(resources.into_iter())
                    .map(|resource| {
                        let (from, to) = match resource_type {
                            ResourceType::Read => (
                                IndexOrWeight::Weight(Weight::Resource(resource)),
                                IndexOrWeight::Index(index),
                            ),
                            ResourceType::Written => (
                                IndexOrWeight::Index(index),
                                IndexOrWeight::Weight(Weight::Resource(resource)),
                            ),
                        };

                        Self::maybe_add_edge(graph.clone(), from, to)
                    })
                    .collect::<SmallVec<[_; 3]>>()
                    .await;

                join_all(result_futures_)
                    .await
                    .into_iter()
                    .try_fold((), |(), next| next)
            })
            .collect::<SmallVec<[_; 2]>>()
            .await;

        let results = join_all(result_futures).await;

        results.into_iter().try_fold((), |(), next| next)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn add_edges_to_resource(
        graph: &Arc<RwLock<PetGraph<Weight, ()>>>,
        resource: Resource,
        index: Index,
    ) -> Result<(), Error> {
        enum OperationType {
            Writer,
            Reader,
        }

        assert!(match &graph.read().await[index] {
            Weight::Operation(_) => false,
            Weight::Resource(resource_) => resource == *resource_,
        });

        let writer =
            (resource.writer().await).map_or_else(SmallVec::new, |operation| smallvec![operation]);

        let result_futures = stream::iter([OperationType::Writer, OperationType::Reader])
            .zip(stream::iter([writer, resource.readers().await]))
            .map(|(operation_type, operations)| async move {
                let result_futures_ = stream::iter(operations.into_iter())
                    .map(|operation| {
                        let (from, to) = match operation_type {
                            OperationType::Reader => (
                                IndexOrWeight::Index(index),
                                IndexOrWeight::Weight(Weight::Operation(operation)),
                            ),
                            OperationType::Writer => (
                                IndexOrWeight::Weight(Weight::Operation(operation)),
                                IndexOrWeight::Index(index),
                            ),
                        };

                        Self::maybe_add_edge(graph.clone(), from, to)
                    })
                    .collect::<SmallVec<[_; 3]>>()
                    .await;

                join_all(result_futures_)
                    .await
                    .into_iter()
                    .try_fold((), |(), next| next)
            })
            .collect::<SmallVec<[_; 2]>>()
            .await;

        let results = join_all(result_futures).await;

        results.into_iter().try_fold((), |(), next| next)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn build_iterations<'a, 'b>(
        graph: &PetGraph<Weight, ()>,
    ) -> Result<Vec<Vec<Index>>, Error> {
        if is_cyclic_directed(graph) || is_cyclic_undirected(graph) {
            return Err(invalid_argument(&format!(
                "graph contains a cycle: {graph:?}",
            )));
        }

        let graph_ref = &graph;

        let mut iterations = Vec::new();

        let iterations_ref = &mut iterations;

        loop {
            let next_iteration = Self::get_next_iteration(graph_ref, iterations_ref).await;

            if next_iteration.is_empty() {
                break;
            }

            iterations_ref.push(next_iteration);
        }

        Ok(iterations)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn check_and_maybe_add_edge(
        graph: &Arc<RwLock<PetGraph<Weight, ()>>>,
        from_weight: Weight,
        from_index: Index,
        to_weight: Weight,
        to_index: Index,
    ) -> Result<(), Error> {
        match from_weight {
            Weight::Operation(from_operation) => {
                if let Weight::Operation(to_operation) = to_weight {
                    Err(invalid_argument(&format!(
                        "cannot add edge from {from_operation:?} to 
                        {to_operation:?}: no edge between two operations allowed"
                    )))
                } else {
                    Ok(())
                }
            }
            Weight::Resource(from_resource) => {
                if let Weight::Resource(to_resource) = to_weight {
                    Err(invalid_argument(&format!(
                        "cannot add edge from {from_resource:?} to 
                        {to_resource:?}: no edge between two resources allowed"
                    )))
                } else {
                    Ok(())
                }
            }
        }?;

        {
            let mut graph_guard = graph.write().await;

            if !graph_guard.contains_edge(from_index, to_index) {
                let _ = graph_guard.add_edge(from_index, to_index, ());
            }
        }

        Ok(())
    }

    #[inline]
    #[must_use]
    pub fn edges(&self) -> Vec<Edge> {
        let graph = &self.graph;

        graph
            .edge_references()
            .map(|edge| {
                let from = graph[edge.source()].clone();

                {
                    let to = graph[edge.target()].clone();

                    Edge { from, to }
                }
            })
            .collect()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_infos(
        graph: &Arc<RwLock<PetGraph<Weight, ()>>>,
        index_or_weight: IndexOrWeight,
    ) -> (Weight, Index) {
        match index_or_weight {
            IndexOrWeight::Index(to_index) => {
                let to_weight = graph.read().await[to_index].clone();

                (to_weight, to_index)
            }
            IndexOrWeight::Weight(to_weight) => {
                let maybe_to_index = async {
                    let graph_guard = graph.read().await;

                    graph_guard
                        .node_indices()
                        .find(|index| to_weight == graph_guard[*index])
                };

                maybe_to_index
                    .await
                    .map_or_else(|| unreachable!(), |to_index| (to_weight, to_index))
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_next_iteration(
        graph: &PetGraph<Weight, ()>,
        iterations: &[Vec<Index>],
    ) -> Vec<Index> {
        stream::iter(graph.node_indices())
            .filter_map(|index| {
                Self::is_unincluded_operation_and_writers_of_read_resources_are_included(
                    index, graph, iterations,
                )
            })
            .collect()
            .await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn is_unincluded_operation_and_writers_of_read_resources_are_included<'a, 'b>(
        index: Index,
        graph: &'a PetGraph<Weight, ()>,
        iterations: &'a [Vec<Index>],
    ) -> Option<Index> {
        match &graph[index] {
            Weight::Resource(_) => None,
            Weight::Operation(operation) => {
                let already_included = iterations
                    .iter()
                    .any(|iteration| iteration.iter().any(|index_| index == *index_));

                if already_included {
                    None
                } else {
                    let all_writer_are_included_in_iteraions =
                        stream::iter(operation.read_resources().await.into_iter())
                            .all(|read_resource| {
                                Self::writer_already_included_in_iterations(
                                    read_resource,
                                    graph,
                                    iterations,
                                )
                            })
                            .await;

                    if !all_writer_are_included_in_iteraions {
                        None
                    } else {
                        Some(index)
                    }
                }
            }
        }
    }

    #[inline]
    #[must_use]
    pub fn iterations(&self) -> &[Vec<Index>] {
        &self.iterations
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn maybe_add_edge(
        graph: Arc<RwLock<PetGraph<Weight, ()>>>,
        from: IndexOrWeight,
        to: IndexOrWeight,
    ) -> Result<(), Error> {
        let (from_weight, from_index) = Self::get_infos(&graph, from).await;

        let (to_weight, to_index) = Self::get_infos(&graph, to).await;

        Self::check_and_maybe_add_edge(&graph, from_weight, from_index, to_weight, to_index).await
    }

    #[inline]
    pub fn nodes(&self) -> impl Iterator<Item = &Weight> {
        self.graph.node_weights()
    }

    #[inline]
    pub fn operations(&self) -> impl Iterator<Item = &Operation> {
        self.graph.node_weights().filter_map(|weight| match weight {
            Weight::Operation(operation) => Some(operation),
            Weight::Resource(_) => None,
        })
    }

    /// Execute this graph.
    ///
    /// # Errors
    ///
    /// This function will return an error if an operation of the graph fails to execute.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn render(&self) -> Result<(), Error> {
        let graph = &self.graph;

        stream::iter(self.iterations.iter())
            .map(Ok)
            .try_for_each(|iteration| async move {
                let iteration_result = stream::iter(iteration.clone())
                    .map(|index| Ok((graph.clone(), index)))
                    .try_for_each_parallel(None, |(graph_, index)| async move {
                        match &graph_[index] {
                            Weight::Resource(_) => unreachable!(
                                "Graph::iterations should be a vector of vectors of indices to 
                                operations inside the graph"
                            ),
                            Weight::Operation(operation) => operation.render().await,
                        }
                    })
                    .await;

                match iteration_result {
                    Ok(value) => Ok(value),
                    Err(error) => match error {
                        TryForEachParallelError::Error(error_) => Err(error_),
                        TryForEachParallelError::TaskError(error_) => match error_ {
                            sleipnir::Error::Cancelled => Err(operation_cancelled()),
                            sleipnir::Error::TaskPanic => {
                                Err(state_not_recoverable("a task panicked"))
                            }
                            _ => unreachable!("unexpected error"),
                        },
                    },
                }
            })
            .await
    }

    #[inline]
    pub fn resources(&self) -> impl Iterator<Item = &Resource> {
        self.graph.node_weights().filter_map(|weight| match weight {
            Weight::Operation(_) => None,
            Weight::Resource(resource) => Some(resource),
        })
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn writer_already_included_in_iterations<'a>(
        resource: Resource,
        graph: &'a PetGraph<Weight, ()>,
        iterations: &'a [Vec<Index>],
    ) -> bool {
        (resource.writer().await).map_or(true, |writer| {
            let maybe_writer_index = graph.node_indices().find(|index_| match &graph[*index_] {
                Weight::Resource(_) => false,
                Weight::Operation(operation_) => writer == *operation_,
            });

            maybe_writer_index.map_or_else(
                || {
                    unreachable!(
                    "it should not be possible to have an operation which isn't part of the graph"
                )
                },
                |writer_index| {
                    let already_included_writer = iterations
                        .iter()
                        .any(|iteration| iteration.iter().any(|index_| writer_index == *index_));

                    already_included_writer
                },
            )
        })
    }
}

impl core::ops::Index<Index> for Graph {
    type Output = Weight;

    #[inline]
    fn index(&self, index: Index) -> &Self::Output {
        self.graph.index(index)
    }
}

#[cfg(any(test, feature = "test"))]
pub mod test {
    use {
        async_lock::OnceCell,
        fenrir_error::{initialization_failed, Error},
        fenrir_hal::{instance::Metadata, Instance},
        futures::{future::join_all, stream, StreamExt},
        itertools::Itertools,
        regex::Regex,
        sleipnir::ffi::runtime::Handle,
        std::{
            env::{current_exe, var},
            fs::read_dir,
            path::{Path, PathBuf},
            str::FromStr,
        },
        strum::IntoEnumIterator,
        tracing::{error, level_filters::LevelFilter},
        tracing_subscriber::fmt::format::FmtSpan,
    };

    #[inline]
    #[cfg(debug_assertions)]
    const fn mode() -> &'static str {
        "debug"
    }

    #[inline]
    #[cfg(not(debug_assertions))]
    const fn mode() -> &'static str {
        "release"
    }

    #[inline]
    #[allow(clippy::future_not_send)]
    pub(crate) async fn init_tracing() {
        static TRACING_SUBSCRIBER: OnceCell<()> = OnceCell::new();

        assert!(TRACING_SUBSCRIBER
            .get_or_try_init(|| async move {
                tracing_subscriber::fmt()
                    .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
                    .with_max_level(LevelFilter::TRACE)
                    .try_init()
            })
            .await
            .is_ok());
    }

    #[inline]
    fn load_shader_compiler(name: &str) -> fenrir_shader_compiler::Compiler {
        let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

        assert!(mainifest_dir_result.is_ok());

        let library_dir_result = PathBuf::from_str(&mainifest_dir_result.unwrap());

        assert!(library_dir_result.is_ok());

        let maybe_library_dir = library_dir_result.unwrap().parent().map(Path::to_path_buf);

        assert!(maybe_library_dir.is_some());

        let mut library_dir = maybe_library_dir.unwrap();

        library_dir.push("target");

        library_dir.push(mode());

        library_dir.push("deps");

        let maybe_entires = read_dir(&library_dir);

        assert!(maybe_entires.is_ok());

        println!("{maybe_entires:?}\n");

        let maybe_compiler = maybe_entires
            .unwrap()
            .filter_map(Result::ok)
            .filter(|entry| entry.file_name().to_string_lossy().contains(name))
            .filter_map(
                |entry| match fenrir_shader_compiler::Compiler::new(&entry.path()) {
                    Err(error) => {
                        error!("{entry:?}: {error}");
                        None
                    }
                    Ok(module) => Some(module),
                },
            )
            .find(|_| true);

        assert!(maybe_compiler.is_some());

        maybe_compiler.unwrap()
    }

    #[inline]
    pub(crate) fn get_allocator_manager(
        allocator_backend: &fenrir_hal::allocator::Backend,
        task_manager: &Handle,
    ) -> fenrir_hal::allocator::manager::Manager {
        let current_exe = current_exe().unwrap();

        let deps_dir = current_exe.parent().unwrap();

        let mut file_regex = "libfenrir_".to_string();

        file_regex.push_str(match allocator_backend {
            fenrir_hal::allocator::Backend::GPUAllocator => "gpu_allocator",
        });

        file_regex.push_str("(-[a-z0-9]+)?\\.so");

        let regex_result = Regex::new(&file_regex);

        assert!(regex_result.is_ok());

        let regex = regex_result.unwrap();

        let regex_ref = &regex;

        let maybe_allocator_manager = read_dir(deps_dir)
            .unwrap()
            .map(|entry_result| {
                let entry = entry_result.unwrap();

                entry.file_name().to_str().map_or_else(
                    || None,
                    |file_name| {
                        if regex_ref.is_match(file_name) {
                            let allocator_manager_result =
                                fenrir_hal::allocator::manager::Manager::new(
                                    &entry.path(),
                                    task_manager.clone(),
                                );

                            allocator_manager_result.ok()
                        } else {
                            None
                        }
                    },
                )
            })
            .fold(None, |result, next| result.map_or(next, Some));

        assert!(maybe_allocator_manager.is_some());

        maybe_allocator_manager.unwrap()
    }

    #[inline]
    async fn new_instance(
        info: (
            fenrir_hal::instance::Backend,
            fenrir_hal::allocator::Backend,
            &str,
            &str,
        ),
    ) -> Result<Instance, Error> {
        let (backend, allocator_backend, shader_compiler_backend, name) = info;

        let backend_str = match backend {
            fenrir_hal::instance::Backend::Ash => "ash",
        };

        let shader_compiler = load_shader_compiler(shader_compiler_backend).into();

        let metadata = Metadata {
            name: name.into(),
            major: 0,
            minor: 0,
            patch: 0,
            tracing_level: 0,
            shader_compiler,
        };

        let runtime = sleipnir::runtime::Handle::current().into();

        let allocator_manager = get_allocator_manager(&allocator_backend, &runtime);

        let current_exe = current_exe().unwrap();

        let deps_dir = current_exe.parent().unwrap();

        let mut file_regex = "libfenrir_".to_string();

        file_regex.push_str(backend_str);

        file_regex.push_str("(-[a-z0-9]+)?\\.so");

        let regex_result = Regex::new(&file_regex);

        assert!(regex_result.is_ok());

        let regex = regex_result.unwrap();

        let regex_ref = &regex;
        let allocator_manager_ref = &allocator_manager;
        let runtime_ref = &runtime;
        let metadata_ref = &metadata;

        #[allow(clippy::manual_try_fold)]
        stream::iter(read_dir(deps_dir).unwrap())
            .map(|entry_result| async move {
                let entry = entry_result.unwrap();

                entry.file_name().to_str().map_or_else(
                    || None,
                    |file_name| {
                        if !regex_ref.is_match(file_name) {
                            None
                        } else {
                            let backend_result = Instance::new(
                                &entry.path(),
                                metadata_ref.clone(),
                                runtime_ref.clone(),
                                allocator_manager_ref.clone(),
                            );

                            backend_result.ok()
                        }
                    },
                )
            })
            .fold(
                Err(initialization_failed("could not find backend")),
                |result, next| async move {
                    match result {
                        Ok(backend) => Ok(backend),
                        Err(code) => (next.await).map_or_else(|| Err(code), Ok),
                    }
                },
            )
            .await
    }

    #[inline]
    #[allow(clippy::missing_panics_doc)]
    pub async fn instances(name: &str) -> Vec<Instance> {
        join_all(
            stream::iter(
                std::iter::once(fenrir_hal::allocator::Backend::GPUAllocator)
                    .cartesian_product(fenrir_hal::instance::Backend::iter())
                    .cartesian_product(std::iter::once("naga")),
            )
            .map(|((allocator_backend, backend), shader_compiler_backend)| {
                (backend, allocator_backend, shader_compiler_backend, name)
            })
            .map(new_instance)
            .collect::<Vec<_>>()
            .await,
        )
        .await
        .into_iter()
        .map(|backend_result| {
            assert!(backend_result.is_ok());
            backend_result.unwrap()
        })
        .collect()
    }
}
