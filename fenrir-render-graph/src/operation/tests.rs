use {
    crate::{
        operation::Operation,
        resource::{ClearValue, Resource},
        test::{init_tracing, instances},
        tests::generate_resource_data,
    },
    enum_derived::Rand,
    fenrir_hal::{
        color::RGBA,
        device::Device,
        geometry::{Extent2D, Point2D, Rect2D},
        pipeline::BindPoint,
        render_pass,
    },
    futures::future::join_all,
    rand::{rng, Rng},
    tokio::test,
};

#[inline]
pub(crate) async fn generate_operation(device: Device) -> Operation {
    let metadata = render_pass::Metadata::new(
        device,
        Vec::new(),
        Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0)),
        0,
        0,
        BindPoint::rand(),
    );

    let operation_result = Operation::new(metadata).await;

    assert!(operation_result.is_ok());

    operation_result.unwrap()
}

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn compare() {
    init_tracing().await;

    let mut rng = rng();

    for instance in instances("operation::compare").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let device_ref = &device;

            let operations = join_all((2..rng.random_range(3..1024)).map(|_| async {
                let metadata = render_pass::Metadata::new(
                    device_ref.clone(),
                    Vec::new(),
                    Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0)),
                    0,
                    0,
                    BindPoint::Graphics,
                );

                let operation_result = Operation::new(metadata).await;

                assert!(operation_result.is_ok());

                operation_result.unwrap()
            }))
            .await;

            for operation in &operations {
                let operations_without_this = operations
                    .iter()
                    .filter(|current_iteration| *operation != **current_iteration);

                assert_eq!(operations.len() - 1, operations_without_this.count());
            }
        }
    }
}

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn add_read_resources() {
    let mut rng = rng();

    for instance in instances("operation::add_read_resources").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operation = generate_operation(device.clone()).await;

            {
                let first_clear_value: ClearValue = RGBA::new(
                    rng.random_range(0usize..255usize),
                    rng.random_range(0..255),
                    rng.random_range(0..255),
                    rng.random_range(0..255),
                )
                .into();

                let first_resource = Resource::new(
                    generate_resource_data(
                        device.clone(),
                        "operation::add_read_resources - first resource",
                    )
                    .await,
                    crate::resource::Metadata::new(None, None, Some(first_clear_value.clone())),
                )
                .await;

                operation.read(first_resource.clone()).await;

                assert_eq!(
                    &[first_resource.clone()],
                    &operation.read_resources().await[..]
                );

                {
                    let second_clear_value = {
                        || loop {
                            let clear_value: ClearValue = RGBA::new(
                                rng.random_range(0usize..255usize),
                                rng.random_range(0..255),
                                rng.random_range(0..255),
                                rng.random_range(0..255),
                            )
                            .into();

                            if first_clear_value != clear_value {
                                return clear_value;
                            }
                        }
                    }();

                    let second_resource = Resource::new(
                        generate_resource_data(
                            device,
                            "operation::add_read_resources - second resource",
                        )
                        .await,
                        crate::resource::Metadata::new(None, None, Some(second_clear_value)),
                    )
                    .await;

                    assert_ne!(first_resource, second_resource);

                    operation.read(second_resource.clone()).await;

                    assert_eq!(
                        &[first_resource.clone(), second_resource.clone()],
                        &operation.read_resources().await[..]
                    );
                }

                assert_eq!(&[first_resource], &operation.read_resources().await[..]);
            }

            assert!(operation.read_resources().await.is_empty());
        }
    }
}

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn write_resources() {
    for instance in instances("resource::write_resources").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let operation_result = Operation::new(fenrir_hal::render_pass::Metadata::new(
                device.clone(),
                Vec::new(),
                Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0)),
                0,
                0,
                BindPoint::rand(),
            ))
            .await;

            assert!(operation_result.is_ok());

            let operation = operation_result.unwrap();

            {
                let resource_metadata = crate::resource::Metadata::new(None, None, None);

                let first_resource = Resource::new(
                    generate_resource_data(
                        device.clone(),
                        "resource::write_resources - first resource",
                    )
                    .await,
                    resource_metadata.clone(),
                )
                .await;

                assert!(operation.write(first_resource.clone()).await.is_ok());

                assert_eq!(
                    &[first_resource.clone()],
                    &operation.written_resources().await[..]
                );

                {
                    let second_resource = Resource::new(
                        generate_resource_data(
                            device,
                            "resource::write_resources - second resource",
                        )
                        .await,
                        resource_metadata,
                    )
                    .await;

                    assert_ne!(first_resource, second_resource);

                    assert!(operation.write(second_resource.clone()).await.is_ok());

                    assert_eq!(
                        &[first_resource.clone(), second_resource],
                        &operation.written_resources().await[..]
                    );
                }

                assert_eq!(&[first_resource], &operation.written_resources().await[..]);
            }

            let empty_resource_array: [Resource; 0] = [];

            assert_eq!(
                &empty_resource_array,
                &operation.written_resources().await[..]
            );
        }
    }
}
