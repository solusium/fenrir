use {
    crate::{
        operation::stable_abi::{Trait, Trait_TO, WeakTrait, WeakTrait_TO},
        resource::Resource,
    },
    abi_stable::{
        sabi_trait,
        std_types::{
            RArc,
            ROption::{self, RNone, RSome},
            RResult, RSlice, RVec,
        },
        StableAbi,
    },
    alloc::sync::{Arc, Weak},
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::{RwLock, RwLockWriteGuard},
    core::{mem::take, ops::Range},
    fenrir_error::{argument_out_of_domain, state_not_recoverable, Error},
    fenrir_hal::{
        graphics::{pipeline::Metadata as GraphicsPipelineMetadata, Pipeline as GraphicsPipeline},
        render_pass::{Metadata, RenderPass},
    },
    fenrir_shader_compiler::Error as ErrorWrapper,
    futures::future::join_all,
    sleipnir::runtime::Handle,
    smallvec::SmallVec,
    uuid::Uuid,
};

extern crate alloc;

#[cfg(test)]
pub(crate) mod tests;

pub(crate) mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::resource::Resource,
        abi_stable::{
            sabi_trait,
            std_types::{RArc, ROption, RResult, RSlice, RVec},
        },
        async_ffi::BorrowingFfiFuture,
        core::clone::Clone,
        fenrir_error::Error,
        fenrir_hal::{
            graphics::{
                pipeline::Metadata as GraphicsPipelineMetadata, Pipeline as GraphicsPipeline,
            },
            render_pass::{Metadata, RenderPass},
        },
        fenrir_shader_compiler::Error as ErrorWrapper,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub(crate) trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn equal(&self, other: &Trait_TO<RArc<()>>) -> bool;

        fn equal_uuid(&self, uuid: [u8; 16]) -> bool;

        fn metadata(&self) -> BorrowingFfiFuture<'_, Metadata>;

        fn new_graphics_pipeline(
            &self,
            metadata: GraphicsPipelineMetadata<'static, 'static>,
        ) -> BorrowingFfiFuture<'_, RResult<GraphicsPipeline, ErrorWrapper>>;

        fn read(&self, resource: Resource) -> BorrowingFfiFuture<'_, ()>;

        fn read_multiple<'a>(
            &'a self,
            resources: RSlice<'a, Resource>,
        ) -> BorrowingFfiFuture<'a, ()>;

        fn read_resources(&self) -> BorrowingFfiFuture<'_, RVec<Resource>>;

        fn render(&self) -> BorrowingFfiFuture<'_, RResult<(), Error>>;

        fn render_pass(&self) -> RenderPass;

        fn remove_read_resource(&self, index: usize) -> BorrowingFfiFuture<'_, RResult<(), Error>>;

        fn remove_written_resource(
            &self,
            index: usize,
        ) -> BorrowingFfiFuture<'_, RResult<(), Error>>;

        fn weak(&self) -> BorrowingFfiFuture<'_, WeakTrait_TO<RArc<()>>>;

        fn written_resources(&self) -> BorrowingFfiFuture<'_, RVec<Resource>>;

        fn write(&self, resource: Resource) -> BorrowingFfiFuture<'_, RResult<(), Error>>;
    }

    #[sabi_trait]
    pub(crate) trait WeakTrait: Clone + Debug + Send + Sync + 'static {
        fn upgrade(&self) -> ROption<Trait_TO<RArc<()>>>;
    }
}

#[derive(Debug)]
pub(crate) struct Implementation {
    uuid: Uuid,
    render_pass: RenderPass,
    read_resources: Option<
        RwLock<
            SmallVec<
                [(
                    Option<crate::resource::stable_abi::WeakTrait_TO<RArc<()>>>,
                    usize,
                ); 3],
            >,
        >,
    >,
    written_resources:
        Option<RwLock<SmallVec<[Option<crate::resource::stable_abi::WeakTrait_TO<RArc<()>>>; 3]>>>,
    runtime: Handle,
    this: RwLock<Weak<Implementation>>,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Operation {
    state: Trait_TO<RArc<()>>,
}

#[derive(Debug)]
struct WeakImplementation {
    this: Weak<Implementation>,
}

impl Implementation {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop(
        read_resources: RwLock<
            SmallVec<
                [(
                    Option<crate::resource::stable_abi::WeakTrait_TO<RArc<()>>>,
                    usize,
                ); 3],
            >,
        >,
        written_resources: RwLock<
            SmallVec<[Option<crate::resource::stable_abi::WeakTrait_TO<RArc<()>>>; 3]>,
        >,
    ) {
        {
            let read_resources_guard = read_resources.read().await;

            let remove_read_resources_futures = || {
                read_resources_guard
                    .iter()
                    .map(|(maybe_weak_read_resource, index)| async move {
                        if let Some(weak_read_resource) = maybe_weak_read_resource.as_ref() {
                            if let RSome(read_resource) = weak_read_resource.upgrade() {
                                if read_resource.remove_reader(*index).await.is_err() {
                                    unreachable!();
                                }
                            }
                        }
                    })
                    .collect::<SmallVec<[_; 6]>>()
            };

            join_all(remove_read_resources_futures()).await;
        }

        {
            let written_resources_guard = written_resources.read().await;

            let remove_write_resources_futures = || {
                written_resources_guard
                    .iter()
                    .filter_map(Option::as_ref)
                    .map(|weak_written_resource| async move {
                        if let RSome(written_resource) = weak_written_resource.upgrade() {
                            if written_resource.remove_writer().await.is_err() {
                                unreachable!();
                            }
                        }
                    })
                    .collect::<SmallVec<[_; 3]>>()
            };

            join_all(remove_write_resources_futures()).await;
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_graphics_pipeline(
        &self,
        metadata: GraphicsPipelineMetadata<'static, 'static>,
    ) -> Result<GraphicsPipeline, ErrorWrapper> {
        self.render_pass.new_graphics_pipeline(metadata).await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn read(&self, resource: Resource) {
        match self.read_resources.as_ref() {
            None => unreachable!(),
            Some(reads) => {
                let mut read_resources = reads.write().await;

                self.read_impl(&resource, &mut read_resources).await;
            }
        };
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn read_impl(
        &self,
        resource: &Resource,
        read_resources: &mut RwLockWriteGuard<
            '_,
            SmallVec<
                [(
                    Option<crate::resource::stable_abi::WeakTrait_TO<RArc<()>>>,
                    usize,
                ); 3],
            >,
        >,
    ) {
        let weak_read_resource = Some(resource.weak_state().await);

        {
            let enumerated_empty_read_resources_iterator = read_resources
                .iter_mut()
                .enumerate()
                .find(|(_, (maybe_weak_read_resource, _))| maybe_weak_read_resource.is_none());

            let (read_index, reader_index) =
                if let Some((reader_index_, (maybe_weak_read_resource, read_index_))) =
                    enumerated_empty_read_resources_iterator
                {
                    *maybe_weak_read_resource = weak_read_resource;

                    (read_index_, reader_index_)
                } else {
                    let reader_index_ = read_resources.len();

                    {
                        read_resources.push((weak_read_resource, 0));

                        let (_, read_index_) = &mut read_resources[reader_index_];

                        (read_index_, reader_index_)
                    }
                };

            *read_index = resource.add_reader(self.weak().await, reader_index).await;
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn read_multiple(&self, resources: &[Resource]) {
        match self.read_resources.as_ref() {
            None => unreachable!(),
            Some(reads) => {
                let mut read_resources = reads.write().await;

                for resource in resources {
                    self.read_impl(resource, &mut read_resources).await;
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn read_resources(&self) -> Vec<Resource> {
        match self.read_resources.as_ref() {
            None => unreachable!(),
            Some(read_resources_lock) => {
                let read_resources = read_resources_lock.read().await;

                read_resources
                    .iter()
                    .map(|(maybe_weak_read_resource, _)| maybe_weak_read_resource)
                    .filter_map(Option::as_ref)
                    .filter_map(|weak_read_resource| {
                        Option::<crate::resource::stable_abi::Trait_TO<RArc<()>>>::from(
                            weak_read_resource.upgrade(),
                        )
                    })
                    .map(Resource::from)
                    .collect()
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn render(&self) -> Result<(), Error> {
        self.render_pass.render().await
    }

    #[inline]
    fn read_resources_member_is_none(&self) -> String {
        format!(
            "the read resources member of operation {self:?} is None while it shouldn't be until 
            dropped",
        )
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn remove_read_resource(&self, index: usize) -> Result<(), Error> {
        match self.read_resources.as_ref() {
            None => Err(state_not_recoverable(&self.read_resources_member_is_none())),
            Some(read_resources_lock) => {
                let mut read_resources_guard = read_resources_lock.write().await;

                if read_resources_guard.is_empty() {
                    drop(read_resources_guard);

                    Err(state_not_recoverable(&format!(
                        "attempting to remove a read resource from {self:?} which doesn't have any 
                        read resources"
                    )))
                } else if read_resources_guard.len() <= index {
                    Err(argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: read_resources_guard.len().into(),
                        },
                        index.into(),
                    ))
                } else {
                    if (index + 1) == read_resources_guard.len() {
                        read_resources_guard.remove(index);
                    } else {
                        read_resources_guard[index].0 = None;
                    }

                    drop(read_resources_guard);

                    Ok(())
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn remove_written_resource(&self, index: usize) -> Result<(), Error> {
        match self.written_resources.as_ref() {
            None => Err(state_not_recoverable(
                &self.written_resources_member_is_none(),
            )),
            Some(writtten_resources_lock) => {
                let mut written_resources_guard = writtten_resources_lock.write().await;

                if written_resources_guard.is_empty() {
                    drop(written_resources_guard);

                    Err(state_not_recoverable(&format!(
                        "attempting to remove a written resource from {self:?} which doesn't have
                        any read resources"
                    )))
                } else if written_resources_guard.len() <= index {
                    Err(argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: written_resources_guard.len().into(),
                        },
                        index.into(),
                    ))
                } else {
                    if (index + 1) == written_resources_guard.len() {
                        written_resources_guard.remove(index);
                    } else {
                        written_resources_guard[index] = None;
                    }

                    drop(written_resources_guard);

                    Ok(())
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn weak(&self) -> WeakTrait_TO<RArc<()>> {
        let this = self.this.read().await.clone();

        WeakTrait_TO::from_ptr(
            RArc::new(WeakImplementation { this }),
            sabi_trait::TD_Opaque,
        )
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn write(&self, resource: Resource) -> Result<(), Error> {
        let index = match self.written_resources.as_ref() {
            None => Err(state_not_recoverable(
                &self.written_resources_member_is_none(),
            )),
            Some(writes) => {
                let mut written_resources = writes.write().await;

                let weak_written_resource = Some(resource.weak_state().await);

                let enumerated_empty_written_resources_iterator = written_resources
                    .iter_mut()
                    .enumerate()
                    .find(|(_, maybe_weak_written_resource)| maybe_weak_written_resource.is_none());

                if let Some((index_, maybe_weak_written_resource)) =
                    enumerated_empty_written_resources_iterator
                {
                    *maybe_weak_written_resource = weak_written_resource;

                    drop(written_resources);

                    Ok(index_)
                } else {
                    let index_ = written_resources.len();

                    written_resources.push(weak_written_resource);

                    drop(written_resources);

                    Ok(index_)
                }
            }
        }?;

        resource.add_writer(self.weak().await, index).await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn written_resources(&self) -> Vec<Resource> {
        match self.written_resources.as_ref() {
            None => unreachable!(),
            Some(written_resources_lock) => {
                let written_resources = written_resources_lock.read().await;

                written_resources
                    .iter()
                    .filter_map(Option::as_ref)
                    .filter_map(|weak_written_resource| {
                        Option::<crate::resource::stable_abi::Trait_TO<RArc<()>>>::from(
                            weak_written_resource.upgrade(),
                        )
                    })
                    .map(Resource::from)
                    .collect()
            }
        }
    }

    #[inline]
    fn written_resources_member_is_none(&self) -> String {
        format!(
            "the written resources member of operation {self:?} is None while it shouldn't be until 
            dropped",
        )
    }
}

impl Operation {
    /// Construct a new operation from metadata.
    ///
    /// # Errors
    ///
    /// This function will return an error if constructing the underlying render pass fails.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new(metadata: Metadata) -> Result<Self, Error> {
        let uuid = Uuid::new_v4();

        let render_pass = metadata.device().new_render_pass(&metadata)?;

        let read_resources = Some(RwLock::new(SmallVec::new()));

        let written_resources = Some(RwLock::new(SmallVec::new()));

        let runtime = Handle::current();

        let this = RwLock::new(Weak::default());

        let implementation = Arc::new(Implementation {
            uuid,
            render_pass,
            read_resources,
            written_resources,
            runtime,
            this,
        });

        let weak_implementation = Arc::downgrade(&implementation);

        *implementation.this.write().await = weak_implementation;

        let state = Trait_TO::from_ptr(RArc::from(implementation), sabi_trait::TD_CanDowncast);

        Ok(Self { state })
    }

    #[inline]
    #[must_use]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn metadata(&self) -> Metadata {
        self.state.metadata().await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new_graphics_pipeline(
        &self,
        metadata: GraphicsPipelineMetadata<'static, 'static>,
    ) -> Result<GraphicsPipeline, ErrorWrapper> {
        self.state.new_graphics_pipeline(metadata).await.into()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn read(&self, resource: Resource) {
        self.state.read(resource).await;
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn read_multiple(&self, resources: &[Resource]) {
        self.state.read_multiple(resources.into()).await;
    }

    /// Execute this operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if the API wrapped by the operation encountered an
    /// error.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn render(&self) -> Result<(), Error> {
        self.state.render().await.into()
    }

    #[inline]
    #[must_use]
    pub fn render_pass(&self) -> RenderPass {
        self.state.render_pass()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn read_resources(&self) -> SmallVec<[Resource; 3]> {
        Vec::from(self.state.read_resources().await).into()
    }

    /// Define an edge from self to resource, meaning that self will write to resource when
    /// executing the render graph containing both self and the resource.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::StateNotRecoverable`] if either some other
    /// [Operation] already writes to resource or if some internal data reached an invalid state.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn write(&self, resource: Resource) -> Result<(), Error> {
        self.state.write(resource).await.into()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn written_resources(&self) -> SmallVec<[Resource; 3]> {
        Vec::from(self.state.written_resources().await).into()
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        match take(&mut self.read_resources) {
            None => unreachable!(),
            Some(reads) => match take(&mut self.written_resources) {
                None => unreachable!(),
                Some(writes) => {
                    self.runtime
                        .spawn(async move { Self::drop(reads, writes).await });
                }
            },
        }
    }
}

impl Clone for Implementation {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!();
    }
}

impl Eq for Implementation {}

impl PartialEq for Implementation {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let uuids_are_equal = self.uuid == other.uuid;

        let render_passes_are_equal = self.render_pass == other.render_pass;

        if uuids_are_equal && !render_passes_are_equal {
            unreachable!()
        }

        uuids_are_equal
    }
}

impl Trait for Implementation {
    #[inline]
    fn equal(&self, other: &Trait_TO<RArc<()>>) -> bool {
        other.equal_uuid(self.uuid.into_bytes())
    }

    #[inline]
    fn equal_uuid(&self, uuid: [u8; 16]) -> bool {
        self.uuid == Uuid::from_bytes(uuid)
    }

    #[inline]
    fn metadata(&self) -> BorrowingFfiFuture<'_, Metadata> {
        async move { self.render_pass.metadata().await }.into_ffi()
    }

    #[inline]
    fn new_graphics_pipeline(
        &self,
        metadata: GraphicsPipelineMetadata<'static, 'static>,
    ) -> BorrowingFfiFuture<'_, RResult<GraphicsPipeline, ErrorWrapper>> {
        async move { self.new_graphics_pipeline(metadata).await.into() }.into_ffi()
    }

    #[inline]
    fn read(&self, resource: Resource) -> BorrowingFfiFuture<'_, ()> {
        async move { self.read(resource).await }.into_ffi()
    }

    fn read_multiple<'a>(&'a self, resources: RSlice<'a, Resource>) -> BorrowingFfiFuture<'a, ()> {
        async move { self.read_multiple(resources.into()).await }.into_ffi()
    }

    #[inline]
    fn read_resources(&self) -> BorrowingFfiFuture<'_, RVec<Resource>> {
        async move { self.read_resources().await.into() }.into_ffi()
    }

    #[inline]
    fn render(&self) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.render().await.into() }.into_ffi()
    }

    #[inline]
    fn render_pass(&self) -> RenderPass {
        self.render_pass.clone()
    }

    #[inline]
    fn remove_read_resource(&self, index: usize) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.remove_read_resource(index).await.into() }.into_ffi()
    }

    #[inline]
    fn remove_written_resource(&self, index: usize) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.remove_written_resource(index).await.into() }.into_ffi()
    }

    #[inline]
    fn weak(&self) -> BorrowingFfiFuture<'_, WeakTrait_TO<RArc<()>>> {
        async move { self.weak().await }.into_ffi()
    }

    #[inline]
    fn write(&self, resource: Resource) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.write(resource).await.into() }.into_ffi()
    }

    #[inline]
    fn written_resources(&self) -> BorrowingFfiFuture<'_, RVec<Resource>> {
        async move { self.written_resources().await.into() }.into_ffi()
    }
}

impl Clone for WeakImplementation {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!();
    }
}

impl WeakTrait for WeakImplementation {
    #[inline]
    fn upgrade(&self) -> ROption<Trait_TO<RArc<()>>> {
        Weak::upgrade(&self.this).map_or_else(
            || RNone,
            |this| RSome(Trait_TO::from_ptr(RArc::from(this), sabi_trait::TD_Opaque)),
        )
    }
}

impl Eq for Operation {}

impl From<Trait_TO<RArc<()>>> for Operation {
    #[inline]
    fn from(state: Trait_TO<RArc<()>>) -> Self {
        Self { state }
    }
}

impl PartialEq for Operation {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.state.equal(&other.state)
    }
}
