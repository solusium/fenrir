use {
    crate::resource::stable_abi::{Trait, Trait_TO, WeakTrait, WeakTrait_TO},
    abi_stable::{
        sabi_trait,
        std_types::{
            RArc,
            ROption::{self, RNone, RSome},
            RResult, RVec,
        },
        StableAbi,
    },
    alloc::{
        format,
        sync::{Arc, Weak},
        vec::Vec,
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::RwLock,
    core::{mem::take, ops::Range},
    fenrir_error::{argument_out_of_domain, invalid_argument, state_not_recoverable, Error},
    fenrir_hal::{color::RGBA, texture::Texture},
    float_eq::float_eq,
    futures::future::join_all,
    sleipnir::runtime::Handle,
    smallvec::SmallVec,
    uuid::Uuid,
};

extern crate alloc;

#[cfg(test)]
pub(crate) mod tests;

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub enum ClearValue {
    FColor(RGBA<f32>),
    SColor(RGBA<isize>),
    UColor(RGBA<usize>),
    DepthStencil { depth: f32, stencil: usize },
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub enum Data {
    ColorAttachment(fenrir_hal::color::Attachment),
    Texture(Texture),
}

pub(crate) mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::{operation::Operation, resource::Data},
        abi_stable::{
            sabi_trait,
            std_types::{RArc, ROption, RResult, RVec},
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub(crate) trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn add_reader<'a>(
            &'a self,
            reader: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
            index: usize,
        ) -> BorrowingFfiFuture<'a, usize>;

        fn add_writer<'a>(
            &'a self,
            operation: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
            index: usize,
        ) -> BorrowingFfiFuture<'a, RResult<(), Error>>;

        fn already_has_writer(&self) -> BorrowingFfiFuture<'_, fenrir_error::Error>;

        fn data(&self) -> Data;

        fn equal(&self, other: &Trait_TO<RArc<()>>) -> bool;

        fn equal_uuid(&self, uuid: [u8; 16]) -> bool;

        fn readers(&self) -> BorrowingFfiFuture<'_, RVec<Operation>>;

        fn remove_reader(&self, index: usize) -> BorrowingFfiFuture<'_, RResult<(), Error>>;

        fn remove_writer(&self) -> BorrowingFfiFuture<'_, RResult<(), Error>>;

        fn weak(&self) -> BorrowingFfiFuture<'_, WeakTrait_TO<RArc<()>>>;

        fn writer(
            &self,
        ) -> BorrowingFfiFuture<'_, ROption<crate::operation::stable_abi::Trait_TO<RArc<()>>>>;
    }

    #[sabi_trait]
    pub(crate) trait WeakTrait: Clone + Debug + Send + Sync + 'static {
        fn upgrade(&self) -> ROption<Trait_TO<RArc<()>>>;
    }
}

#[repr(C)]
#[derive(Debug)]
pub(crate) struct Inner {
    uuid: Uuid,
    data: Data,
    metadata: Metadata,
    readers: Option<
        RwLock<
            SmallVec<[Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>; 3]>,
        >,
    >,
    writer: Option<RwLock<Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>>>,
    runtime: Handle,
    this: RwLock<Weak<Inner>>,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub enum Operation {
    Clear,
    DontCare,
    Load,
    None,
    Store,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Metadata {
    begin_rendering_operation: ROption<Operation>,
    end_rendering_operation: ROption<Operation>,
    clear_value: ROption<ClearValue>,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, StableAbi)]
pub struct Resource {
    inner: Trait_TO<RArc<()>>,
}

#[derive(Debug)]
struct WeakInner {
    this: Weak<Inner>,
}

impl Metadata {
    #[inline]
    #[must_use]
    pub fn new(
        begin_rendering_operation_: Option<Operation>,
        end_rendering_operation_: Option<Operation>,
        clear_value_: Option<ClearValue>,
    ) -> Self {
        let begin_rendering_operation = begin_rendering_operation_.into();

        let end_rendering_operation = end_rendering_operation_.into();

        let clear_value = clear_value_.into();

        Self {
            begin_rendering_operation,
            end_rendering_operation,
            clear_value,
        }
    }
}

impl Inner {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn add_reader(
        &self,
        reader: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> usize {
        let mut readers = self.get_readers_lock().write().await;

        let maybe_reader_index = readers
            .iter_mut()
            .enumerate()
            .find(|(_, maybe_weak_reader)| maybe_weak_reader.is_none())
            .map(|(reader_index, maybe_weak_reader)| {
                *maybe_weak_reader = Some((reader.clone(), index));

                reader_index
            });

        match maybe_reader_index {
            None => {
                let reader_index = readers.len();

                readers.push(None);

                readers.last_mut().map_or_else(
                    || unreachable!(),
                    |maybe_weak_reader| {
                        *maybe_weak_reader = Some((reader, index));
                    },
                );

                reader_index
            }
            Some(reader_index) => {
                drop(readers);

                reader_index
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn add_writer(
        &self,
        writer: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> Result<(), Error> {
        let writer_lock = self.get_writer_lock();

        let mut writer_guard = writer_lock.write().await;

        if let Some(weak_writer) = writer_guard.clone() {
            drop(writer_guard);

            Err(self.already_has_writer(&weak_writer.0))
        } else {
            *writer_guard = Some((writer, index));

            drop(writer_guard);

            Ok(())
        }
    }

    #[cold]
    #[inline]
    fn already_has_writer(
        &self,
        weak_writer: &crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
    ) -> fenrir_error::Error {
        state_not_recoverable(&format!(
            "{self:?} already contains writer {weak_writer:?}. Multiple writers are not allowed"
        ))
    }

    #[inline]
    fn data(&self) -> Data {
        self.data.clone()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop(
        writer_lock: RwLock<Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>>,
        readers_lock: RwLock<
            SmallVec<[Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>; 3]>,
        >,
    ) {
        {
            let maybe_writer = writer_lock.read().await;

            if let Some(writer) = maybe_writer.as_ref() {
                let weak_writer = &writer.0;

                if let RSome(writer_arc) = weak_writer.upgrade() {
                    let index = writer.1;

                    if writer_arc.remove_written_resource(index).await.is_err() {
                        unreachable!();
                    }
                }
            }
        }

        let readers_lock_guard = readers_lock.read().await;

        let remove_readers_futures = || {
            readers_lock_guard
                .iter()
                .filter_map(Option::as_ref)
                .map(|(weak_reader, index)| async move {
                    if let RSome(reader) = weak_reader.upgrade() {
                        if reader.remove_read_resource(*index).await.is_err() {
                            unreachable!();
                        }
                    }
                })
                .collect::<SmallVec<[_; 3]>>()
        };

        join_all(remove_readers_futures()).await;
    }

    #[inline]
    fn get_readers_lock(
        &self,
    ) -> &RwLock<SmallVec<[Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>; 3]>>
    {
        self.readers.as_ref().map_or_else(
            || {
                unreachable!(
                    "the reading operations member of {self:?} is None while it shouldn't be until \
                    dropped"
                )
            },
            |writer_lock| writer_lock,
        )
    }

    #[inline]
    fn get_writer_lock(
        &self,
    ) -> &RwLock<Option<(crate::operation::stable_abi::WeakTrait_TO<RArc<()>>, usize)>> {
        self.writer
            .as_ref()
            .map_or_else(|| unreachable!(), |writer_lock| writer_lock)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn readers(&self) -> Vec<crate::operation::Operation> {
        match self.readers.as_ref() {
            None => unreachable!(
                "the reading operations member of {self:?} is None while it shouldn't be until 
                dropped"
            ),
            Some(readers_lock) => {
                let readers = readers_lock.read().await;

                readers
                    .iter()
                    .filter_map(Option::as_ref)
                    .filter_map(|(weak_reader, _)| {
                        Option::<crate::operation::stable_abi::Trait_TO<RArc<()>>>::from(
                            weak_reader.upgrade(),
                        )
                    })
                    .map(crate::operation::Operation::from)
                    .collect()
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn remove_reader(&self, index: usize) -> Result<(), Error> {
        let mut readers = self.get_readers_lock().write().await;

        if readers.is_empty() {
            drop(readers);

            Err(state_not_recoverable(&format!(
                "attempting to remove a reader from {self:?} which doesn't have any readers"
            )))
        } else if readers.len() <= index {
            Err(argument_out_of_domain(
                Range {
                    start: 0.into(),
                    end: readers.len().into(),
                },
                index.into(),
            ))
        } else {
            let maybe_reader = &mut readers[index];

            if maybe_reader.is_none() {
                drop(readers);

                Err(invalid_argument(&format!(
                    "{self:?} has no reader at index {index}"
                )))
            } else {
                *maybe_reader = None;

                Ok(())
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn remove_writer(&self) -> Result<(), Error> {
        let mut maybe_real_writer = self.get_writer_lock().write().await;

        let maybe_real_writer_ref = maybe_real_writer.as_ref();

        if maybe_real_writer_ref.is_none() {
            drop(maybe_real_writer);

            Err(state_not_recoverable(&format!(
                "{self:?} has no current writer"
            )))
        } else {
            *maybe_real_writer = None;

            drop(maybe_real_writer);

            Ok(())
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn weak(&self) -> WeakTrait_TO<RArc<()>> {
        let this = self.this.read().await.clone();

        WeakTrait_TO::from_ptr(RArc::new(WeakInner { this }), sabi_trait::TD_Opaque)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn writer(&self) -> Option<crate::operation::stable_abi::Trait_TO<RArc<()>>> {
        match self.writer.as_ref() {
            None => None,
            Some(writer_lock) => {
                let maybe_writer = writer_lock.read().await;

                let maybe_writer_ref = maybe_writer.as_ref();

                #[allow(clippy::option_if_let_else)]
                match maybe_writer_ref {
                    None => {
                        drop(maybe_writer);

                        None
                    }
                    Some(writer) => writer.0.upgrade().into(),
                }
            }
        }
    }
}

impl Resource {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new(data: Data, metadata: Metadata) -> Self {
        let uuid = Uuid::new_v4();

        let readers = Some(RwLock::new(SmallVec::new()));

        let writer = Some(RwLock::new(None));

        let runtime = Handle::current();

        let this = RwLock::new(Weak::default());

        let inner_arc = Arc::new(Inner {
            uuid,
            data,
            metadata,
            readers,
            writer,
            runtime,
            this,
        });

        let weak_inner = Arc::downgrade(&inner_arc);

        *inner_arc.this.write().await = weak_inner;

        let inner = Trait_TO::from_ptr(RArc::from(inner_arc), sabi_trait::TD_CanDowncast);

        Self { inner }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn add_reader(
        &self,
        operation: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> usize {
        self.inner.add_reader(operation, index).await
    }

    #[cfg(test)]
    pub(crate) async fn already_has_writer(&self) -> fenrir_error::Error {
        self.inner.already_has_writer().await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn add_writer(
        &self,
        operation: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> Result<(), Error> {
        self.inner.add_writer(operation, index).await.into()
    }

    #[inline]
    #[must_use]
    pub fn data(&self) -> Data {
        self.inner.data()
    }

    #[inline]
    #[must_use]
    pub fn metadata(&self) -> Metadata {
        self.inner.obj.downcast_as::<Inner>().map_or_else(
            |_| unreachable!(),
            |implementation| implementation.metadata.clone(),
        )
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn readers(&self) -> SmallVec<[crate::operation::Operation; 3]> {
        Vec::from(self.inner.readers().await).into()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn weak_state(&self) -> WeakTrait_TO<RArc<()>> {
        self.inner.weak().await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn writer(&self) -> Option<crate::operation::Operation> {
        match self.inner.writer().await {
            RSome(writer) => Some(writer.into()),
            RNone => None,
        }
    }
}

impl Eq for ClearValue {}

impl From<RGBA<f32>> for ClearValue {
    #[inline]
    fn from(rgba: RGBA<f32>) -> Self {
        Self::FColor(rgba)
    }
}

impl From<RGBA<isize>> for ClearValue {
    #[inline]
    fn from(rgba: RGBA<isize>) -> Self {
        Self::SColor(rgba)
    }
}

impl From<RGBA<usize>> for ClearValue {
    #[inline]
    fn from(rgba: RGBA<usize>) -> Self {
        Self::UColor(rgba)
    }
}

impl From<(f32, usize)> for ClearValue {
    #[inline]
    fn from((depth, stencil): (f32, usize)) -> Self {
        Self::DepthStencil { depth, stencil }
    }
}

impl PartialEq for ClearValue {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::FColor(left), Self::FColor(right)) => left == right,
            (Self::SColor(left), Self::SColor(right)) => left == right,
            (Self::UColor(left), Self::UColor(right)) => left == right,
            (
                Self::DepthStencil {
                    depth: left_depth,
                    stencil: left_stencil,
                },
                Self::DepthStencil {
                    depth: right_depth,
                    stencil: right_stencil,
                },
            ) => {
                float_eq!(left_depth, right_depth, r2nd <= f32::EPSILON)
                    && left_stencil == right_stencil
            }
            _ => false,
        }
    }
}

impl From<Trait_TO<RArc<()>>> for Resource {
    #[inline]
    fn from(inner: Trait_TO<RArc<()>>) -> Self {
        Self { inner }
    }
}

impl Clone for Inner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!();
    }
}

impl Drop for Inner {
    #[inline]
    fn drop(&mut self) {
        let maybe_writer_lock = take(&mut self.writer);

        match maybe_writer_lock {
            None => unreachable!(),
            Some(writer_lock) => {
                let maybe_readers_lock = take(&mut self.readers);

                match maybe_readers_lock {
                    None => unreachable!(),
                    Some(readers_lock) => {
                        let _join_handle = self
                            .runtime
                            .spawn(async move { Self::drop(writer_lock, readers_lock).await });
                    }
                }
            }
        }
    }
}

impl Eq for Inner {}

impl PartialEq for Inner {
    fn eq(&self, other: &Self) -> bool {
        let uuids_are_equal = self.uuid == other.uuid;

        let metadatas_are_equal = self.metadata == other.metadata;

        if uuids_are_equal && !metadatas_are_equal {
            unreachable!();
        }

        uuids_are_equal
    }
}

impl Trait for Inner {
    #[inline]
    fn add_reader(
        &self,
        reader: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> BorrowingFfiFuture<'_, usize> {
        async move { self.add_reader(reader, index).await }.into_ffi()
    }

    #[inline]
    fn add_writer(
        &self,
        operation: crate::operation::stable_abi::WeakTrait_TO<RArc<()>>,
        index: usize,
    ) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.add_writer(operation, index).await.into() }.into_ffi()
    }

    #[inline]
    fn already_has_writer(&self) -> BorrowingFfiFuture<'_, fenrir_error::Error> {
        async move {
            match self.writer.as_ref() {
                None => unreachable!(),
                Some(weak_writer_lock) => (*weak_writer_lock.read().await).as_ref().map_or_else(
                    || unreachable!(),
                    |weak_writer| self.already_has_writer(&weak_writer.0),
                ),
            }
        }
        .into_ffi()
    }

    #[inline]
    fn data(&self) -> Data {
        self.data()
    }

    #[inline]
    fn equal(&self, other: &Trait_TO<RArc<()>>) -> bool {
        other.equal_uuid(self.uuid.into_bytes())
    }

    #[inline]
    fn equal_uuid(&self, uuid: [u8; 16]) -> bool {
        self.uuid == Uuid::from_bytes(uuid)
    }

    #[inline]
    fn readers(&self) -> BorrowingFfiFuture<'_, RVec<crate::operation::Operation>> {
        async move { self.readers().await.into() }.into_ffi()
    }

    #[inline]
    fn remove_reader(&self, index: usize) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.remove_reader(index).await.into() }.into_ffi()
    }

    #[inline]
    fn remove_writer(&self) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { self.remove_writer().await.into() }.into_ffi()
    }

    #[inline]
    fn weak(&self) -> BorrowingFfiFuture<'_, WeakTrait_TO<RArc<()>>> {
        async move { self.weak().await }.into_ffi()
    }

    #[inline]
    fn writer(
        &self,
    ) -> BorrowingFfiFuture<'_, ROption<crate::operation::stable_abi::Trait_TO<RArc<()>>>> {
        async move { self.writer().await.into() }.into_ffi()
    }
}

impl PartialEq for Resource {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.inner.equal(&other.inner)
    }
}

unsafe impl Send for Resource {}
unsafe impl Sync for Resource {}

impl Clone for WeakInner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!();
    }
}

impl WeakTrait for WeakInner {
    #[inline]
    fn upgrade(&self) -> ROption<Trait_TO<RArc<()>>> {
        Weak::upgrade(&self.this).map_or_else(
            || RNone,
            |this| RSome(Trait_TO::from_ptr(RArc::from(this), sabi_trait::TD_Opaque)),
        )
    }
}
