use {
    abi_stable::std_types::RResult::{RErr, ROk},
    core::ops::Deref,
    fenrir_error::{initialization_failed, state_not_recoverable, Error},
    fenrir_frame::pipeline::Pipeline,
    fenrir_hal::{
        instance::Metadata, shader::compiler::Backend as ShaderCompilerBackend, Instance,
    },
    futures::{future, stream, Future, StreamExt},
    regex::Regex,
    sleipnir::ffi::runtime::Handle,
    smallvec::{smallvec, SmallVec},
    std::{
        env::current_exe,
        fs::{read_dir, DirEntry},
        path::PathBuf,
    },
    strum::{EnumCount, IntoEnumIterator},
    tracing::error,
};

pub struct Core {
    pub instance: Instance,
    pub render_graph_manager: fenrir_frame::render_graph::Manager,
    pub texture_manager: fenrir_frame::texture::manager::Manager,
    pub pipeline: Pipeline,
}

impl Core {
    pub async fn new(
        search_paths: &mut Vec<PathBuf>,
        backend_name: String,
        mut metadata: Metadata,
        allocator_backend: fenrir_hal::allocator::Backend,
        shader_compiler_backend: Option<ShaderCompilerBackend>,
    ) -> Result<Self, Error> {
        match current_exe() {
            Err(_) => Err(state_not_recoverable("path to this executable not found.")),
            Ok(current_exe) => match current_exe.parent() {
                None => Ok(()),
                Some(exe_path) => match exe_path.parent() {
                    None => Ok(()),
                    Some(exe_parent_path) => {
                        if !search_paths
                            .iter()
                            .any(|search_path| search_path == exe_parent_path)
                        {
                            let mut search_path = exe_parent_path.to_path_buf();

                            search_path.push("deps");

                            search_paths.push(search_path.clone());

                            search_paths.push(search_path.parent().unwrap().to_path_buf());
                        }

                        Ok(())
                    }
                },
            },
        }?;

        match Pipeline::new().await {
            RErr(code) => Err(code),
            ROk(pipeline) => {
                let mut file_regex = "libfenrir_".to_string();

                file_regex.push_str(backend_name.as_str());
                file_regex.push_str("(-[a-z0-9]+)?\\.so");

                let regex = match Regex::new(&file_regex) {
                    Ok(regex_) => Ok(regex_),
                    Err(_) => Err(initialization_failed("failed to construct regex")),
                }?;

                let runtime = sleipnir::runtime::Handle::current().into();

                let runtime_ref = &runtime;

                let allocator_manager =
                    Self::get_allocator_manager(allocator_backend, search_paths, runtime_ref)
                        .await?;

                let shader_compiler =
                    Self::get_shader_compiler(shader_compiler_backend, search_paths)?;

                metadata.shader_compiler = shader_compiler.into();

                let regex_ref = &regex;
                let metadata_ref = &mut metadata;
                let allocator_manager_ref = &allocator_manager;

                let instance = search_paths
                    .iter()
                    .filter(|search_path| search_path.is_dir())
                    .map(|search_path| match read_dir(search_path) {
                        Err(_) => Err(initialization_failed("could not read directory")),
                        Ok(entries) => entries
                            .into_iter()
                            .map(|entry_result| {
                                Self::get_backend_from_directory_entry(
                                    entry_result,
                                    regex_ref,
                                    metadata_ref,
                                    runtime_ref,
                                    allocator_manager_ref,
                                )
                            })
                            .fold(
                                Err(initialization_failed("could not get backend")),
                                Self::fold_backend_results,
                            ),
                    })
                    .fold(
                        Err(initialization_failed("could not get backend")),
                        Self::fold_backend_results,
                    )?;

                let texture_manager =
                    fenrir_frame::texture::manager::Manager::new(pipeline.clone()).await;

                let render_graph_manager = fenrir_frame::render_graph::Manager::new(
                    pipeline.clone(),
                    texture_manager.clone(),
                )
                .await?;

                Ok(Core {
                    instance,
                    render_graph_manager,
                    texture_manager,
                    pipeline,
                })
            }
        }
    }

    async fn fold_allocator_manager_results<F>(
        result: Result<fenrir_hal::allocator::manager::Manager, Error>,
        current: F,
    ) -> Result<fenrir_hal::allocator::manager::Manager, Error>
    where
        F: Future<Output = Result<fenrir_hal::allocator::manager::Manager, Error>>,
    {
        match result {
            Ok(allocator) => Ok(allocator),
            Err(error) => match current.await {
                Ok(allocator) => Ok(allocator),
                Err(_) => Err(error),
            },
        }
    }

    #[inline]
    fn fold_backend_results(
        result: Result<Instance, Error>,
        current: Result<Instance, Error>,
    ) -> Result<Instance, Error> {
        match result {
            Ok(backend) => Ok(backend),
            Err(error) => match current {
                Ok(backend) => Ok(backend),
                Err(_) => Err(error),
            },
        }
    }

    async fn get_allocator_manager(
        allocator_backend: fenrir_hal::allocator::Backend,
        search_paths: &[PathBuf],
        runtime: &Handle,
    ) -> Result<fenrir_hal::allocator::manager::Manager, Error> {
        let allocator_name = match allocator_backend {
            fenrir_hal::allocator::Backend::GPUAllocator => "gpu_allocator",
        };

        let mut file_regex = "libfenrir_".to_string();

        file_regex.push_str(allocator_name);
        file_regex.push_str("(-[a-z0-9]+)?\\.so");

        let regex = match Regex::new(&file_regex) {
            Ok(regex_) => Ok(regex_),
            Err(_) => Err(initialization_failed("could not construct regex")),
        }?;

        let regex_ref = &regex;

        stream::iter(search_paths.iter())
            .filter(|search_path| future::ready(search_path.is_dir()))
            .map(|search_path| async move {
                match read_dir(search_path) {
                    Err(_) => Err(initialization_failed(&format!(
                        "could not read directory {search_path:?}"
                    ))),
                    Ok(entries) => {
                        stream::iter(entries)
                            .map(|entry_result| async move {
                                Self::get_allocator_manager_from_directory_entry(
                                    entry_result,
                                    regex_ref,
                                    runtime,
                                )
                            })
                            .fold(
                                Err(initialization_failed("could not get allocator manager")),
                                Self::fold_allocator_manager_results,
                            )
                            .await
                    }
                }
            })
            .fold(
                Err(initialization_failed("could not get allocator manager")),
                Self::fold_allocator_manager_results,
            )
            .await
    }

    fn get_allocator_manager_from_directory_entry(
        entry_result: std::io::Result<DirEntry>,
        regex: &Regex,
        runtime: &Handle,
    ) -> Result<fenrir_hal::allocator::manager::Manager, Error> {
        match entry_result {
            Err(_) => Err(initialization_failed("could not read directory entry")),
            Ok(entry) => {
                let path = entry.path();

                if !path.is_file() {
                    Err(initialization_failed("path is not a file"))
                } else {
                    match path.file_name() {
                        None => Err(initialization_failed("could not get file name")),
                        Some(file_os_name) => match file_os_name.to_str() {
                            None => {
                                Err(initialization_failed("failed to convert file name to str"))
                            }
                            Some(file_name) => {
                                if !regex.is_match(file_name) {
                                    Err(initialization_failed("failed to match regex"))
                                } else {
                                    fenrir_hal::allocator::manager::Manager::new(
                                        &path,
                                        runtime.clone(),
                                    )
                                }
                            }
                        },
                    }
                }
            }
        }
    }

    fn get_backend_from_directory_entry(
        entry_result: std::io::Result<DirEntry>,
        regex: &Regex,
        metadata: &Metadata,
        runtime: &Handle,
        allocator_manager: &fenrir_hal::allocator::manager::Manager,
    ) -> Result<Instance, Error> {
        match entry_result {
            Err(_) => Err(initialization_failed("failed to read directory entry")),
            Ok(entry) => {
                let path = entry.path();

                if !path.is_file() {
                    Err(initialization_failed("path is not a file"))
                } else {
                    match path.file_name() {
                        None => Err(initialization_failed("could not get file name")),
                        Some(file_os_name) => {
                            match file_os_name.to_str() {
                                None => {
                                    Err(initialization_failed("failed to convert file name to str"))
                                }
                                Some(file_name) => {
                                    if !regex.is_match(file_name) {
                                        Err(initialization_failed("failed to match regex"))
                                    } else {
                                        let backend_result = Instance::new(
                                            &path,
                                            metadata.clone(),
                                            runtime.clone(),
                                            allocator_manager.clone(),
                                        );

                                        match backend_result {
                                            Ok(backend) => Ok(backend),
                                            Err(error) => {
                                                error!("failed to load backend from '{path:?}': {error:?}");

                                                Err(error)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    #[inline]
    fn get_shader_compiler(
        maybe_shader_compiler_backend: Option<ShaderCompilerBackend>,
        search_paths: &[PathBuf],
    ) -> Result<fenrir_shader_compiler::Compiler, Error> {
        let shader_compiler_backends: SmallVec<[_; ShaderCompilerBackend::COUNT]> =
            match maybe_shader_compiler_backend {
                Some(shader_compiler_backend) => smallvec![shader_compiler_backend],
                None => ShaderCompilerBackend::iter().collect(),
            };

        let maybe_shader_compiler = shader_compiler_backends
            .into_iter()
            .map(|shader_compiler_backend| match shader_compiler_backend {
                ShaderCompilerBackend::Naga => "naga",
            })
            .find_map(|shader_compiler_backend| {
                search_paths
                    .iter()
                    .filter(|search_path| search_path.is_dir())
                    .find_map(|search_dir| match read_dir(search_dir) {
                        Err(_) => None,
                        Ok(entries) => entries
                            .into_iter()
                            .filter_map(Result::ok)
                            .filter(|entry| {
                                entry
                                    .file_name()
                                    .to_string_lossy()
                                    .contains(shader_compiler_backend)
                            })
                            .find_map(|entry| {
                                fenrir_shader_compiler::Compiler::new(&entry.path()).ok()
                            }),
                    })
            });

        maybe_shader_compiler.ok_or_else(|| {
            let reason = format!("could not find shader compiler in {search_paths:?}");

            initialization_failed(&reason)
        })
    }

    #[inline]
    pub fn render_graph_manager(&self) -> fenrir_frame::render_graph::Manager {
        self.render_graph_manager.clone()
    }
}

impl Deref for Core {
    type Target = Instance;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.instance
    }
}

unsafe impl Send for Core {}
unsafe impl Sync for Core {}

#[cfg(test)]
pub mod tests {
    use {
        crate::Core,
        fenrir_hal::instance::Metadata,
        futures::future::join_all,
        itertools::Itertools,
        smallvec::SmallVec,
        std::{
            env::{current_exe, var},
            fs::read_dir,
            path::{Path, PathBuf},
            str::FromStr,
        },
        strum::IntoEnumIterator,
        tokio::test,
    };

    #[inline]
    #[cfg(debug_assertions)]
    const fn mode() -> &'static str {
        "debug"
    }

    #[inline]
    #[cfg(not(debug_assertions))]
    const fn mode() -> &'static str {
        "release"
    }

    #[inline]
    fn load_shader_compiler(name: &str) -> fenrir_shader_compiler::Compiler {
        let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

        assert!(mainifest_dir_result.is_ok());

        let library_dir_result = PathBuf::from_str(&mainifest_dir_result.unwrap());

        assert!(library_dir_result.is_ok());

        let maybe_library_dir = library_dir_result.unwrap().parent().map(Path::to_path_buf);

        assert!(maybe_library_dir.is_some());

        let mut library_dir = maybe_library_dir.unwrap();

        library_dir.push("target");

        library_dir.push(mode());

        library_dir.push("deps");

        let maybe_entires = read_dir(&library_dir);

        assert!(maybe_entires.is_ok());

        let maybe_compiler = maybe_entires
            .unwrap()
            .filter_map(Result::ok)
            .filter(|entry| entry.file_name().to_string_lossy().contains(name))
            .filter_map(
                |entry| match fenrir_shader_compiler::Compiler::new(&entry.path()) {
                    Err(_) => None,
                    Ok(module) => Some(module),
                },
            )
            .find(|_| true);

        assert!(maybe_compiler.is_some());

        maybe_compiler.unwrap()
    }

    pub(crate) async fn cores() -> SmallVec<[Core; 1]> {
        let mut _core_futures = Vec::new();

        _core_futures = fenrir_hal::instance::Backend::iter()
            .cartesian_product([fenrir_hal::allocator::Backend::GPUAllocator])
            .cartesian_product(["naga"])
            .map(
                |((backend, allocator_backend), shader_compiler_backend)| async move {
                    let exe_path = current_exe().unwrap();

                    let mut search_paths = Vec::new();

                    search_paths.push(exe_path.parent().unwrap().to_path_buf());

                    let mut name = "tests::core::cores - ".to_string();

                    let backend_name = match backend {
                        fenrir_hal::instance::Backend::Ash => "ash",
                    };

                    name.push_str(backend_name);

                    let shader_compiler = load_shader_compiler(shader_compiler_backend).into();

                    let metadata = Metadata {
                        name: name.into(),
                        major: 0,
                        minor: 1,
                        patch: 0,
                        tracing_level: 0,
                        shader_compiler,
                    };

                    Core::new(
                        &mut search_paths,
                        backend_name.to_string(),
                        metadata,
                        allocator_backend,
                        None,
                    )
                    .await
                    .unwrap()
                },
            )
            .collect();

        join_all(_core_futures.into_iter()).await.into()
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn devices() {
        let cores = cores().await;

        for core in cores {
            let devices = core.devices();

            assert!(devices.is_ok());

            assert!(!devices.unwrap().is_empty())
        }
    }
}
