pub(crate) use crate::subscription::service::item::Item;
use {
    super::Subscription,
    crate::subscription::{
        service::stable_abi::{Trait, Trait_TO},
        Guard,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::{TD_CanDowncast, TD_Opaque},
        std_types::{
            RArc, RBox,
            ROption::{self, RNone, RSome},
            Tuple2,
        },
        StableAbi,
    },
    alloc::sync::Arc,
    async_lock::Mutex,
    core::{
        fmt::{Debug, Formatter},
        pin::Pin,
        task::{Context, Poll},
    },
    futures::{channel::oneshot::channel, Stream},
    sleipnir::runtime::Handle,
};

extern crate alloc;

pub(crate) mod item;

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use abi_stable::{sabi_trait, std_types::RBox};

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + Sync + Unpin + 'static {
        fn new_item(&self) -> crate::subscription::service::item::stable_abi::Trait_TO<RBox<()>>;

        fn subscribe(&self) -> crate::subscription::stable_abi::Trait_TO<RBox<()>>;
    }
}

#[derive(Debug)]
struct Inner<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
    runtime: Handle,
    next_iteration_sender: tokio::sync::broadcast::Sender<Iteration<T, U>>,
}

#[repr(C)]
#[derive(Debug, StableAbi)]
pub(crate) struct Iteration<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
    guard: RArc<Guard>,
    pub(crate) sender_and_receiver: ROption<
        Tuple2<
            sleipnir::ffi::sync::mpsc::UnboundedSender<U>,
            sleipnir::ffi::sync::broadcast::Receiver<T>,
        >,
    >,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Service<T, U> {
    inner: Trait_TO<RBox<()>>,
    _phantom_data: NonOwningPhantom<(T, U)>,
}

impl<T, U> Service<T, U>
where
    T: Clone + Debug + StableAbi + Send + Sync + Unpin + 'static,
    U: Clone + Debug + StableAbi + Send + Sync + Unpin + 'static,
{
    #[inline]
    #[must_use]
    #[allow(dead_code)]
    pub fn new(runtime: Handle) -> Self {
        let (next_iteration_sender, _) = tokio::sync::broadcast::channel(1);

        let inner = Trait_TO::from_value(
            Inner::<T, U> {
                runtime,
                next_iteration_sender,
            },
            TD_Opaque,
        );

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }

    #[inline]
    #[must_use]
    pub fn subscribe(&self) -> Subscription<T, U> {
        Subscription::new(self.inner.subscribe())
    }
}

unsafe impl<T, U> Send for Inner<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
}

impl<T, U> Clone for Iteration<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
    #[inline]
    fn clone(&self) -> Self {
        let sender_and_receiver = match self.sender_and_receiver.as_ref() {
            RNone => RNone,
            RSome(sender_and_receiver_) => {
                let sender = sender_and_receiver_.0.clone();
                let receiver = sender_and_receiver_.1.resubscribe();

                RSome((sender, receiver).into())
            }
        };

        Self {
            guard: self.guard.clone(),
            sender_and_receiver,
        }
    }
}

unsafe impl<T, U> Send for Iteration<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
}

unsafe impl<T, U> Sync for Iteration<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
}

impl<T, U> Trait for Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
{
    #[inline]
    fn new_item(&self) -> crate::subscription::service::item::stable_abi::Trait_TO<RBox<()>> {
        let next_iteration_sender = self.next_iteration_sender.clone();

        let (guard_drop_sender, guard_drop_receiver) = channel();

        let runtime = self.runtime.clone();

        let guard = Arc::new(Guard::new(guard_drop_sender, runtime)).into();

        let (service_message_sender, service_message_receiver) = tokio::sync::broadcast::channel(1);

        let (subscription_message_sender, subscription_message_receiver) =
            futures::channel::mpsc::unbounded();

        let sender_and_receiver = RSome(
            (
                subscription_message_sender.into(),
                service_message_receiver.into(),
            )
                .into(),
        );

        let iteration = Iteration {
            guard,
            sender_and_receiver,
        };

        let join_handle = self.runtime.spawn(async move {
            next_iteration_sender
                .send(iteration)
                .map_or(0, |subscriber_count| subscriber_count)
        });

        crate::subscription::service::item::stable_abi::Trait_TO::from_value(
            crate::subscription::service::item::Inner::<T, U>::new(
                join_handle,
                guard_drop_receiver,
                service_message_sender,
                subscription_message_receiver,
            ),
            TD_Opaque,
        )
    }

    #[inline]
    fn subscribe(&self) -> crate::subscription::Trait_TO<RBox<()>> {
        let receiver = Arc::new(Mutex::new(self.next_iteration_sender.subscribe()));

        crate::subscription::Trait_TO::from_value(
            crate::subscription::Inner { receiver },
            TD_CanDowncast,
        )
    }
}

impl<T, U> Debug for Service<T, U> {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Service")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T, U> Stream for Service<T, U>
where
    T: Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
{
    type Item = Item<T, U>;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        Poll::Ready(Some(Item::new(self.inner.new_item())))
    }
}
