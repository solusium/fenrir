use {
    crate::subscription::guard::stable_abi::{Trait, Trait_TO},
    abi_stable::{sabi_trait::TD_Opaque, std_types::RBox, StableAbi},
    core::mem::take,
    futures::channel::oneshot::Sender,
    sleipnir::runtime::Handle,
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use abi_stable::sabi_trait;

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + Sync + Unpin + 'static {
        fn drop(&mut self);
    }
}

#[repr(C)]
#[derive(Debug, StableAbi)]
pub(super) struct Guard {
    inner: Trait_TO<RBox<()>>,
}

#[derive(Debug)]
struct Inner {
    drop_sender: Option<Sender<()>>,
    runtime: Handle,
}

impl Guard {
    #[inline]
    pub(super) fn new(drop_sender: Sender<()>, runtime: Handle) -> Self {
        let drop_sender = Some(drop_sender);

        Self {
            inner: Trait_TO::from_value(
                Inner {
                    drop_sender,
                    runtime,
                },
                TD_Opaque,
            ),
        }
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        self.inner.drop();
    }
}

impl Trait for Inner {
    #[inline]
    fn drop(&mut self) {
        match take(&mut self.drop_sender) {
            None => unreachable!("sender to signal shouldn't be taken before dropped"),
            Some(drop_sender) => {
                let _join_handle = self.runtime.spawn(async move {
                    let _result = drop_sender.send(());
                });
            }
        }
    }
}
