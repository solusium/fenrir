use {
    crate::subscription::Service,
    abi_stable::StableAbi,
    async_lock::Mutex,
    fenrir_error::Error,
    futures::{SinkExt, StreamExt},
    rand::{thread_rng, Rng},
    sleipnir::runtime::Handle,
    std::sync::Arc,
};

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
enum ServiceMessage {
    Test(usize),
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
enum SubscriptionMessage {
    Test(usize),
}

#[inline]
async fn iterate(
    service: &mut Service<ServiceMessage, SubscriptionMessage>,
) -> crate::subscription::service::Item<ServiceMessage, SubscriptionMessage> {
    let maybe_iteration = service.next().await;

    assert!(maybe_iteration.is_some());

    maybe_iteration.unwrap()
}

async fn await_service_iteration(
    iteration: crate::subscription::service::Item<ServiceMessage, SubscriptionMessage>,
) {
    let result = iteration.await;

    assert!(result.is_ok());
}

fn unwrap_subscription_iteration(
    maybe_iteration_result: Option<
        Result<crate::subscription::Item<ServiceMessage, SubscriptionMessage>, Error>,
    >,
) -> crate::subscription::Item<ServiceMessage, SubscriptionMessage> {
    assert!(maybe_iteration_result.is_some());

    let iteration_result = maybe_iteration_result.unwrap();

    assert!(iteration_result.is_ok());

    iteration_result.unwrap()
}

#[tokio::test]
async fn no_subscription() {
    let mut service = Service::<ServiceMessage, SubscriptionMessage>::new(Handle::current());

    await_service_iteration(iterate(&mut service).await).await;
}

#[tokio::test]
async fn one_subscription_no_communication() {
    let runtime = Handle::current();

    let mut service = Service::<ServiceMessage, SubscriptionMessage>::new(runtime.clone());

    let mut subscription = service.subscribe();

    let subscription_task = runtime.spawn(async move {
        let _subscription_iteration = unwrap_subscription_iteration(subscription.next().await);
    });

    await_service_iteration(iterate(&mut service).await).await;

    assert!(subscription_task.await.is_ok());
}

#[tokio::test]
async fn one_subscription_multiple_iterations_no_communication() {
    let runtime = Handle::current();

    let mut service = Service::<ServiceMessage, SubscriptionMessage>::new(runtime.clone());

    let subscription = Arc::new(Mutex::new(service.subscribe()));

    for _ in 0..thread_rng().gen_range(2..16) {
        let subscription_ = subscription.clone();

        let subscription_task = runtime.spawn(async move {
            let _subscription_iteration =
                unwrap_subscription_iteration(subscription_.clone().lock().await.next().await);
        });

        await_service_iteration(iterate(&mut service).await).await;

        assert!(subscription_task.await.is_ok());
    }
}

#[tokio::test]
async fn one_subscription_with_communication() {
    let runtime = Handle::current();

    let mut service = Service::<ServiceMessage, SubscriptionMessage>::new(runtime.clone());

    let mut subscription = service.subscribe();

    let expected_value = 42;

    let subscription_task = runtime.spawn(async move {
        let mut subscription_iteration = unwrap_subscription_iteration(subscription.next().await);

        let sender_and_stream = subscription_iteration.sender_and_stream();

        assert!(sender_and_stream.is_some());

        let (mut sender, mut stream) = sender_and_stream.unwrap();

        assert!(sender
            .send(SubscriptionMessage::Test(expected_value))
            .await
            .is_ok());

        let maybe_message = stream.next().await;

        assert!(maybe_message.is_some());

        let ServiceMessage::Test(value) = maybe_message.unwrap();

        assert_eq!(expected_value, value);
    });

    let mut service_iteration = iterate(&mut service).await;

    let maybe_sender_stream = service_iteration.sender_and_stream();

    assert!(maybe_sender_stream.is_some());

    let (sender, mut stream) = maybe_sender_stream.unwrap();

    let maybe_message = stream.next().await;

    assert!(maybe_message.is_some());

    let SubscriptionMessage::Test(value) = maybe_message.unwrap();

    assert_eq!(expected_value, value);

    assert!(sender.send(ServiceMessage::Test(expected_value)).is_ok());

    await_service_iteration(service_iteration).await;

    assert!(subscription_task.await.is_ok());
}
