use {
    crate::subscription::{
        item::stable_abi::{Trait, Trait_TO},
        service::Iteration,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        std_types::{
            RBox,
            ROption::{self, RNone, RSome},
            RSlice, RSliceMut, Tuple2,
        },
        StableAbi,
    },
    async_ffi::FfiFuture,
    core::{
        fmt::Debug,
        mem::size_of,
        pin::{pin, Pin},
        ptr::addr_of_mut,
        slice::from_raw_parts_mut,
        task::{Context, Poll},
    },
    fenrir_error::{connection_shutdown, Error},
    futures::Sink,
};

extern crate alloc;

pub(super) mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use abi_stable::{sabi_trait, std_types::RSliceMut};

    #[sabi_trait]
    pub(crate) trait Trait: Debug + Send + Sync + Unpin + 'static {
        fn sender_and_stream(&mut self) -> RSliceMut<'_, u8>;
    }
}

#[derive(Debug)]
pub(super) struct Inner<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
    iteration: Iteration<T, U>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Item<T, U> {
    inner: Trait_TO<RBox<()>>,
    next: ROption<FfiFuture<ROption<RSlice<'static, u8>>>>,
    _phantom_data: NonOwningPhantom<(T, U)>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<'a, U>
where
    U: StableAbi,
{
    subscription_message_sender: sleipnir::ffi::sync::mpsc::UnboundedSender<U>,
    _phantom_data: NonOwningPhantom<&'a ()>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Stream<'a, T>
where
    T: StableAbi,
{
    service_message_receiver: sleipnir::ffi::sync::broadcast::Receiver<T>,
    _phantom_data: NonOwningPhantom<&'a ()>,
}

impl<T, U> Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Unpin + 'static,
    U: Clone + Send + Sync + StableAbi + 'static,
{
    #[inline]
    pub(super) const fn new(iteration: Iteration<T, U>) -> Self {
        Self { iteration }
    }
}

impl<T, U> Item<T, U>
where
    T: StableAbi,
    U: Send + StableAbi,
{
    #[inline]
    pub(super) const fn new(inner: Trait_TO<RBox<()>>) -> Self {
        let next = RNone;

        Self {
            inner,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }

    /// Get a sender of messages to and stream for receiving messages from the connected
    /// subscription service.
    ///
    /// Calling this the first time returns the Sender and Stream. Consecutive calls return None.
    ///
    /// # Panics
    ///
    /// To get generics over the FFI-boundary we cast to and from &u8. If the cast back from a &u8
    /// fails, we panic.
    #[inline]
    pub fn sender_and_stream(&mut self) -> Option<(Sender<'_, U>, Stream<'_, T>)> {
        let mut bytes = self.inner.sender_and_stream();

        let pointer: *mut ROption<
            Tuple2<
                sleipnir::ffi::sync::mpsc::UnboundedSender<U>,
                sleipnir::ffi::sync::broadcast::Receiver<T>,
            >,
        > = bytes.as_mut_ptr().cast();

        // We constructed bytes in (Inner as Trait)::sender_and_stream from a non-null pointer of an instance
        // of what pointer points to.
        let maybe_sender_and_receiver = unsafe { pointer.as_mut() }
            .expect("pointer must not be null")
            .take();

        match maybe_sender_and_receiver {
            RNone => None,
            RSome(sender_and_receiver) => {
                let (subscription_message_sender, service_message_receiver) =
                    sender_and_receiver.into();

                Some((
                    Sender {
                        subscription_message_sender,
                        _phantom_data: NonOwningPhantom::NEW,
                    },
                    Stream {
                        service_message_receiver,
                        _phantom_data: NonOwningPhantom::NEW,
                    },
                ))
            }
        }
    }
}

impl<T, U> Trait for Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + 'static,
{
    #[inline]
    fn sender_and_stream(&mut self) -> RSliceMut<'_, u8> {
        unsafe {
            from_raw_parts_mut(
                addr_of_mut!(self.iteration.sender_and_receiver).cast(),
                size_of::<
                    ROption<
                        Tuple2<
                            sleipnir::ffi::sync::mpsc::UnboundedSender<U>,
                            sleipnir::ffi::sync::broadcast::Receiver<T>,
                        >,
                    >,
                >(),
            )
        }
        .into()
    }
}

impl<U> Sender<'_, U>
where
    U: StableAbi,
{
    #[inline]
    const fn process_poll(
        poll_result: Poll<Result<(), sleipnir::ffi::sync::Error>>,
    ) -> Poll<Result<(), Error>> {
        match poll_result {
            Poll::Pending => Poll::Pending,
            Poll::Ready(result) => Poll::Ready(Self::process_result(&result)),
        }
    }

    #[inline]
    const fn process_result(result: &Result<(), sleipnir::ffi::sync::Error>) -> Result<(), Error> {
        match result {
            Ok(()) => Ok(()),
            Err(_) => Err(connection_shutdown()),
        }
    }
}

impl<U> Sink<U> for Sender<'_, U>
where
    U: StableAbi,
{
    type Error = Error;

    #[inline]
    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::process_poll(pin!(&mut self.get_mut().subscription_message_sender).poll_close(cx))
    }

    #[inline]
    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::process_poll(pin!(&mut self.get_mut().subscription_message_sender).poll_flush(cx))
    }

    #[inline]
    fn poll_ready(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::process_poll(pin!(&mut self.get_mut().subscription_message_sender).poll_ready(cx))
    }

    #[inline]
    fn start_send(self: Pin<&mut Self>, item: U) -> Result<(), Self::Error> {
        Self::process_result(
            &pin!(&mut self.get_mut().subscription_message_sender).start_send(item),
        )
    }
}

impl<T> futures::Stream for Stream<'_, T>
where
    T: Clone + Debug + Send + StableAbi + Unpin + 'static,
{
    type Item = T;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        let service_message_receiver = &mut this.service_message_receiver;

        match pin!(service_message_receiver).poll_next(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(maybe_message) => maybe_message
                .map_or_else(|| Poll::Ready(None), |message| Poll::Ready(Some(message))),
        }
    }
}
