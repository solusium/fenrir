pub(super) use crate::subscription::service::item::stable_abi::Trait_TO;
use {
    crate::subscription::service::item::stable_abi::Trait,
    abi_stable::{
        marker_type::NonOwningPhantom,
        std_types::{
            RBox,
            ROption::{self, RNone, RSome},
            RResult::{self, RErr, ROk},
            RSliceMut, Tuple2,
        },
        StableAbi,
    },
    async_ffi::{FfiFuture, FutureExt},
    core::{
        fmt::Debug,
        future::{ready, Future},
        mem::{size_of, take, transmute},
        pin::{pin, Pin},
        task::{Context, Poll},
    },
    fenrir_error::{connection_shutdown, state_not_recoverable, Error},
    sleipnir::task::JoinHandle,
    std::{ptr::addr_of_mut, slice::from_raw_parts_mut},
};

extern crate alloc;

pub(super) mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{ROption, RResult, RSliceMut},
        },
        async_ffi::FfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    pub(crate) trait Trait: Debug + Send + Sync + Unpin + 'static {
        fn await_guard_drop_receiver(&mut self) -> FfiFuture<RResult<(), Error>>;

        fn await_join_handle(&mut self) -> FfiFuture<RResult<usize, Error>>;

        fn next(&mut self) -> FfiFuture<ROption<*const u8>>;

        fn sender_and_stream(&mut self) -> RSliceMut<'_, u8>;
    }
}

#[repr(C)]
#[derive(StableAbi)]
enum BroadcastingGuard {
    Future(FfiFuture<RResult<usize, Error>>),
    None,
}

#[derive(Debug)]
pub(super) struct Inner<T, U>
where
    T: Debug + StableAbi,
    U: StableAbi,
{
    broadcasting_guard: Option<JoinHandle<usize>>,
    guard_drop_receiver: Option<futures::channel::oneshot::Receiver<()>>,
    sender_and_receiver: ROption<
        Tuple2<
            sleipnir::ffi::sync::broadcast::Sender<T>,
            sleipnir::ffi::sync::mpsc::UnboundedReceiver<U>,
        >,
    >,
}

#[repr(C)]
#[derive(StableAbi)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub struct Item<T, U> {
    inner: Trait_TO<RBox<()>>,
    broadcasting_guard: BroadcastingGuard,
    guard_drop_receiver: ROption<FfiFuture<RResult<(), Error>>>,
    next: ROption<FfiFuture<ROption<*const u8>>>,
    _phantom_data: NonOwningPhantom<(T, U)>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<'a, T>
where
    T: StableAbi,
{
    service_message_sender: sleipnir::ffi::sync::broadcast::Sender<T>,
    _phantom_data: NonOwningPhantom<&'a ()>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Stream<'a, U>
where
    U: StableAbi,
{
    subscription_message_receiver: sleipnir::ffi::sync::mpsc::UnboundedReceiver<U>,
    _phantom_data: NonOwningPhantom<&'a ()>,
}

impl<T, U> Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Debug + Send + StableAbi + 'static,
{
    #[inline]
    pub(super) fn new(
        broadcasting_guard: JoinHandle<usize>,
        guard_drop_receiver: futures::channel::oneshot::Receiver<()>,
        service_message_sender: tokio::sync::broadcast::Sender<T>,
        subscription_message_receiver: futures::channel::mpsc::UnboundedReceiver<U>,
    ) -> Self {
        let broadcasting_guard = Some(broadcasting_guard);
        let guard_drop_receiver = Some(guard_drop_receiver);
        let sender_and_receiver = RSome(
            (
                service_message_sender.into(),
                subscription_message_receiver.into(),
            )
                .into(),
        );

        Self {
            broadcasting_guard,
            guard_drop_receiver,
            sender_and_receiver,
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn await_join_handle(
        maybe_join_handle: Option<JoinHandle<usize>>,
    ) -> Result<usize, Error> {
        match maybe_join_handle {
            None => Err(state_not_recoverable("join handle is None")),
            Some(maybe_join_handle) => match maybe_join_handle.await {
                Ok(subscriber_count) => Ok(subscriber_count),
                Err(error) => Err(error.into()),
            },
        }
    }
}

impl<T, U> Item<T, U>
where
    T: Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: StableAbi,
{
    #[inline]
    pub(super) const fn new(inner: Trait_TO<RBox<()>>) -> Self {
        let broadcasting_guard = BroadcastingGuard::None;
        let guard_drop_receiver = RNone;
        let next = RNone;

        Self {
            inner,
            broadcasting_guard,
            guard_drop_receiver,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }

    /// Get a sender of messages to and stream for receiving messages from the connected subscriptions.
    ///
    /// Calling this the first time returns the Sender and Stream. Consecutive calls return None.
    ///
    /// # Panics
    ///
    /// To get generics over the FFI-boundary we cast to and from &u8. If the cast back from a &u8
    /// fails, we panic.
    #[inline]
    pub fn sender_and_stream(&mut self) -> Option<(Sender<'_, T>, Stream<'_, U>)> {
        let mut bytes = self.inner.sender_and_stream();

        let pointer: *mut ROption<
            Tuple2<
                sleipnir::ffi::sync::broadcast::Sender<T>,
                sleipnir::ffi::sync::mpsc::UnboundedReceiver<U>,
            >,
        > = bytes.as_mut_ptr().cast();

        // We constructed bytes in (Inner as Trait)::sender_and_stream from a non-null pointer of an instance
        // of what pointer points to.
        let maybe_sender_and_receiver = unsafe { pointer.as_mut() }
            .expect("pointer must not be null")
            .take();

        match maybe_sender_and_receiver {
            RNone => None,
            RSome(sender_and_receiver) => {
                let (service_message_sender, subscription_message_receiver) =
                    sender_and_receiver.into();

                Some((
                    Sender {
                        service_message_sender,
                        _phantom_data: NonOwningPhantom::NEW,
                    },
                    Stream {
                        subscription_message_receiver,
                        _phantom_data: NonOwningPhantom::NEW,
                    },
                ))
            }
        }
    }
}

impl<T> Sender<'_, T>
where
    T: Send + StableAbi + Sync,
{
    #[inline]
    pub fn send(&self, message: T) -> Result<usize, Error> {
        self.service_message_sender
            .send(message)
            .map_or_else(|_| Err(connection_shutdown()), Ok)
    }
}

impl<T, U> Trait for Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
{
    #[inline]
    fn await_guard_drop_receiver(&mut self) -> FfiFuture<RResult<(), Error>> {
        let guard_drop_receiver = self
            .guard_drop_receiver
            .take()
            .expect("await_guard_drop_receiver called twice");

        async move { (guard_drop_receiver.await).map_or_else(|_| RErr(connection_shutdown()), ROk) }
            .into_ffi()
    }

    #[inline]
    fn await_join_handle(&mut self) -> FfiFuture<RResult<usize, Error>> {
        let broadcasting_guard = take(&mut self.broadcasting_guard);

        async move { Self::await_join_handle(broadcasting_guard).await.into() }.into_ffi()
    }

    #[inline]
    fn next(&mut self) -> FfiFuture<ROption<*const u8>> {
        unsafe { transmute(async move { self.next().await }.into_ffi()) }
    }

    #[inline]
    fn sender_and_stream(&mut self) -> RSliceMut<'_, u8> {
        unsafe {
            from_raw_parts_mut(
                addr_of_mut!(self.sender_and_receiver).cast(),
                size_of::<
                    ROption<
                        Tuple2<
                            sleipnir::ffi::sync::broadcast::Sender<T>,
                            sleipnir::ffi::sync::mpsc::UnboundedReceiver<U>,
                        >,
                    >,
                >(),
            )
        }
        .into()
    }
}

impl<T, U> Future for Item<T, U> {
    type Output = Result<(), Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        let broadcasting_guard_future = match &mut this.broadcasting_guard {
            BroadcastingGuard::None => {
                this.broadcasting_guard = BroadcastingGuard::Future(this.inner.await_join_handle());

                if let BroadcastingGuard::Future(future) = &mut this.broadcasting_guard {
                    future
                } else {
                    unreachable!()
                }
            }
            BroadcastingGuard::Future(future) => future,
        };

        match pin!(broadcasting_guard_future).poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(RErr(error)) => Poll::Ready(Err(error)),
            Poll::Ready(ROk(subscriber_count)) => {
                if 0 == subscriber_count {
                    Poll::Ready(Ok(()))
                } else {
                    let guard_drop_receiver = this
                        .guard_drop_receiver
                        .get_or_insert_with(|| this.inner.await_guard_drop_receiver());

                    match pin!(guard_drop_receiver).poll(cx) {
                        Poll::Pending => {
                            this.broadcasting_guard =
                                BroadcastingGuard::Future(ready(ROk(subscriber_count)).into_ffi());

                            Poll::Pending
                        }
                        Poll::Ready(guard_drop_result) => guard_drop_result.map_or_else(
                            |_| Poll::Ready(Err(connection_shutdown())),
                            |()| Poll::Ready(Ok(())),
                        ),
                    }
                }
            }
        }
    }
}

impl<U> futures::Stream for Stream<'_, U>
where
    U: Clone + Debug + Send + StableAbi + Unpin + 'static,
{
    type Item = U;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        pin!(&mut self.get_mut().subscription_message_receiver).poll_next(cx)
    }
}
