use {
    crate::subscription::service::message::stable_abi::{Trait, Trait_TO},
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            ROption::{self, RNone, RSome},
            RVec,
        },
        StableAbi,
    },
    alloc::sync::Arc,
    async_ffi::{FfiFuture, FutureExt},
    async_lock::Mutex,
    bytemuck::{bytes_of, from_bytes, Pod},
    core::{
        fmt::Debug,
        future::Future,
        pin::{pin, Pin},
        task::{Context, Poll},
    },
    futures::Stream,
};

extern crate alloc;

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{ROption, RVec},
        },
        async_ffi::FfiFuture,
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Unpin + 'static {
        fn next(&self) -> FfiFuture<ROption<RVec<u8>>>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    receiver: Arc<Mutex<tokio::sync::broadcast::Receiver<T>>>,
}

#[repr(C)]
#[derive(StableAbi)]
pub(crate) struct Receiver<T> {
    inner: Trait_TO<RBox<()>>,
    next: ROption<FfiFuture<ROption<RVec<u8>>>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Inner<T> {
    #[inline]
    #[cfg_attr(feature = "async_backtrace", async_backtrace::framed)]
    async fn next(receiver: Arc<Mutex<tokio::sync::broadcast::Receiver<T>>>) -> ROption<RVec<u8>>
    where
        T: Clone + Pod + Send,
    {
        (receiver.lock().await.recv().await)
            .map_or_else(|_| RNone, |value| RSome(bytes_of(&value).to_vec().into()))
    }
}

impl<T> Receiver<T>
where
    T: Debug,
{
    #[inline]
    #[allow(dead_code)]
    pub(crate) fn new(receiver: tokio::sync::broadcast::Receiver<T>) -> Self
    where
        T: Pod + Send,
    {
        let inner = Trait_TO::from_value(
            Inner {
                receiver: Arc::new(Mutex::new(receiver)),
            },
            TD_Opaque,
        );
        let next = RNone;

        Self {
            inner,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Trait for Inner<T>
where
    T: Debug + Pod + Send + 'static,
{
    #[inline]
    fn next(&self) -> FfiFuture<ROption<RVec<u8>>> {
        let receiver = self.receiver.clone();

        async move { Self::next(receiver).await }.into_ffi()
    }
}

impl<T> Stream for Receiver<T>
where
    T: Pod,
{
    type Item = T;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        let next_future = this.next.get_or_insert_with(|| this.inner.next());

        let pinned_next_future = pin!(next_future);

        match pinned_next_future.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(result) => match result {
                RNone => Poll::Ready(None),
                RSome(bytes) => Poll::Ready(Some(*from_bytes(&bytes))),
            },
        }
    }
}
