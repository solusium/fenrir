pub use crate::texture::state::State;

pub mod manager;
pub mod state;

pub type Manager = crate::texture::manager::Manager;
