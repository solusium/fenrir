pub(crate) mod manager;
pub mod operation;
pub mod resource;

pub(crate) mod state;

pub type Manager = crate::render_graph::manager::Manager;
