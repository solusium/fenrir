pub use crate::subscription::{item::Item, service::Service};
use {
    crate::subscription::{
        guard::Guard,
        service::Iteration,
        stable_abi::{Trait, Trait_TO},
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        rtry,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            ROption::{self, RNone},
            RResult::{self, RErr, ROk},
        },
        StableAbi,
    },
    alloc::sync::Arc,
    async_ffi::{FfiFuture, FutureExt},
    async_lock::Mutex,
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        pin::{pin, Pin},
        task::{Context, Poll},
    },
    fenrir_error::{connection_shutdown, Error},
    futures::Stream,
};

extern crate alloc;

mod guard;

pub mod item;
pub mod service;

#[cfg(test)]
mod tests;

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{RBox, RResult},
        },
        async_ffi::FfiFuture,
        fenrir_error::Error,
    };

    extern crate alloc;

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + Sync + Unpin + 'static {
        fn new_item_inner(
            &self,
        ) -> FfiFuture<RResult<crate::subscription::item::stable_abi::Trait_TO<RBox<()>>, Error>>;
    }
}

#[derive(Debug)]
struct Inner<T, U>
where
    T: Send + StableAbi,
    U: Send + StableAbi,
{
    receiver: Arc<Mutex<tokio::sync::broadcast::Receiver<Iteration<T, U>>>>,
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Subscription<T, U>
where
    T: StableAbi,
    U: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    next: ROption<
        FfiFuture<RResult<crate::subscription::item::stable_abi::Trait_TO<RBox<()>>, Error>>,
    >,
    _phantom_data: NonOwningPhantom<(T, U)>,
}

impl<T, U> Subscription<T, U>
where
    T: Debug + StableAbi + Send + Sync + Unpin + 'static,
    U: Debug + StableAbi + Send + Sync + Unpin + 'static,
{
    #[inline]
    const fn new(inner: Trait_TO<RBox<()>>) -> Self {
        let next = RNone;

        Self {
            inner,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T, U> Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + 'static,
{
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_item_inner(
        receiver: Arc<Mutex<tokio::sync::broadcast::Receiver<Iteration<T, U>>>>,
    ) -> RResult<crate::subscription::item::stable_abi::Trait_TO<RBox<()>>, Error> {
        let iteration =
            rtry!((receiver.lock().await.recv().await)
                .map_or_else(|_| RErr(connection_shutdown()), ROk));

        ROk(crate::subscription::item::stable_abi::Trait_TO::from_value(
            crate::subscription::item::Inner::<T, U>::new(iteration),
            TD_Opaque,
        ))
    }
}

impl<T, U> Trait for Inner<T, U>
where
    T: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
    U: Clone + Debug + Send + StableAbi + Sync + Unpin + 'static,
{
    #[inline]
    fn new_item_inner(
        &self,
    ) -> FfiFuture<RResult<crate::subscription::item::stable_abi::Trait_TO<RBox<()>>, Error>> {
        let receiver = self.receiver.clone();

        async move { Self::new_item_inner(receiver).await }.into_ffi()
    }
}

impl<T, U> Debug for Subscription<T, U>
where
    T: StableAbi,
    U: StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Subscription")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

unsafe impl<T, U> Send for Subscription<T, U>
where
    T: StableAbi,
    U: StableAbi,
{
}
unsafe impl<T, U> Sync for Subscription<T, U>
where
    T: StableAbi,
    U: StableAbi,
{
}

impl<T, U> Stream for Subscription<T, U>
where
    T: StableAbi + Unpin,
    U: StableAbi + Send,
{
    type Item = Result<Item<T, U>, Error>;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        let next = this.next.get_or_insert(this.inner.new_item_inner());

        let pinned_next = pin!(next);

        match pinned_next.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(item_inner_result) => {
                this.next = RNone;

                match item_inner_result {
                    ROk(item_inner) => Poll::Ready(Some(Ok(Item::new(item_inner)))),
                    RErr(error) => Poll::Ready(Some(Err(error))),
                }
            }
        }
    }
}
