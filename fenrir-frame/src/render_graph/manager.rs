use {
    crate::{
        graphics::pipeline::State as GraphicsPipelineState,
        pipeline::{
            filter::{callback::stable_abi::FutureGeneratorTrait, connection::Connection},
            Pipeline, Stage,
        },
        render_graph::{
            manager::{
                prepare_task::PrepareTask,
                render_task::RenderTask,
                stable_abi::{Trait, Trait_TO},
            },
            operation,
        },
        texture,
    },
    abi_stable::{
        sabi_trait,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RSlice,
        },
        StableAbi,
    },
    alloc::sync::Arc,
    async_ffi::BorrowingFfiFuture,
    async_lock::Mutex,
    core::{fmt::Debug, mem::take},
    fenrir_error::{connection_shutdown, invalid_argument, Error},
    fenrir_hal::{
        graphics::{pipeline::Metadata as GraphisPipelineMetadata, Pipeline as GraphicsPipeline},
        render_pass,
    },
    fenrir_render_graph::{operation::Operation, resource::Resource, Graph},
    fenrir_shader_compiler::Error as ShaderCompilerError,
    futures::{
        channel::mpsc::{channel, unbounded, Receiver, Sender, UnboundedReceiver, UnboundedSender},
        future::join_all,
        SinkExt, StreamExt,
    },
    sleipnir::runtime::Handle,
    smallvec::SmallVec,
    std::sync::mpsc::sync_channel,
};
extern crate alloc;

mod prepare_task;
mod render_task;

#[cfg(test)]
mod tests;

type OperationState = crate::render_graph::operation::state::State;
type ResourceMetadata = fenrir_render_graph::resource::Metadata;
type ResourceState = crate::render_graph::resource::state::State;

enum Message {
    NewGraphicsPipeline(
        OperationState,
        GraphisPipelineMetadata<'static, 'static>,
        Sender<Result<GraphicsPipelineState, ShaderCompilerError>>,
        Manager,
    ),
    NewGraphicsPipelineContinuation(
        GraphicsPipeline,
        [usize; 2],
        Sender<Result<GraphicsPipelineState, ShaderCompilerError>>,
        Manager,
    ),
    NewOperation(
        fenrir_hal::render_pass::Metadata,
        Sender<Result<OperationState, Error>>,
        Manager,
    ),
    NewOperationContinuation(
        Operation,
        [usize; 2],
        Sender<Result<OperationState, Error>>,
        Manager,
    ),
    NewResource(
        ResourceMetadata,
        crate::render_graph::resource::data::State,
        Sender<Result<ResourceState, Error>>,
        Manager,
    ),
    NewResourceContinuation(
        Resource,
        crate::render_graph::resource::data::State,
        [usize; 2],
        Sender<Result<ResourceState, Error>>,
        Manager,
    ),
    RenderPassMetadata(
        OperationState,
        Sender<Result<fenrir_hal::render_pass::Metadata, Error>>,
    ),
    Write(ResourceState, OperationState, Sender<Result<(), Error>>),
    InsertOperationsAndResources(
        SmallVec<[(Operation, OperationState); 1]>,
        SmallVec<[(Resource, ResourceState); 1]>,
        Sender<Result<(), Error>>,
    ),
    WritesTo(OperationState, ResourceState, Sender<Result<bool, Error>>),
    DropOperation([usize; 2]),
    DropOperationContinuation([usize; 2]),
    DropResource([usize; 2]),
    DropResourceContinuation([usize; 2]),
    DrainedMessages,
}

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::{
            graphics::pipeline::State as GraphicsPipelineState,
            render_graph::manager::{Manager, OperationState, ResourceMetadata, ResourceState},
        },
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RSlice},
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
        fenrir_hal::graphics::pipeline::Metadata as GraphisPipelineMetadata,
        fenrir_shader_compiler::Error as ShaderCompilerError,
    };

    #[sabi_trait]
    pub(super) trait Trait: Clone + Debug + Send + Sync + 'static {
        fn drop_operation(&self, indices: RSlice<'_, usize>) -> RResult<(), Error>;

        fn drop_resource(&self, indices: RSlice<'_, usize>) -> RResult<(), Error>;

        fn new_graphics_pipeline(
            &self,
            operation: OperationState,
            metadata: GraphisPipelineMetadata<'static, 'static>,
            manager: Manager,
        ) -> BorrowingFfiFuture<'_, RResult<GraphicsPipelineState, ShaderCompilerError>>;

        fn new_operation(
            &self,
            metadata: fenrir_hal::render_pass::Metadata,
            manager: Manager,
        ) -> BorrowingFfiFuture<RResult<OperationState, Error>>;

        fn new_resource(
            &self,
            manager: Manager,
            metadata: ResourceMetadata,
            data: crate::render_graph::resource::data::State,
        ) -> BorrowingFfiFuture<RResult<ResourceState, Error>>;

        fn render_pass_metadata(
            &self,
            operation: OperationState,
        ) -> BorrowingFfiFuture<RResult<fenrir_hal::render_pass::Metadata, Error>>;

        fn write(
            &self,
            resource: ResourceState,
            operation: OperationState,
        ) -> BorrowingFfiFuture<RResult<(), Error>>;

        fn writes_to(
            &self,
            operation: OperationState,
            resource: ResourceState,
        ) -> BorrowingFfiFuture<RResult<bool, Error>>;
    }
}

struct Data {
    receiver: Arc<Mutex<UnboundedReceiver<Message>>>,
    cache: Vec<Message>,
    operations: Vec<Option<Operation>>,
    resources: Vec<Option<Resource>>,
    resource_data_states: Vec<Option<crate::render_graph::resource::data::State>>,
    graphics_pipelines: Vec<Option<GraphicsPipeline>>,
    graph: Graph,
    sender: UnboundedSender<Message>,
    index: usize,
}

#[derive(Debug)]
struct Inner {
    sender: UnboundedSender<Message>,
    connections: Arc<Mutex<Option<[Connection; 2]>>>,
    runtime: Handle,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Manager {
    inner: Trait_TO<RArc<()>>,
}

impl Inner {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_graphics_pipeline(
        &self,
        operation: OperationState,
        metadata: GraphisPipelineMetadata<'static, 'static>,
        manager: Manager,
    ) -> Result<GraphicsPipelineState, ShaderCompilerError> {
        let (sender, mut receiver) = channel(1);

        let message_send_result = self.sender.unbounded_send(Message::NewGraphicsPipeline(
            operation, metadata, sender, manager,
        ));

        match message_send_result {
            Err(_) => Err(ShaderCompilerError::FenrirError(connection_shutdown())),
            Ok(()) => (receiver.next().await).map_or_else(
                || Err(ShaderCompilerError::FenrirError(connection_shutdown())),
                |state| state,
            ),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_operation(
        &self,
        metadata: render_pass::Metadata,
        manager: Manager,
    ) -> Result<operation::state::State, Error> {
        let (sender, mut receiver) = channel(1);

        let message_send_result = self
            .sender
            .unbounded_send(Message::NewOperation(metadata, sender, manager));

        match message_send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => {
                (receiver.next().await).map_or_else(|| Err(connection_shutdown()), |state| state)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_resource(
        &self,
        manager: Manager,
        metadata: ResourceMetadata,
        data: crate::render_graph::resource::data::State,
    ) -> Result<ResourceState, Error> {
        let (sender, mut receiver) = channel(1);

        let message_send_result = self
            .sender
            .unbounded_send(Message::NewResource(metadata, data, sender, manager));

        match message_send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => {
                (receiver.next().await).map_or_else(|| Err(connection_shutdown()), |state| state)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn render_pass_metadata(
        &self,
        operation: OperationState,
    ) -> Result<fenrir_hal::render_pass::Metadata, Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = self
            .sender
            .unbounded_send(Message::RenderPassMetadata(operation, sender));

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => {
                (receiver.next().await).map_or_else(|| Err(connection_shutdown()), |result| result)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn write(&self, resource: ResourceState, operation: OperationState) -> Result<(), Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = self
            .sender
            .unbounded_send(Message::Write(resource, operation, sender));

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => {
                (receiver.next().await).map_or_else(|| Err(connection_shutdown()), |result| result)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn writes_to(
        &self,
        operation: OperationState,
        resource: ResourceState,
    ) -> Result<bool, Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = self
            .sender
            .unbounded_send(Message::WritesTo(operation, resource, sender));

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => {
                (receiver.next().await).map_or_else(|| Err(connection_shutdown()), |result| result)
            }
        }
    }
}

impl Manager {
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return [`fenrir_error::Error::InvalidArgument`] if the Manager
    /// cannot connect the callbacks to the [`Stage`]s.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new(
        pipeline: Pipeline,
        texture_manager: texture::manager::Manager,
    ) -> Result<Self, Error> {
        let mut data_channels = (0..2)
            .map(|_| channel(2))
            .collect::<SmallVec<[(Sender<Data>, Receiver<Data>); 2]>>();

        let (sender, message_receiver) = unbounded();

        let message_receiver_arc = Arc::new(Mutex::new(message_receiver));

        {
            let operations = [Vec::new(), Vec::new()];

            let resources = [Vec::new(), Vec::new()];

            let resource_state_datas = [Vec::new(), Vec::new()];

            let graphics_pipelines = [Vec::new(), Vec::new()];

            let graph = Graph::new(&[], &[]).await?;

            let graphs = [graph.clone(), graph];

            let first_data_sender_ref = &data_channels[0].0;

            let message_sender_ref = &sender;
            let message_receiver_ref = &message_receiver_arc;

            let bootstrap_data_futures = operations
                .into_iter()
                .zip(resources.into_iter())
                .zip(resource_state_datas.into_iter())
                .zip(graphics_pipelines.into_iter())
                .zip(graphs.into_iter())
                .enumerate()
                .map(
                    |(
                        index,
                        (
                            (((operations, resources), resource_data_states), graphics_pipelines),
                            graph,
                        ),
                    )| async move {
                        first_data_sender_ref
                            .clone()
                            .send(Data {
                                receiver: message_receiver_ref.clone(),
                                cache: Vec::new(),
                                operations,
                                resources,
                                resource_data_states,
                                graphics_pipelines,
                                graph,
                                sender: message_sender_ref.clone(),
                                index,
                            })
                            .await
                    },
                );

            let bootstrap_data_results = join_all(bootstrap_data_futures).await;

            bootstrap_data_results
                .into_iter()
                .try_fold((), |(), current| match current {
                    Ok(()) => Ok(()),
                    Err(_) => Err(connection_shutdown()),
                })?;
        }

        data_channels.reverse();

        let Some((first_data_sender, first_data_receiver)) = data_channels.pop() else {
            unreachable!()
        };
        let Some((second_data_sender, second_data_receiver)) = data_channels.pop() else {
            unreachable!()
        };

        let prepare_task =
            PrepareTask::new(first_data_receiver, second_data_sender, &texture_manager).await;
        let render_task =
            RenderTask::new(second_data_receiver, first_data_sender, &texture_manager).await;

        let connections = Arc::new(Mutex::new(Some([
            Self::connect_to_pipeline(&pipeline, Stage::Prepare, prepare_task).await?,
            Self::connect_to_pipeline(&pipeline, Stage::Render, render_task).await?,
        ])));

        let runtime = Handle::current();

        Ok(Self {
            inner: Trait_TO::from_ptr(
                RArc::new(Inner {
                    sender,
                    connections,
                    runtime,
                }),
                sabi_trait::TD_Opaque,
            ),
        })
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn connect_to_pipeline<FG>(
        pipeline: &Pipeline,
        stage: Stage,
        future_generator: FG,
    ) -> Result<Connection, Error>
    where
        FG: FutureGeneratorTrait + 'static,
    {
        let connect_result = pipeline
            .connect(
                stage,
                future_generator,
                "fenrir_frame::render_graph::manager::Manager",
            )
            .await;

        match connect_result {
            ROk(connection) => Ok(connection),
            RErr(error) => Err(error),
        }
    }

    #[inline]
    pub(crate) fn drop_operation(&self, indices: [usize; 2]) -> Result<(), Error> {
        self.inner.drop_operation(indices.as_slice().into()).into()
    }

    #[inline]
    pub(crate) fn drop_resource(&self, indices: [usize; 2]) -> Result<(), Error> {
        self.inner.drop_resource(indices.as_slice().into()).into()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new_graphics_pipeline(
        &self,
        operation: OperationState,
        metadata: GraphisPipelineMetadata<'static, 'static>,
    ) -> Result<GraphicsPipelineState, ShaderCompilerError> {
        self.inner
            .new_graphics_pipeline(operation, metadata, self.clone())
            .await
            .into()
    }

    /// Construct a new [`operation::State`](crate::render_graph::operation::state::State).
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return an error if the API wrapped by the operation couldn't
    /// construct the wrapped structs.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new_operation(
        &self,
        metadata: render_pass::Metadata,
    ) -> Result<crate::render_graph::operation::state::State, Error> {
        self.inner
            .new_operation(metadata, self.clone())
            .await
            .into()
    }

    /// Construct a new [`resource::State`](crate::render_graph::resource::state::State).
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return [`fenrir_error::Error::InvalidArgument`] if data doesn't
    /// represent a valid state.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new_resource(
        &self,
        metadata: ResourceMetadata,
        data: crate::render_graph::resource::data::State,
    ) -> Result<crate::render_graph::resource::state::State, Error> {
        self.inner
            .new_resource(self.clone(), metadata, data)
            .await
            .into()
    }

    /// Get the [`Metadata`](fenrir_hal::render_pass::Metadata) of the underlying render pass.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return [`fenrir_error::Error::InvalidArgument`] if operation doesn't
    /// represent a valid operation state.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn render_pass_metadata(
        &self,
        operation: OperationState,
    ) -> Result<fenrir_hal::render_pass::Metadata, Error> {
        self.inner.render_pass_metadata(operation).await.into()
    }

    /// Let this operation write to resource.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if resource or operation
    /// doesn't represent a valid resource or operation state, respectively.
    /// This function constructs new [`Operations`](fenrir_render_graph::operation::Operation) and
    /// might return an error if the API wrapped by the operation couldn't construct the wrapped
    /// structs.
    /// This function might also return [`fenrir_error::Error::StateNotRecoverable`] if some
    /// internal data got dropped or another operation already writes to resource.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn write(
        &self,
        resource: ResourceState,
        operation: OperationState,
    ) -> Result<(), Error> {
        self.inner.write(resource, operation).await.into()
    }

    /// Queries if operation writes to resource.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if resource or operation
    /// doesn't represent a valid resource or operation state, respectively.
    ///
    /// Those errors arise probably because of bugs and we should report them.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn writes_to(
        &self,
        operation: OperationState,
        resource: ResourceState,
    ) -> Result<bool, Error> {
        self.inner.writes_to(operation, resource).await.into()
    }
}

impl Clone for Inner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!();
    }
}

impl Drop for Inner {
    #[inline]
    fn drop(&mut self) {
        let connections_arc = self.connections.clone();

        let (sender, receiver) = sync_channel(1);

        let _join_handle = self.runtime.spawn(async move {
            let mut maybe_connections = connections_arc.lock().await;

            match take(&mut *maybe_connections) {
                None => unreachable!(),
                Some(connections) => {
                    for connection in connections {
                        let _disconnect_result = connection.disconnect().await;
                    }
                }
            }

            drop(maybe_connections);

            if sender.send(()).is_err() {
                unreachable!();
            }
        });

        if receiver.recv().is_err() {
            unreachable!()
        }
    }
}

impl Trait for Inner {
    #[inline]
    fn drop_operation(&self, index_slice: RSlice<'_, usize>) -> RResult<(), Error> {
        index_slice.as_slice().try_into().map_or_else(
            |_| {
                RErr(invalid_argument(
                    "couldn't convert index_slice from RSlice<'_, usize> to &[usize]",
                ))
            },
            |indices| {
                self.sender
                    .unbounded_send(Message::DropOperation(indices))
                    .map_or_else(|_| RErr(connection_shutdown()), ROk)
            },
        )
    }

    #[inline]
    fn drop_resource(&self, index_slice: RSlice<'_, usize>) -> RResult<(), Error> {
        index_slice.as_slice().try_into().map_or_else(
            |_| {
                RErr(invalid_argument(
                    "couldn't convert index_slice from RSlice<'_, usize> to &[usize]",
                ))
            },
            |indices| {
                self.sender
                    .unbounded_send(Message::DropResource(indices))
                    .map_or_else(|_| RErr(connection_shutdown()), ROk)
            },
        )
    }

    #[inline]
    fn new_graphics_pipeline(
        &self,
        operation: OperationState,
        metadata: GraphisPipelineMetadata<'static, 'static>,
        manager: Manager,
    ) -> BorrowingFfiFuture<RResult<GraphicsPipelineState, ShaderCompilerError>> {
        async_ffi::FutureExt::into_ffi(async move {
            self.new_graphics_pipeline(operation, metadata, manager)
                .await
                .into()
        })
    }

    #[inline]
    fn new_operation(
        &self,
        metadata: render_pass::Metadata,
        manager: Manager,
    ) -> BorrowingFfiFuture<RResult<operation::state::State, Error>> {
        async_ffi::FutureExt::into_ffi(
            async move { self.new_operation(metadata, manager).await.into() },
        )
    }

    #[inline]
    fn new_resource(
        &self,
        manager: Manager,
        metadata: ResourceMetadata,
        data: crate::render_graph::resource::data::State,
    ) -> BorrowingFfiFuture<RResult<ResourceState, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            self.new_resource(manager, metadata, data).await.into()
        })
    }

    #[inline]
    fn render_pass_metadata(
        &self,
        operation: OperationState,
    ) -> BorrowingFfiFuture<RResult<fenrir_hal::render_pass::Metadata, Error>> {
        async_ffi::FutureExt::into_ffi(
            async move { self.render_pass_metadata(operation).await.into() },
        )
    }

    #[inline]
    fn write(
        &self,
        resource: ResourceState,
        operation: operation::state::State,
    ) -> BorrowingFfiFuture<RResult<(), Error>> {
        async_ffi::FutureExt::into_ffi(async move { self.write(resource, operation).await.into() })
    }

    #[inline]
    fn writes_to(
        &self,
        operation: OperationState,
        resource: ResourceState,
    ) -> BorrowingFfiFuture<RResult<bool, Error>> {
        async_ffi::FutureExt::into_ffi(
            async move { self.writes_to(operation, resource).await.into() },
        )
    }
}
