use {
    crate::{
        pipeline::filter::callback::stable_abi::FutureGeneratorTrait,
        render_graph::{
            manager::{Manager, Message},
            operation::state::State as OperationState,
            state::StateTrait,
        },
    },
    abi_stable::std_types::RResult,
    async_ffi::BorrowingFfiFuture,
    core::{any::type_name, clone::Clone, fmt::Debug, mem::swap, ops::Range},
    fenrir_error::{
        argument_out_of_domain, connection_shutdown, invalid_argument, state_not_recoverable, Error,
    },
    fenrir_hal::{
        color::blend::State as ColorBlendState,
        graphics::pipeline::{Metadata as GraphicsPipelineMetadata, Pipeline as GraphicsPipeline},
    },
    fenrir_render_graph::{operation::Operation, resource::Resource, Graph},
    fenrir_shader_compiler::Error as ErrorWrapper,
    futures::{
        channel::mpsc::{Receiver, Sender},
        stream::{self, select},
        FutureExt, SinkExt, Stream, StreamExt,
    },
    smallvec::{smallvec, SmallVec},
};

extern crate alloc;

enum MessageWrapper {
    TextureManagerMessage(crate::texture::manager::prepare_task::subscription::service::Message),
    RenderGraphManagerMessage(crate::render_graph::manager::Message),
}

#[derive(Debug)]
struct CallbackData {
    receiver: futures::channel::mpsc::Receiver<crate::render_graph::manager::Data>,
    texture_manager_subscription: crate::texture::manager::prepare_task::Subscription,
    sender: Sender<crate::render_graph::manager::Data>,
}

#[derive(Debug)]
pub(super) struct PrepareTask {
    callback_data_receiver: futures::channel::oneshot::Receiver<CallbackData>,
}

impl PrepareTask {
    #[inline]
    pub(super) async fn new(
        receiver: futures::channel::mpsc::Receiver<crate::render_graph::manager::Data>,
        sender: futures::channel::mpsc::Sender<crate::render_graph::manager::Data>,
        texture_manager: &crate::texture::manager::Manager,
    ) -> Self {
        let (callback_data_sender, callback_data_receiver) = futures::channel::oneshot::channel();

        let texture_manager_subscription = texture_manager.prepare_task().subscribe().await;

        callback_data_sender
            .send(CallbackData {
                receiver,
                texture_manager_subscription,
                sender,
            })
            .expect("we constructed the receiving half in this scope");

        Self {
            callback_data_receiver,
        }
    }

    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn callback(
        callback_data_receiver: futures::channel::oneshot::Receiver<CallbackData>,
        callback_data_sender: futures::channel::oneshot::Sender<CallbackData>,
    ) -> Result<(), Error> {
        let mut callback_data = callback_data_receiver
            .await
            .map_or_else(|_| Err(connection_shutdown()), Ok)?;

        let mut data = Self::get_data(&mut callback_data.receiver).await?;

        Self::get_messages_into_cache(&mut data).await?;

        let mut cache = Vec::new();

        swap(&mut data.cache, &mut cache);

        data.cache =
            Self::construct_subscription_and_process_messages(cache, &mut callback_data, &mut data)
                .await?;

        callback_data
            .sender
            .send(data)
            .await
            .map_or_else(|_| Err(connection_shutdown()), Ok)?;

        match callback_data_sender.send(callback_data) {
            Ok(()) => Ok(()),
            Err(_) => Err(connection_shutdown()),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn construct_subscription_and_process_messages(
        mut messages: Vec<crate::render_graph::manager::Message>,
        callback_data: &mut CallbackData,
        data: &mut crate::render_graph::manager::Data,
    ) -> Result<Vec<crate::render_graph::manager::Message>, Error> {
        #[allow(clippy::iter_with_drain)]
        let render_graph_manager_messages = stream::iter(messages.drain(..))
            .map(MessageWrapper::RenderGraphManagerMessage)
            .chain(stream::iter([MessageWrapper::RenderGraphManagerMessage(
                crate::render_graph::manager::Message::DrainedMessages,
            )]));

        let mut texture_manager_subscription_item = callback_data
            .texture_manager_subscription
            .next()
            .await
            .map_or_else(|| Err(connection_shutdown()), Ok)??;

        let (
            mut texture_manager_subscription_message_sender,
            texture_manager_subscription_service_messages,
        ) = texture_manager_subscription_item
            .sender_and_stream()
            .map_or_else(
                || {
                    Err(state_not_recoverable(
                        "sender_and_stream must only be called once",
                    ))
                },
                Ok,
            )?;

        let mut streams = select(
            render_graph_manager_messages,
            texture_manager_subscription_service_messages
                .map(MessageWrapper::TextureManagerMessage),
        );

        Self::process_wrapped_messages(
            &mut streams,
            data,
            &mut texture_manager_subscription_message_sender,
        )
        .await;

        let (_, wrapped_texture_manager_subscription_service_messages) = streams.into_inner();

        let mut texture_manager_subscription_service_messages =
            wrapped_texture_manager_subscription_service_messages.into_inner();

        while let Some(message) = texture_manager_subscription_service_messages.next().await {
            match message {
                    crate::texture::manager::prepare_task::subscription::service::Message::ModifiedTexture => todo!(),
                }
        }

        let operations = data
            .operations
            .iter()
            .filter_map(Clone::clone)
            .collect::<Vec<_>>();

        let resources = data
            .resources
            .iter()
            .filter_map(Clone::clone)
            .collect::<Vec<_>>();

        data.graph = Graph::new(&operations, &resources).await?;

        Ok(messages)
    }

    #[inline]
    fn check_unfailable_result<T, E>(result: &Result<T, E>) {
        if result.is_err() {
            unreachable!();
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop_operation(data: &mut crate::render_graph::manager::Data, indices: [usize; 2]) {
        Self::remove(&mut data.operations, indices, data.index);

        Self::check_unfailable_result(
            &data
                .sender
                .send(Message::DropOperationContinuation(indices))
                .await,
        );
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop_resource(data: &mut crate::render_graph::manager::Data, indices: [usize; 2]) {
        Self::remove(&mut data.resources, indices, data.index);

        Self::remove(&mut data.resource_data_states, indices, data.index);

        Self::check_unfailable_result(
            &data
                .sender
                .send(Message::DropResourceContinuation(indices))
                .await,
        );
    }

    #[inline]
    fn emplace<T>(
        t: T,
        vector: &mut Vec<Option<T>>,
        mut indices: [usize; 2],
        data_index: usize,
    ) -> [usize; 2]
    where
        T: Send,
    {
        let index = vector
            .iter_mut()
            .position(|entry| entry.is_none())
            .map_or_else(
                || {
                    let index_ = vector.len();

                    vector.push(None);

                    index_
                },
                |index_| index_,
            );

        vector[index] = Some(t);

        indices[data_index] = index;

        indices
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_data(
        receiver: &mut Receiver<crate::render_graph::manager::Data>,
    ) -> Result<crate::render_graph::manager::Data, Error> {
        (receiver.next().await).map_or_else(|| Err(connection_shutdown()), Ok)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_messages_into_cache(
        data: &mut crate::render_graph::manager::Data,
    ) -> Result<(), Error> {
        let mut data_receiver = data.receiver.lock().await;

        while let Some(maybe_message) = data_receiver.next().now_or_never() {
            match maybe_message {
                None => {
                    return Err(connection_shutdown());
                }
                Some(message) => {
                    data.cache.push(message);
                }
            }
        }

        Ok(())
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_resource_data<'a>(
        state: &crate::render_graph::resource::data::State,
        texture_manager_subscription_item_sender: &mut crate::texture::manager::prepare_task::subscription::item::Sender<'a>,
    ) -> Result<fenrir_render_graph::resource::Data, Error> {
        match state {
            crate::render_graph::resource::data::State::ColorAttachment(state_, _)
            | crate::render_graph::resource::data::State::Texture(state_) => {
                texture_manager_subscription_item_sender
                    .query_texture(state_.clone())
                    .await
            }
        }
        .and_then(|texture| match state {
            crate::render_graph::resource::data::State::ColorAttachment(_, metadata) => {
                Ok(fenrir_render_graph::resource::Data::ColorAttachment(
                    fenrir_hal::color::Attachment::new(
                        texture.view(metadata.mapping(), metadata.subresource_range())?,
                        None,
                        metadata.load_operation(),
                        metadata.store_operation(),
                        metadata.clear_value(),
                        ColorBlendState::builder().build(),
                    ),
                ))
            }
            crate::render_graph::resource::data::State::Texture(_) => {
                Ok(fenrir_render_graph::resource::Data::Texture(texture))
            }
        })
    }

    #[inline]
    fn insert<T, U>(
        (new_t, state): (T, U),
        vector: &mut [Option<T>],
        index: usize,
    ) -> Result<(), Error>
    where
        T: Send,
        U: Debug + Send + StateTrait,
    {
        let maybe_entry = vector.get_mut(state.indices()[index]);

        match maybe_entry {
            None => Err(Self::not_a_valid_state_of(state, type_name::<T>())),
            Some(maybe_t) => maybe_t.as_mut().map_or_else(
                || Err(Self::not_a_valid_state_of(state, type_name::<T>())),
                |t| {
                    *t = new_t;

                    Ok(())
                },
            ),
        }
    }

    #[inline]
    fn insert_multiple<T, U>(
        ts_and_states: SmallVec<[(T, U); 1]>,
        vector: &mut [Option<T>],
        index: usize,
    ) -> Result<(), Error>
    where
        T: Send,
        U: Debug + Send + StateTrait,
    {
        for t_and_state in ts_and_states {
            Self::insert(t_and_state, vector, index)?;
        }

        Ok(())
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn insert_operations_and_resources(
        data: &mut crate::render_graph::manager::Data,
        operations_and_states: SmallVec<
            [(Operation, crate::render_graph::operation::state::State); 1],
        >,
        resources_and_states: SmallVec<
            [(Resource, crate::render_graph::resource::state::State); 1],
        >,
        mut sender: Sender<Result<(), Error>>,
    ) {
        let result = [
            Self::insert_multiple(operations_and_states, &mut data.operations, data.index),
            Self::insert_multiple(resources_and_states, &mut data.resources, data.index),
        ]
        .into_iter()
        .try_fold((), |(), current| current);

        Self::check_unfailable_result(&sender.send(result).await);
    }

    #[inline]
    fn insert_resource_data(
        data: &mut crate::render_graph::manager::Data,
        resource_data_state: crate::render_graph::resource::data::State,
        indices: [usize; 2],
    ) {
        let resource_data_states = &mut data.resource_data_states;

        let index = indices[data.index];

        if resource_data_states.len() > index {
            assert!(resource_data_states[index].is_none());

            resource_data_states[index] = Some(resource_data_state);
        } else {
            assert!(resource_data_states.len() == index);

            resource_data_states.push(Some(resource_data_state));
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_graphics_pipeline(
        operation_state: OperationState,
        metadata: GraphicsPipelineMetadata<'static, 'static>,
        operations: &[Option<Operation>],
        graphics_pipelines: &mut Vec<Option<GraphicsPipeline>>,
        data_index: usize,
    ) -> Result<(GraphicsPipeline, [usize; 2]), ErrorWrapper> {
        let index = operation_state.indices()[data_index];

        match operations.get(index) {
            None => Err(ErrorWrapper::FenrirError(argument_out_of_domain(
                Range {
                    start: 0.into(),
                    end: operations.len().into(),
                },
                index.into(),
            ))),
            Some(maybe_operation) => match maybe_operation {
                None => Err(ErrorWrapper::FenrirError(invalid_argument(&format!(
                    "entry at {index} doesn't contain an operation"
                )))),
                Some(operation) => match operation.new_graphics_pipeline(metadata).await {
                    Err(error) => Err(error),
                    Ok(graphics_pipeline) => Ok((
                        graphics_pipeline.clone(),
                        Self::emplace(graphics_pipeline, graphics_pipelines, [0; 2], data_index),
                    )),
                },
            },
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_operation(
        metadata: fenrir_hal::render_pass::Metadata,
        operations: &mut Vec<Option<Operation>>,
        data_index: usize,
    ) -> Result<(Operation, [usize; 2]), Error> {
        let operation = Operation::new(metadata).await?;

        Ok((
            operation.clone(),
            Self::emplace(operation, operations, [0; 2], data_index),
        ))
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_resource<'a>(
        data: &mut crate::render_graph::manager::Data,
        resource_data_state: crate::render_graph::resource::data::State,
        texture_manager_subscription_item_sender: &mut crate::texture::manager::prepare_task::subscription::item::Sender<'a>,
        metadata: fenrir_render_graph::resource::Metadata,
        mut sender: futures::channel::mpsc::Sender<
            Result<crate::render_graph::resource::state::State, Error>,
        >,
        manager: Manager,
    ) {
        let resource_data_result = Self::get_resource_data(
            &resource_data_state,
            texture_manager_subscription_item_sender,
        )
        .await;

        match resource_data_result {
            Err(error) => Self::check_unfailable_result(&sender.send(Err(error)).await),
            Ok(resource_data) => {
                let resource = Resource::new(resource_data, metadata).await;

                let indices =
                    Self::emplace(resource.clone(), &mut data.resources, [0; 2], data.index);

                Self::insert_resource_data(data, resource_data_state.clone(), indices);

                Self::check_unfailable_result(
                    &data
                        .sender
                        .send(Message::NewResourceContinuation(
                            resource,
                            resource_data_state,
                            indices,
                            sender,
                            manager,
                        ))
                        .await,
                );
            }
        }
    }

    #[cold]
    #[inline]
    fn not_a_valid_state_of<S>(state: S, type_name: &str) -> Error
    where
        S: Debug,
    {
        invalid_argument(&format!(
            "{state:?} not representing a valid {type_name:?}."
        ))
    }

    #[inline]
    #[allow(clippy::too_many_lines)]
    async fn process_message<'a>(
        message: Message,
        data: &mut crate::render_graph::manager::Data,
        texture_manager_subscription_message_sender:
            &mut crate::texture::manager::prepare_task::subscription::item::Sender<'a>,
    ) {
        match message {
            Message::NewGraphicsPipeline(operation, metadata, mut sender, manager) => {
                let pipeline_result = Self::new_graphics_pipeline(
                    operation,
                    metadata,
                    &data.operations,
                    &mut data.graphics_pipelines,
                    data.index,
                )
                .await;

                match pipeline_result {
                    Err(error) => Self::check_unfailable_result(&sender.send(Err(error)).await),
                    Ok((pipeline, indices)) => Self::check_unfailable_result(
                        &data
                            .sender
                            .send(Message::NewGraphicsPipelineContinuation(
                                pipeline, indices, sender, manager,
                            ))
                            .await,
                    ),
                }
            }
            Message::NewGraphicsPipelineContinuation(pipeline, indices, mut sender, manager) => {
                Self::check_unfailable_result(
                    &sender
                        .send(Ok(crate::graphics::pipeline::State::new(
                            manager,
                            Self::emplace(
                                pipeline,
                                &mut data.graphics_pipelines,
                                indices,
                                data.index,
                            ),
                        )))
                        .await,
                );
            }
            Message::NewOperation(metadata, mut sender, manager) => {
                let operation_result =
                    Self::new_operation(metadata.clone(), &mut data.operations, data.index).await;

                match operation_result {
                    Err(error) => Self::check_unfailable_result(&sender.send(Err(error)).await),
                    Ok((operation, indices)) => Self::check_unfailable_result(
                        &data
                            .sender
                            .send(Message::NewOperationContinuation(
                                operation, indices, sender, manager,
                            ))
                            .await,
                    ),
                }
            }
            Message::NewOperationContinuation(operation, indices, mut sender, manager) => {
                Self::check_unfailable_result(
                    &sender
                        .send(Ok(crate::render_graph::operation::state::State::new(
                            manager,
                            Self::emplace(operation, &mut data.operations, indices, data.index),
                        )))
                        .await,
                );
            }
            Message::NewResource(metadata, resource_data, sender, manager) => {
                Self::new_resource(
                    data,
                    resource_data,
                    texture_manager_subscription_message_sender,
                    metadata,
                    sender,
                    manager,
                )
                .await;
            }
            Message::NewResourceContinuation(
                resource,
                resource_data,
                indices,
                mut sender,
                manager,
            ) => {
                Self::insert_resource_data(data, resource_data, indices);

                let resource_state = crate::render_graph::resource::state::State::new(
                    manager,
                    Self::emplace(resource, &mut data.resources, indices, data.index),
                );

                Self::check_unfailable_result(&sender.send(Ok(resource_state)).await);
            }
            Message::RenderPassMetadata(operation_state, sender) => {
                Self::render_pass_metadata(data, operation_state, sender).await;
            }
            Message::Write(resource_state, operation_state, sender) => {
                Self::write(data, operation_state, resource_state, sender).await;
            }
            Message::WritesTo(operation, resource, sender) => {
                Self::writes_to(data, operation, resource, sender).await;
            }
            Message::InsertOperationsAndResources(
                operations_and_states,
                resources_and_states,
                sender,
            ) => {
                Self::insert_operations_and_resources(
                    data,
                    operations_and_states,
                    resources_and_states,
                    sender,
                )
                .await;
            }
            Message::DropOperation(indices) => {
                Self::drop_operation(data, indices).await;
            }
            Message::DropOperationContinuation(indices) => {
                Self::remove(&mut data.operations, indices, data.index);
            }
            Message::DropResource(indices) => {
                Self::drop_resource(data, indices).await;
            }
            Message::DropResourceContinuation(indices) => {
                Self::remove(&mut data.resources, indices, data.index);

                Self::remove(&mut data.resource_data_states, indices, data.index);
            }
            Message::DrainedMessages => {
                unreachable!("must be processed before this function");
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn process_wrapped_messages<'a, S>(
        streams: &mut S,
        data: &mut crate::render_graph::manager::Data,
        texture_manager_subscription_message_sender:
            &mut crate::texture::manager::prepare_task::subscription::item::Sender<'a>,
    ) where
        S: Send + Stream<Item = MessageWrapper> + Unpin,
    {
        while let Some(wrapped_message) = streams.next().await {
            match wrapped_message {
                    MessageWrapper::RenderGraphManagerMessage(message) => {
                        match message {
                            Message::DrainedMessages => {
                                Self::check_unfailable_result(
                                    &texture_manager_subscription_message_sender
                                    .close_service_message_stream()
                                    .await,
                                );

                                break;
                            },
                            other_message_types => {
                                Self::process_message(
                                    other_message_types,
                                    data,
                                    texture_manager_subscription_message_sender,
                                )
                                .await;
                            },
                        }
                    },
                    MessageWrapper::TextureManagerMessage(message) => match message {
                        crate::texture::manager::prepare_task::subscription::service::Message::ModifiedTexture => todo!(),
                    },
                }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn render_pass_metadata(
        data: &mut crate::render_graph::manager::Data,
        state: crate::render_graph::operation::state::State,
        mut result_sender: Sender<Result<fenrir_hal::render_pass::Metadata, Error>>,
    ) {
        let operations = &mut data.operations;

        let maybe_maybe_operation = operations.get_mut(state.indices()[data.index]);

        let result = if let Some(Some(operation)) = maybe_maybe_operation {
            let metadata = operation.metadata().await;

            Ok(metadata)
        } else {
            Err(Self::not_a_valid_state_of(state, type_name::<Operation>()))
        };

        Self::check_unfailable_result(&result_sender.send(result).await);
    }

    #[inline]
    fn remove<T>(vector: &mut [Option<T>], indices: [usize; 2], data_index: usize)
    where
        T: Send,
    {
        vector.get_mut(indices[data_index]).map_or_else(
            || unreachable!(),
            |maybe_element| match maybe_element {
                None => unreachable!(),
                Some(_) => {
                    *maybe_element = None;
                }
            },
        );
    }

    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn write(
        data: &mut crate::render_graph::manager::Data,
        operation_state: crate::render_graph::operation::state::State,
        resource_state: crate::render_graph::resource::state::State,
        mut result_sender: Sender<Result<(), Error>>,
    ) {
        let operations = &mut data.operations;

        let maybe_maybe_operation = operations.get_mut(operation_state.indices()[data.index]);

        if let Some(Some(operation)) = maybe_maybe_operation {
            match Operation::new(operation.metadata().await).await {
                Err(error) => {
                    Self::check_unfailable_result(&result_sender.send(Err(error)).await);
                }
                Ok(new_operation) => {
                    new_operation
                        .read_multiple(&operation.read_resources().await)
                        .await;

                    *operation = new_operation.clone();

                    let resources = &mut data.resources;

                    let maybe_maybe_resource =
                        resources.get_mut(resource_state.indices()[data.index]);

                    if let Some(Some(resource)) = maybe_maybe_resource {
                        let new_resource =
                            Resource::new(resource.data(), resource.metadata()).await;

                        *resource = new_resource.clone();

                        if let Err(error) = new_operation.write(new_resource.clone()).await {
                            Self::check_unfailable_result(&result_sender.send(Err(error)).await);
                        } else {
                            let operations_and_states = smallvec![(new_operation, operation_state)];

                            let resources_and_states = smallvec![(new_resource, resource_state)];

                            Self::check_unfailable_result(
                                &data
                                    .sender
                                    .send(Message::InsertOperationsAndResources(
                                        operations_and_states,
                                        resources_and_states,
                                        result_sender,
                                    ))
                                    .await,
                            );
                        }
                    } else {
                        Self::check_unfailable_result(
                            &result_sender
                                .send(Err(invalid_argument(&format!(
                                    "{resource_state:?} not representing a valid resource"
                                ))))
                                .await,
                        );
                    }
                }
            }
        } else {
            Self::check_unfailable_result(
                &result_sender
                    .send(Err(invalid_argument(&format!(
                        "{operation_state:?} not representing a valid operation"
                    ))))
                    .await,
            );
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn writes_to(
        data: &mut crate::render_graph::manager::Data,
        operation_state: crate::render_graph::operation::state::State,
        resource_state: crate::render_graph::resource::state::State,
        mut result_sender: Sender<Result<bool, Error>>,
    ) {
        let resources = &mut data.resources;

        let maybe_maybe_resource = resources.get(resource_state.indices()[data.index]);

        if let Some(Some(resource)) = maybe_maybe_resource {
            match resource.writer().await {
                None => {
                    Self::check_unfailable_result(&result_sender.send(Ok(false)).await);
                }
                Some(writer) => {
                    let operations = &mut data.operations;

                    let maybe_maybe_operation =
                        operations.get(operation_state.indices()[data.index]);

                    if let Some(Some(operation)) = maybe_maybe_operation {
                        Self::check_unfailable_result(
                            &result_sender.send(Ok(writer == *operation)).await,
                        );
                    } else {
                        Self::check_unfailable_result(
                            &result_sender
                                .send(Err(invalid_argument(&format!(
                                    "{operation_state:?} not representing a valid operation"
                                ))))
                                .await,
                        );
                    }
                }
            }
        } else {
            Self::check_unfailable_result(
                &result_sender
                    .send(Err(invalid_argument(&format!(
                        "{resource_state:?} not representing a valid resource"
                    ))))
                    .await,
            );
        }
    }
}

impl FutureGeneratorTrait for PrepareTask {
    fn generate_future(&mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>> {
        let (callback_data_sender, mut callback_data_receiver) =
            futures::channel::oneshot::channel();

        swap(
            &mut self.callback_data_receiver,
            &mut callback_data_receiver,
        );

        async_ffi::FutureExt::into_ffi(async move {
            Self::callback(callback_data_receiver, callback_data_sender)
                .await
                .into()
        })
    }
}
