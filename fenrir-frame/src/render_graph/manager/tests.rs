use {
    crate::{
        pipeline::{
            tests::{one_pipeline_iteration, pipeline, two_pipeline_iterations},
            Pipeline,
        },
        render_graph::{
            self,
            manager::{Manager, ResourceMetadata},
            operation,
            resource::{self},
        },
        texture,
    },
    fenrir_hal::{
        color::RGBA,
        device::Device,
        geometry::{Extent2D, Point2D, Rect2D},
        pipeline::BindPoint,
        render_pass,
    },
    fenrir_render_graph::{resource::ClearValue, test::instances},
    rand::{rng, rngs::StdRng, Rng, SeedableRng},
    sleipnir::runtime::Handle,
    tokio::test,
    tracing_test::traced_test,
};

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn new() {
    let pipeline = pipeline().await;

    let texture_manager = crate::texture::manager::Manager::new(pipeline.clone()).await;

    let render_graph_manager = Manager::new(pipeline, texture_manager).await;

    assert!(render_graph_manager.is_ok());
}

pub(crate) async fn render_graph_manager(
    pipeline: Pipeline,
    texture_manager: crate::texture::manager::Manager,
) -> Manager {
    let render_graph_manager = Manager::new(pipeline, texture_manager).await;

    assert!(render_graph_manager.is_ok());

    render_graph_manager.unwrap()
}

#[traced_test]
#[test(flavor = "multi_thread", worker_threads = 1)]
async fn new_operation() {
    let pipeline = pipeline().await;

    let texture_manager = crate::texture::manager::Manager::new(pipeline.clone()).await;

    let render_graph_manager =
        render_graph_manager(pipeline.clone(), texture_manager.clone()).await;

    let mut rng = rng();

    let runtime = Handle::current();

    for instance in instances("render_graph::manager::tests::new_operation").await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let render_area = Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0));

            let bind_point = match rng.random_range(0..3) {
                0 => BindPoint::Graphics,
                1 => BindPoint::Compute,
                _ => BindPoint::RayTracing,
            };

            let render_graph_manager_clone = render_graph_manager.clone();

            let metadata =
                render_pass::Metadata::new(device, Vec::new(), render_area, 0, 0, bind_point);

            let metadata_clone = metadata.clone();

            let new_operation_task = runtime.spawn(async move {
                render_graph_manager_clone
                    .new_operation(metadata_clone)
                    .await
            });

            two_pipeline_iterations(pipeline.clone()).await;

            let task_result = new_operation_task.await;

            assert!(task_result.is_ok());

            let operation_result = task_result.unwrap();

            assert!(operation_result.is_ok());

            let operation = operation_result.unwrap();

            let operation_clone = operation.clone();

            let render_graph_manager_clone = render_graph_manager.clone();

            let render_pass_metadata_task = runtime.spawn(async move {
                render_graph_manager_clone
                    .render_pass_metadata(operation_clone)
                    .await
            });

            one_pipeline_iteration(pipeline.clone()).await;

            let render_pass_metadata_task_result = render_pass_metadata_task.await;

            assert!(render_pass_metadata_task_result.is_ok());

            let render_pass_metadata_result = render_pass_metadata_task_result.unwrap();

            assert!(render_pass_metadata_result.is_ok());

            assert_eq!(metadata, render_pass_metadata_result.unwrap());
        }
    }
}

async fn generate_texture(
    rng: &mut StdRng,
    device: Device,
    name: &str,
    texture_manager: texture::manager::Manager,
    pipeline: Pipeline,
) -> texture::state::State {
    let runtime = Handle::current();

    let width = 2usize.pow(rng.random_range(0..4));
    let height = 2usize.pow(rng.random_range(0..4));
    let depth = rng.random_range(1..4);

    let metadata = fenrir_hal::texture::Metadata::new(
        device,
        width,
        height,
        depth,
        Vec::new(),
        name.to_string(),
    );

    let texture_state_task =
        runtime.spawn(async move { texture_manager.new_texture(metadata).await });

    two_pipeline_iterations(pipeline.clone()).await;

    let texture_state_task_result = texture_state_task.await;

    assert!(texture_state_task_result.is_ok());

    let texture_state_result = texture_state_task_result.unwrap();

    assert!(texture_state_result.is_ok());

    texture_state_result.unwrap()
}

#[traced_test]
#[test(flavor = "multi_thread", worker_threads = 1)]
async fn new_resource() {
    let name = "render_graph::manager::tests::new_resource";

    let pipeline = pipeline().await;

    let texture_manager = crate::texture::manager::Manager::new(pipeline.clone()).await;

    let render_graph_manager =
        render_graph_manager(pipeline.clone(), texture_manager.clone()).await;

    let mut seeder = rng();

    let mut rng = StdRng::from_rng(&mut seeder);

    let runtime = Handle::current();

    for instance in instances(name).await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let texture_state = generate_texture(
                &mut rng,
                device.clone(),
                name,
                texture_manager.clone(),
                pipeline.clone(),
            )
            .await;

            let render_graph_manager_clone = render_graph_manager.clone();

            let begin_rendering_operation = Some(match rng.random_range(0..2) {
                0 => fenrir_render_graph::resource::Operation::Load,
                1 => fenrir_render_graph::resource::Operation::Clear,
                _ => fenrir_render_graph::resource::Operation::DontCare,
            });

            let end_rendering_operation = Some(match rng.random_range(0..2) {
                0 => fenrir_render_graph::resource::Operation::Store,
                1 => fenrir_render_graph::resource::Operation::Clear,
                _ => fenrir_render_graph::resource::Operation::DontCare,
            });

            let clear_value = Some(ClearValue::FColor(RGBA::new(
                rng.random_range(0.0..1.0),
                rng.random_range(0.0..1.0),
                rng.random_range(0.0..1.0),
                rng.random_range(0.0..1.0),
            )));

            let metadata = ResourceMetadata::new(
                begin_rendering_operation,
                end_rendering_operation,
                clear_value,
            );

            let resource_state_task = runtime.spawn(async move {
                render_graph_manager_clone
                    .new_resource(
                        metadata,
                        crate::render_graph::resource::data::State::Texture(texture_state),
                    )
                    .await
            });

            two_pipeline_iterations(pipeline.clone()).await;

            let resource_state_task_result = resource_state_task.await;

            assert!(resource_state_task_result.is_ok());

            let resourec_state_result = resource_state_task_result.unwrap();

            assert!(resourec_state_result.is_ok());
        }
    }
}

async fn generate_operation(
    rng: &mut StdRng,
    device: Device,
    render_graph_manager: render_graph::manager::Manager,
    pipeline: Pipeline,
) -> operation::state::State {
    let render_area = Rect2D::new(Point2D::new(0, 0), Extent2D::new(0, 0));

    let bind_point = match rng.random_range(0..3) {
        0 => BindPoint::Graphics,
        1 => BindPoint::Compute,
        _ => BindPoint::RayTracing,
    };

    let metadata = render_pass::Metadata::new(device, Vec::new(), render_area, 0, 0, bind_point);

    let metadata_clone = metadata.clone();

    let runtime = Handle::current();

    let new_operation_task =
        runtime.spawn(async move { render_graph_manager.new_operation(metadata_clone).await });

    two_pipeline_iterations(pipeline).await;

    let task_result = new_operation_task.await;

    assert!(task_result.is_ok());

    let operation_result = task_result.unwrap();

    assert!(operation_result.is_ok());

    operation_result.unwrap()
}

async fn generate_resource(
    rng: &mut StdRng,
    render_graph_manager: render_graph::manager::Manager,
    device: Device,
    name: &str,
    texture_manager: texture::manager::Manager,
    pipeline: Pipeline,
) -> resource::state::State {
    let begin_rendering_operation = Some(match rng.random_range(0..2) {
        0 => fenrir_render_graph::resource::Operation::Load,
        1 => fenrir_render_graph::resource::Operation::Clear,
        _ => fenrir_render_graph::resource::Operation::DontCare,
    });

    let end_rendering_operation = Some(match rng.random_range(0..2) {
        0 => fenrir_render_graph::resource::Operation::Store,
        1 => fenrir_render_graph::resource::Operation::Clear,
        _ => fenrir_render_graph::resource::Operation::DontCare,
    });

    let clear_value = Some(ClearValue::FColor(RGBA::new(
        rng.random_range(0.0..1.0),
        rng.random_range(0.0..1.0),
        rng.random_range(0.0..1.0),
        rng.random_range(0.0..1.0),
    )));

    let metadata = ResourceMetadata::new(
        begin_rendering_operation,
        end_rendering_operation,
        clear_value,
    );

    let texture_state = generate_texture(
        rng,
        device.clone(),
        name,
        texture_manager.clone(),
        pipeline.clone(),
    )
    .await;

    let runtime = Handle::current();

    let resource_state_task = runtime.spawn(async move {
        render_graph_manager
            .new_resource(
                metadata,
                crate::render_graph::resource::data::State::Texture(texture_state),
            )
            .await
    });

    two_pipeline_iterations(pipeline.clone()).await;

    let resource_state_task_result = resource_state_task.await;

    assert!(resource_state_task_result.is_ok());

    let resource_state_result = resource_state_task_result.unwrap();

    assert!(resource_state_result.is_ok());

    resource_state_result.unwrap()
}

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn write_resource() {
    let name = "render_graph::manager::tests::write_resource";

    let pipeline = pipeline().await;

    let texture_manager = crate::texture::manager::Manager::new(pipeline.clone()).await;

    let render_graph_manager =
        render_graph_manager(pipeline.clone(), texture_manager.clone()).await;

    let mut seeder = rng();

    let mut rng = StdRng::from_rng(&mut seeder);

    let runtime = Handle::current();

    for instance in instances(name).await {
        let devices = instance.devices();

        assert!(devices.is_ok());

        for device in devices.unwrap() {
            let resource_state = generate_resource(
                &mut rng,
                render_graph_manager.clone(),
                device.clone(),
                name,
                texture_manager.clone(),
                pipeline.clone(),
            )
            .await;

            let operation_state = generate_operation(
                &mut rng,
                device,
                render_graph_manager.clone(),
                pipeline.clone(),
            )
            .await;

            {
                let render_graph_manager_clone = render_graph_manager.clone();
                let resource_state_clone = resource_state.clone();
                let operation_state_clone = operation_state.clone();

                let write_task = runtime.spawn(async move {
                    render_graph_manager_clone
                        .write(resource_state_clone, operation_state_clone)
                        .await
                });

                two_pipeline_iterations(pipeline.clone()).await;

                let write_task_result = write_task.await;

                assert!(write_task_result.is_ok());

                let write_result = write_task_result.unwrap();

                assert!(write_result.is_ok());
            }

            let render_graph_manager_clone = render_graph_manager.clone();

            let writes_to_task = runtime.spawn(async move {
                render_graph_manager_clone
                    .writes_to(operation_state, resource_state)
                    .await
            });

            one_pipeline_iteration(pipeline.clone()).await;

            let writes_to_task_result = writes_to_task.await;

            assert!(writes_to_task_result.is_ok());

            let writes_to_result = writes_to_task_result.unwrap();

            assert!(writes_to_result.is_ok());

            let writes_to = writes_to_result.unwrap();

            assert!(writes_to);
        }
    }
}
