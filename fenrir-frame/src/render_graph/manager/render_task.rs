use {
    crate::{
        pipeline::filter::callback::stable_abi::FutureGeneratorTrait, render_graph::manager::Data,
    },
    abi_stable::std_types::RResult,
    async_ffi::BorrowingFfiFuture,
    fenrir_error::{connection_shutdown, Error},
    futures::{
        channel::mpsc::{Receiver, Sender},
        SinkExt, StreamExt,
    },
    std::mem::swap,
    tracing::error,
};

#[derive(Debug)]
struct CallbackData {
    receiver: futures::channel::mpsc::Receiver<Data>,
    texture_manager_subscription: crate::texture::manager::render_task::Subscription,
    sender: futures::channel::mpsc::Sender<Data>,
}

#[derive(Debug)]
pub(super) struct RenderTask {
    callback_data_receiver: futures::channel::oneshot::Receiver<CallbackData>,
}

impl RenderTask {
    #[inline]
    pub(super) async fn new(
        receiver: Receiver<Data>,
        sender: Sender<Data>,
        texture_manager: &crate::texture::manager::Manager,
    ) -> Self {
        let (callback_data_sender, callback_data_receiver) = futures::channel::oneshot::channel();

        let texture_manager_subscription = texture_manager.render_task().subscribe().await;

        callback_data_sender
            .send(CallbackData {
                receiver,
                texture_manager_subscription,
                sender,
            })
            .expect("we constructed the receiving half in this scope");

        Self {
            callback_data_receiver,
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn callback(
        callback_data_receiver: futures::channel::oneshot::Receiver<CallbackData>,
        callback_data_sender: futures::channel::oneshot::Sender<CallbackData>,
    ) -> Result<(), Error> {
        let mut callback_data =
            (callback_data_receiver.await).map_or_else(|_| Err(connection_shutdown()), Ok)?;

        let _texture_manager_subscription_iteration =
            (callback_data.texture_manager_subscription.next().await)
                .map_or_else(|| Err(connection_shutdown()), Ok)??;

        {
            let data = Self::get_data(&mut callback_data.receiver).await?;

            let result = data.graph.render().await;

            if let Err(error) = &result {
                error!("failed to execute render graph: {error}");
            }

            result?;

            let send_result = callback_data.sender.send(data).await;

            match send_result {
                Ok(()) => Ok(()),
                Err(_) => Err(connection_shutdown()),
            }?;
        }

        match callback_data_sender.send(callback_data) {
            Ok(()) => Ok(()),
            Err(_) => Err(connection_shutdown()),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_data(receiver: &mut Receiver<Data>) -> Result<Data, Error> {
        (receiver.next().await).map_or_else(|| Err(connection_shutdown()), Ok)
    }
}

impl FutureGeneratorTrait for RenderTask {
    #[inline]
    fn generate_future(&mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>> {
        let (callback_data_sender, mut callback_data_receiver) =
            futures::channel::oneshot::channel();

        swap(
            &mut self.callback_data_receiver,
            &mut callback_data_receiver,
        );

        async_ffi::FutureExt::into_ffi(async move {
            Self::callback(callback_data_receiver, callback_data_sender)
                .await
                .into()
        })
    }
}
