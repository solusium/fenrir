use {
    crate::render_graph::{manager::Manager, resource, state::StateTrait},
    abi_stable::{std_types::RArc, StableAbi},
    fenrir_error::Error,
    fenrir_hal::render_pass::Metadata,
};

#[repr(C)]
#[derive(Debug, StableAbi)]
struct Implementation {
    manager: Manager,
    indices: [usize; 2],
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct State {
    implementation: RArc<Implementation>,
}

impl State {
    #[inline]
    pub(crate) fn new(manager: Manager, indices: [usize; 2]) -> Self {
        let implementation = RArc::new(Implementation { manager, indices });

        Self { implementation }
    }

    #[inline]
    pub fn manager(&self) -> Manager {
        self.implementation.manager.clone()
    }

    #[inline]
    pub async fn metadata(&self) -> Result<Metadata, Error> {
        self.implementation
            .manager
            .render_pass_metadata(self.clone())
            .await
    }

    #[inline]
    pub async fn write(&self, resource: resource::state::State) -> Result<(), Error> {
        self.implementation
            .manager
            .write(resource, self.clone())
            .await
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        if self.manager.drop_operation(self.indices).is_err() {
            unreachable!();
        }
    }
}

impl StateTrait for State {
    #[inline]
    #[must_use]
    fn indices(&self) -> [usize; 2] {
        self.implementation.indices
    }
}
