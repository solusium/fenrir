use {crate::render_graph::resource::data::color::attachment::Metadata, abi_stable::StableAbi};

pub mod color;

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub enum State {
    ColorAttachment(crate::texture::state::State, Metadata),
    Texture(crate::texture::state::State),
}
