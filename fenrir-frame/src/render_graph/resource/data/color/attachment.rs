use {
    abi_stable::StableAbi,
    fenrir_hal::{
        color::{
            attachment::{LoadOperation, StoreOperation},
            component::Mapping,
        },
        texture::SubresourceRange,
    },
};

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Metadata {
    mapping: Mapping,
    subresource_range: SubresourceRange,
    load_operation: LoadOperation,
    store_operation: StoreOperation,
    clear_value: fenrir_hal::clear::Value,
}

impl Metadata {
    #[inline]
    #[must_use]
    pub const fn new(
        mapping: Mapping,
        subresource_range: SubresourceRange,
        load_operation: LoadOperation,
        store_operation: StoreOperation,
        clear_value: fenrir_hal::clear::Value,
    ) -> Self {
        Self {
            mapping,
            subresource_range,
            load_operation,
            store_operation,
            clear_value,
        }
    }

    #[inline]
    pub(crate) const fn mapping(&self) -> Mapping {
        self.mapping
    }

    #[inline]
    pub(crate) const fn subresource_range(&self) -> SubresourceRange {
        self.subresource_range
    }

    #[inline]
    pub(crate) const fn load_operation(&self) -> LoadOperation {
        self.load_operation
    }

    #[inline]
    pub(crate) const fn store_operation(&self) -> StoreOperation {
        self.store_operation
    }

    #[inline]
    pub(crate) const fn clear_value(&self) -> fenrir_hal::clear::Value {
        self.clear_value
    }
}
