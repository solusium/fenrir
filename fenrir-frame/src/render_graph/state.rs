pub(crate) trait StateTrait {
    fn indices(&self) -> [usize; 2];
}
