pub mod state;

pub type State = crate::render_graph::operation::state::State;
