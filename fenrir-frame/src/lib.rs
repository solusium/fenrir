#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::type_complexity,
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate
)]

use {
    crate::{pipeline::Stage, state::State},
    abi_stable::{
        std_types::ROption::{self, RNone, RSome},
        StableAbi,
    },
    async_ffi::FfiFuture,
    core::{
        fmt::{Debug, Formatter},
        mem::take,
        pin::Pin,
        task::{Context, Poll},
    },
    fenrir_error::Error,
    futures::Future,
    sleipnir::{ffi::runtime::Handle, pin},
};

pub mod config;
pub mod graphics;
pub mod pipeline;
pub mod render_graph;
pub mod state;
pub mod subscription;
pub mod texture;

#[repr(C)]
#[derive(StableAbi)]
pub struct Frame {
    state: State,
    runtime: Handle,
    drop: ROption<FfiFuture<[u8; 0]>>,
}

impl Frame {
    #[inline]
    pub(crate) const fn new(state: State, runtime: Handle, drop: FfiFuture<[u8; 0]>) -> Self {
        Self {
            state,
            runtime,
            drop: RSome(drop),
        }
    }

    /// Returns the next stage of this [`Frame`].
    ///
    /// # Errors
    ///
    /// This function will return [`ConnectionShutdown`](fenrir_error::Error::ConnectionShutdown)
    /// if the [`Pipeline`](crate::pipeline::Pipeline) this [`Frame`] originate from dropped.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn current_stage(&mut self) -> Result<Stage, Error> {
        self.state.current_stage().await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn next_stage(&mut self) -> Result<Stage, Error> {
        self.state.next_stage().await
    }
}

impl Debug for Frame {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Frame")
            .field("state", &self.state)
            .field("runtime", &self.runtime)
            .finish_non_exhaustive()
    }
}

impl Drop for Frame {
    #[inline]
    fn drop(&mut self) {
        match take(&mut self.drop) {
            RNone => unreachable!(),
            RSome(drop) => {
                let _join_handle = self.runtime.spawn(async move {
                    let _ = drop.await;
                });
            }
        }
    }
}

impl Future for Frame {
    type Output = <State as Future>::Output;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut_self = self.get_mut();

        let state = &mut mut_self.state;

        pin!(state);

        state.poll(cx)
    }
}

impl Future for State {
    type Output = Result<Stage, Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let last_stage = self.last_stage();

        if (Stage::Present == last_stage) || (Stage::Error == last_stage) {
            Poll::Ready(Ok(last_stage))
        } else {
            let mut_self = self.get_mut();

            let mut get_current_stage_poll = || {
                let current_stage_future = mut_self.current_stage();

                pin!(current_stage_future);

                current_stage_future.poll(cx)
            };

            match get_current_stage_poll() {
                Poll::Pending => Poll::Pending,
                Poll::Ready(current_stage_result) => match current_stage_result {
                    Err(error) => Poll::Ready(Err(error)),
                    Ok(current_stage) => {
                        if (Stage::Present == current_stage) || (Stage::Error == current_stage) {
                            Poll::Ready(Ok(current_stage))
                        } else {
                            let mut poll_result = Poll::Pending;

                            loop {
                                let next_stage_future = mut_self.next_stage();

                                pin!(next_stage_future);

                                let next_stage_poll = next_stage_future.poll(cx);

                                match next_stage_poll {
                                    Poll::Pending => break,
                                    Poll::Ready(result) => match result {
                                        Err(error) => {
                                            poll_result = Poll::Ready(Err(error));
                                            break;
                                        }
                                        Ok(stage) => {
                                            if (Stage::Present == stage) || (Stage::Error == stage)
                                            {
                                                poll_result = Poll::Ready(Ok(stage));
                                                break;
                                            }
                                        }
                                    },
                                }
                            }

                            poll_result
                        }
                    }
                },
            }
        }
    }
}

impl Unpin for Frame {}
