#[allow(unused_imports)]
use {crate::pipeline::Pipeline, fenrir_error::Error::ConnectionShutdown};
use {
    crate::{
        state::stable_abi::{Trait, Trait_TO},
        Stage,
    },
    abi_stable::{
        sabi_trait,
        std_types::{
            RBox,
            RResult::{self, RErr, ROk},
        },
        StableAbi,
    },
    async_ffi::BorrowingFfiFuture,
    fenrir_error::{connection_shutdown, Error},
    futures::{channel::mpsc::UnboundedReceiver, lock::Mutex, FutureExt, StreamExt},
    std::{fmt::Debug, sync::Arc},
};

mod stable_abi {
    #![allow(
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        clippy::cast_ptr_alignment,
        clippy::needless_lifetimes,
        non_local_definitions
    )]

    use {
        crate::pipeline::Stage,
        abi_stable::{sabi_trait, std_types::RResult},
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    pub(crate) trait Trait: Clone + Debug + Unpin + Send + Sync {
        fn current_stage<'a>(&'a mut self) -> BorrowingFfiFuture<'a, RResult<Stage, Error>>;

        fn entity(&self) -> usize;

        fn next_stage<'a>(&'a mut self) -> BorrowingFfiFuture<'a, RResult<Stage, Error>>;

        fn last_stage(&self) -> Stage;
    }
}

#[derive(Clone, Debug)]
pub(crate) struct Implementation {
    entity: usize,
    stage_receiver: Arc<Mutex<UnboundedReceiver<Stage>>>,
    stage: Stage,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct State {
    implementation: Trait_TO<'static, RBox<()>>,
}

impl Implementation {
    #[inline]
    pub(crate) fn new(entity: usize, stage_receiver: UnboundedReceiver<Stage>) -> Self {
        Self {
            entity,
            stage_receiver: Arc::new(Mutex::new(stage_receiver)),
            stage: Stage::Unknown,
        }
    }
}

impl State {
    #[inline]
    pub(crate) fn new(entity: usize, stage_receiver: UnboundedReceiver<Stage>) -> Self {
        Self {
            implementation: Trait_TO::from_value(
                Implementation::new(entity, stage_receiver),
                sabi_trait::TD_Opaque,
            ),
        }
    }

    #[inline]
    pub(crate) async fn current_stage(&mut self) -> Result<Stage, Error> {
        match self.implementation.current_stage().await {
            ROk(stage) => Ok(stage),
            RErr(code) => Err(code),
        }
    }

    #[inline]
    #[must_use]
    pub fn entity(&self) -> usize {
        self.implementation.entity()
    }

    /// Returns the next stage of this [`State`].
    ///
    /// # Errors
    ///
    /// This function will return [`ConnectionShutdown`] if the [`Pipeline`] this [`State`]
    /// originate from dropped.
    #[inline]
    pub async fn next_stage(&mut self) -> Result<Stage, Error> {
        match self.implementation.next_stage().await {
            ROk(stage) => Ok(stage),
            RErr(code) => Err(code),
        }
    }

    #[inline]
    #[must_use]
    pub fn last_stage(&self) -> Stage {
        self.implementation.last_stage()
    }
}

impl Trait for Implementation {
    #[inline]
    fn current_stage(&mut self) -> BorrowingFfiFuture<'_, RResult<Stage, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            let mut writer = self.stage_receiver.lock().await;

            while let Some(stage_result) = writer.next().now_or_never() {
                match stage_result {
                    None => {
                        return RErr(connection_shutdown());
                    }
                    Some(stage) => {
                        self.stage = stage;
                    }
                }
            }

            ROk(self.stage)
        })
    }

    #[inline]
    fn entity(&self) -> usize {
        self.entity
    }

    #[inline]
    fn next_stage(&mut self) -> BorrowingFfiFuture<'_, RResult<Stage, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            let mut writer = self.stage_receiver.lock().await;

            let maybe_stage = writer.next().await;

            match maybe_stage {
                None => RErr(connection_shutdown()),
                Some(stage) => {
                    self.stage = stage;

                    ROk(stage)
                }
            }
        })
    }

    #[inline]
    fn last_stage(&self) -> Stage {
        self.stage
    }
}

unsafe impl Send for State {}
unsafe impl Sync for State {}
