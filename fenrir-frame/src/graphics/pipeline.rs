pub(crate) mod state;

pub type State = crate::graphics::pipeline::state::State;
