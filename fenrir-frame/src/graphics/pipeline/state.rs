use {crate::render_graph::Manager, abi_stable::StableAbi};

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct State {
    manager: Manager,
    indices: [usize; 2],
}

impl State {
    #[inline]
    pub(crate) const fn new(manager: Manager, indices: [usize; 2]) -> Self {
        Self { manager, indices }
    }
}
