use {
    crate::{
        config::RECOMMEND_MULTI_BUFFERING,
        pipeline::{
            filter::{
                callback::{stable_abi::FutureGeneratorTrait, Adder, FutureGenerator},
                connection::Connection,
                Filter,
            },
            frame::Frame,
            stable_abi::{Trait, Trait_TO},
        },
    },
    abi_stable::{
        sabi_trait,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RStr,
        },
        StableAbi,
    },
    async_ffi::BorrowingFfiFuture,
    async_lock::{Mutex, RwLock},
    fenrir_error::{connection_shutdown, invalid_argument, state_not_recoverable, Error},
    futures::{
        channel::mpsc::{channel, unbounded, Receiver, Sender, UnboundedSender},
        future::{join_all, lazy, select, Either},
        stream, FutureExt, SinkExt, StreamExt, TryStreamExt,
    },
    sleipnir::{pin, runtime::Handle, task::JoinHandle},
    smallvec::SmallVec,
    std::{
        fmt::Debug,
        mem::take,
        ops::{DerefMut, IndexMut},
        sync::{mpsc::sync_channel, Arc},
    },
};

pub mod filter;
pub mod frame;

#[derive(Debug)]
pub(crate) enum Action {
    Continue(futures::channel::oneshot::Receiver<()>),
    Shutdown,
}

#[derive(Debug)]
pub(crate) enum Message {
    Connect(Adder),
    Disconnect(usize, Option<Sender<()>>),
    Frame(Frame),
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Stage {
    Enqueue,
    User,
    Prepare,
    Render,
    Present,
    Error,
    Unknown,
}

mod stable_abi {
    #![allow(
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        clippy::cast_ptr_alignment,
        clippy::needless_lifetimes,
        non_local_definitions
    )]

    use {
        crate::pipeline::{
            filter::{callback::FutureGenerator, connection::Connection},
            Stage,
        },
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RStr},
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    pub trait Trait: Clone + Debug + Send + Sync {
        fn connect<'a, FG>(
            &'a self,
            stage: Stage,
            future_generator: FutureGenerator,
            name: RStr<'a>,
        ) -> BorrowingFfiFuture<'a, RResult<Connection, Error>>;

        fn push<'a>(&'a self) -> BorrowingFfiFuture<'a, RResult<crate::Frame, Error>>;
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub(crate) struct Implementation {
    filter: Option<Mutex<[Filter; 4]>>,
    task: Arc<Mutex<JoinHandle<Result<(), Error>>>>,
    unused_frames_sender: Sender<Frame>,
    unused_frames_receiver: Arc<Mutex<Receiver<Frame>>>,
    frame_recycle_sender: Sender<Message>,
    frame_state_sender: Sender<crate::state::State>,
    frame_state_receiver: Arc<RwLock<Receiver<crate::state::State>>>,
    runtime: Handle,
    shutdown_sender: Arc<Mutex<Option<futures::channel::oneshot::Sender<()>>>>,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Pipeline {
    state: Trait_TO<'static, RArc<()>>,
}

impl Implementation {
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn new() -> Result<Self, Error> {
        let (mut unused_frames_sender, receiver) = channel(RECOMMEND_MULTI_BUFFERING);

        let unused_frames_sender_ref = &mut unused_frames_sender;

        let setup_states = || async move {
            let frame_state_channels =
                (0..RECOMMEND_MULTI_BUFFERING).map(|entity| (entity, unbounded()));

            let mut frame_datas: SmallVec<
                [(usize, UnboundedSender<Stage>); RECOMMEND_MULTI_BUFFERING],
            > = SmallVec::new();

            let mut states: SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]> =
                SmallVec::new();

            for (entity, (sender, receiver)) in frame_state_channels {
                frame_datas.push((entity, sender));
                states.push(crate::state::State::new(entity, receiver));
            }

            stream::iter(frame_datas.into_iter())
                .map(
                    |frame_data| -> Result<(Sender<Frame>, usize, UnboundedSender<Stage>), Error> {
                        let (entity, stage_sender) = frame_data;

                        Ok((unused_frames_sender_ref.clone(), entity, stage_sender))
                    },
                )
                .try_for_each(
                    |(mut unused_frames_sender_, entity, stage_sender)| async move {
                        (unused_frames_sender_
                            .send(Frame::new(entity, stage_sender))
                            .await)
                            .map_or_else(|_| Err(connection_shutdown()), Ok)
                    },
                )
                .await?;

            let (mut frame_sender, frame_receiver) = channel(RECOMMEND_MULTI_BUFFERING);

            let frame_sender_ref = &mut frame_sender;

            let result = stream::iter(states.into_iter())
                .map(|frame_state| -> Result<crate::state::State, Error> { Ok(frame_state) })
                .try_for_each(|frame| async {
                    (frame_sender_ref.clone().send(frame).await)
                        .map_or_else(|_| Err(connection_shutdown()), Ok)
                })
                .await;

            match result {
                Ok(()) => Ok((frame_sender, frame_receiver)),
                Err(error) => Err(error),
            }
        };

        let (frame_state_sender, frame_state_receiver_) = setup_states().await?;

        let frame_state_receiver = Arc::new(RwLock::new(frame_state_receiver_));

        let (frame_recycle_sender, frame_recycle_receiver) = channel(RECOMMEND_MULTI_BUFFERING);

        let (shutdown_sender_, shutdown_receiver) = futures::channel::oneshot::channel();

        let shutdown_sender = Arc::new(Mutex::new(Some(shutdown_sender_)));

        let runtime = Handle::current();

        let task = Arc::new(Mutex::new(runtime.spawn(Self::task(
            frame_recycle_receiver,
            unused_frames_sender.clone(),
            shutdown_receiver,
        ))));

        let unused_frames_receiver = Arc::new(Mutex::new(receiver));

        let mut output_sender = frame_recycle_sender.clone();

        let mut filter_ =
            [Stage::Present, Stage::Render, Stage::Prepare, Stage::User].map(|stage| {
                let mut filter = Filter::new(runtime.clone(), stage, output_sender.clone());

                output_sender = filter.input_sender().clone();

                filter
            });

        filter_.reverse();

        let filter = Some(Mutex::new(filter_));

        Ok(Self {
            filter,
            task,
            unused_frames_sender,
            unused_frames_receiver,
            frame_recycle_sender,
            frame_state_sender,
            frame_state_receiver,
            runtime,
            shutdown_sender,
        })
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn await_more_unused_frames_or_state_receivers(
        &self,
        unused_frames: &mut SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]>,
        frame_states: &mut SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]>,
    ) {
        let mut unused_frames_receiver = self.unused_frames_receiver.lock().await;

        let mut frame_state_receiver = self.frame_state_receiver.write().await;

        let more_data = select(unused_frames_receiver.next(), frame_state_receiver.next()).await;

        match more_data {
            Either::Left((unused_frame, frame_state_future)) => {
                drop(unused_frames_receiver);

                unused_frames.push(unused_frame.unwrap());

                if let Some(Some(frame_state)) = frame_state_future.now_or_never() {
                    drop(frame_state_receiver);

                    frame_states.push(frame_state);
                }
            }
            Either::Right((frame_state, unused_frame_future)) => {
                frame_states.push(frame_state.unwrap());

                drop(frame_state_receiver);

                if let Some(Some(unused_frame)) = unused_frame_future.now_or_never() {
                    drop(unused_frames_receiver);

                    unused_frames.push(unused_frame);
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop_impl(
        sender_arc: Arc<Mutex<Option<futures::channel::oneshot::Sender<()>>>>,
        task: Arc<Mutex<JoinHandle<Result<(), Error>>>>,
    ) {
        let mut sender_lock = sender_arc.lock().await;

        let maybe_sender = take(sender_lock.deref_mut());

        drop(sender_lock);

        match maybe_sender {
            None => {
                unreachable!("the sender for the shutdown signal shouldn't be taken already")
            }
            Some(sender) => {
                // We either send the shutdown signal with success to the task or the task (and
                // shutdown receiver) already shutdown because the filter and their frame channel
                // from or to the pipeline already shutdown.
                let _send_result = sender.send(());

                (task.lock().await.deref_mut().await).map_or_else(
                    |_| unreachable!("joining the task shouldn't fail"),
                    |result| {
                        if let Err(error) = result {
                            assert_eq!(Error::ConnectionShutdown, error);
                        }
                    },
                );
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_unused_frames_and_state_receivers(
        &self,
        unused_frames: &mut SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]>,
        frame_states: &mut SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]>,
    ) {
        {
            let mut unused_frames_receiver = self.unused_frames_receiver.lock().await;

            while let Some(Some(unused_frame)) = unused_frames_receiver.next().now_or_never() {
                unused_frames.push(unused_frame);
            }
        }

        let mut frame_state_receiver = self.frame_state_receiver.write().await;

        while let Some(Some(frame_state)) = frame_state_receiver.next().now_or_never() {
            frame_states.push(frame_state);
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn iteration(
        finished_frame_receiver: &mut Receiver<Message>,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
        unused_frame_sender: &mut Sender<Frame>,
    ) -> Result<Action, Error> {
        match select(finished_frame_receiver.next(), shutdown_receiver).await {
            Either::Left((maybe_message, shutdown_future_outer)) => match maybe_message {
                None => Err(connection_shutdown()),
                Some(message) => match message {
                    Message::Frame(frame) => {
                        let send_future = unused_frame_sender.send(frame);

                        pin!(send_future);

                        match select(send_future, shutdown_future_outer).await {
                            Either::Left((send_result, shutdown_future_inner)) => match send_result
                            {
                                Ok(()) => Ok(Action::Continue(shutdown_future_inner)),
                                Err(_) => Err(connection_shutdown()),
                            },
                            Either::Right(_) => Ok(Action::Shutdown),
                        }
                    }
                    _ => {
                        unreachable!();
                    }
                },
            },
            Either::Right((_, _)) => Ok(Action::Shutdown),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn push_frame_and_get_state(
        &self,
        unused_frames: &mut SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]>,
        frame_states: &mut SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]>,
    ) -> Result<crate::state::State, Error> {
        loop {
            self.get_unused_frames_and_state_receivers(unused_frames, frame_states)
                .await;

            let frame_state_receiver_option = self
                .try_push_unused_frame(unused_frames, frame_states)
                .await?;

            if let Some(frame_state_receiver) = frame_state_receiver_option {
                return Ok(frame_state_receiver);
            }

            self.await_more_unused_frames_or_state_receivers(unused_frames, frame_states)
                .await;
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn return_unused_frames_and_states(
        &self,
        unused_frames: SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]>,
        frame_states: SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]>,
    ) -> Result<(), Error> {
        let frame_state_sender = self.frame_state_sender.clone();

        let returned_frame_states = join_all(frame_states.into_iter().map(|frame_state| async {
            match frame_state_sender.clone().send(frame_state).await {
                Ok(()) => Ok(()),
                Err(_) => Err(connection_shutdown()),
            }
        }))
        .await;

        let unused_frames_sender = &self.unused_frames_sender;

        let returned_unused_frames_results =
            join_all(unused_frames.into_iter().map(|unused_frame| async {
                match unused_frames_sender.clone().send(unused_frame).await {
                    Ok(()) => Ok(()),
                    Err(_) => Err(connection_shutdown()),
                }
            }))
            .await;

        returned_frame_states
            .into_iter()
            .try_for_each(|x| -> Result<(), Error> { x })?;

        returned_unused_frames_results
            .into_iter()
            .try_for_each(|x| -> Result<(), Error> { x })
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn task(
        mut finished_frame_receiver: Receiver<Message>,
        mut unused_frame_sender: Sender<Frame>,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
    ) -> Result<(), Error> {
        let mut next_action = Self::iteration(
            &mut finished_frame_receiver,
            shutdown_receiver,
            &mut unused_frame_sender,
        )
        .await?;

        while let Action::Continue(shutdown_future) = next_action {
            next_action = Self::iteration(
                &mut finished_frame_receiver,
                shutdown_future,
                &mut unused_frame_sender,
            )
            .await?;
        }

        Ok(())
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn try_push_unused_frame(
        &self,
        unused_frames: &mut SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]>,
        frame_states: &mut SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]>,
    ) -> Result<Option<crate::state::State>, Error> {
        let position = unused_frames.iter().position(|unused_frame| {
            let entity = unused_frame.entity();
            frame_states
                .iter()
                .any(|frame_state| entity == frame_state.entity())
        });

        match position {
            None => Ok(None),
            Some(unused_frame_index) => {
                let frame = unused_frames.swap_remove(unused_frame_index);

                let frame_ref = &frame;

                let index_result = frame_states
                    .iter()
                    .position(|frame_state| frame_ref.entity() == frame_state.entity());

                match index_result {
                    None => Err(state_not_recoverable(&format!(
                        "no frame state found with entity {}",
                        frame.entity()
                    ))),
                    Some(index) => {
                        let frame_state = frame_states.swap_remove(index);

                        match self.filter.as_ref() {
                            None => unreachable!("filter should only be taken when dropping self"),
                            Some(filter) => {
                                let mut filter_guard = filter.lock().await;

                                let maybe_filter = filter_guard.first_mut();

                                match maybe_filter {
                                    None => {
                                        unreachable!(
                                            "filter, being an array of size 4, can't be empty"
                                        )
                                    }
                                    Some(filter) => {
                                        filter.push(frame).await?;

                                        drop(filter_guard);

                                        Ok(Some(frame_state))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

impl Pipeline {
    #[inline]
    #[must_use]
    pub fn new() -> BorrowingFfiFuture<'static, RResult<Self, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            match Implementation::new().await {
                Err(error) => RErr(error),
                Ok(implementation) => ROk(Self {
                    state: Trait_TO::from_ptr(RArc::new(implementation), sabi_trait::TD_Opaque),
                }),
            }
        })
    }

    #[inline]
    pub fn connect<'a, FG>(
        &'a self,
        stage: Stage,
        future_generator: FG,
        name: &'a str,
    ) -> BorrowingFfiFuture<'a, RResult<Connection, Error>>
    where
        FG: FutureGeneratorTrait + 'static,
    {
        self.state
            .connect(stage, FutureGenerator::new(future_generator), name.into())
    }

    #[inline]
    #[must_use]
    pub fn push(&self) -> BorrowingFfiFuture<'_, RResult<crate::Frame, Error>> {
        self.state.push()
    }
}

impl Clone for Implementation {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!(
            "we use this struct only inside an RArc (as a Trait_TO trait object) which shouldn't \
            clone"
        )
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        let _filter_drop_result = take(&mut self.filter)
            .into_iter()
            .map(drop)
            .collect::<SmallVec<[(); 4]>>();

        let shutdown_sender = self.shutdown_sender.clone();

        let task = self.task.clone();

        let (sender, receiver) = sync_channel(1);

        self.runtime.spawn(async move {
            Self::drop_impl(shutdown_sender, task).await;

            if sender.send(()).is_err() {
                unreachable!();
            }
        });

        if receiver.recv().is_err() {
            unreachable!();
        }
    }
}

impl Trait for Implementation {
    #[inline]
    fn connect<'a>(
        &'a self,
        stage: Stage,
        future_generator: FutureGenerator,
        name: RStr<'a>,
    ) -> BorrowingFfiFuture<'a, RResult<Connection, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            let filter_index_result = match stage {
                Stage::User => Ok(0),
                Stage::Prepare => Ok(1),
                Stage::Render => Ok(2),
                Stage::Present => Ok(3),
                _ => Err(invalid_argument(&format!(
                    "expected 'stage' to be one of: {:?}, got: {stage:?}",
                    [Stage::User, Stage::Prepare, Stage::Render, Stage::Present]
                ))),
            };

            match filter_index_result {
                Err(error) => RErr(error),
                Ok(filter_index) => {
                    let result = match self.filter.as_ref() {
                        None => unreachable!("filter should only be taken when dropping self"),
                        Some(filter) => {
                            filter
                                .lock()
                                .await
                                .index_mut(filter_index)
                                .connect(future_generator, name.into())
                                .await
                        }
                    };

                    match result {
                        Ok(connection) => ROk(connection),
                        Err(error) => RErr(error),
                    }
                }
            }
        })
    }

    #[inline]
    fn push(&self) -> BorrowingFfiFuture<'_, RResult<crate::Frame, Error>> {
        async_ffi::FutureExt::into_ffi(async move {
            let mut unused_frames: SmallVec<[Frame; RECOMMEND_MULTI_BUFFERING]> = SmallVec::new();
            let mut frame_states: SmallVec<[crate::state::State; RECOMMEND_MULTI_BUFFERING]> =
                SmallVec::new();

            let frame_state_result = self
                .push_frame_and_get_state(&mut unused_frames, &mut frame_states)
                .await;

            let returned_unused_frames_results = self
                .return_unused_frames_and_states(unused_frames, frame_states)
                .await;

            match returned_unused_frames_results {
                Err(error) => RErr(error),
                Ok(()) => match frame_state_result {
                    Err(error) => RErr(error),
                    Ok(state) => {
                        let mut frame_state_sender = self.frame_state_sender.clone();

                        let state_clone = state.clone();

                        let runtime = Handle::current().into();

                        let drop = async_ffi::FutureExt::into_ffi(
                            lazy(|_| async move {
                                let _frame_state_send_result =
                                    frame_state_sender.send(state_clone).await;

                                [0u8; 0]
                            })
                            .await,
                        );

                        ROk(crate::Frame::new(state, runtime, drop))
                    }
                },
            }
        })
    }
}

impl Unpin for Pipeline {}

#[cfg(test)]
pub(crate) mod tests {
    use {
        crate::{
            config::RECOMMEND_MULTI_BUFFERING,
            pipeline::{filter::callback::stable_abi::FutureGeneratorTrait, Pipeline, Stage},
        },
        abi_stable::std_types::RResult::{self, ROk},
        async_ffi::BorrowingFfiFuture,
        async_lock::Barrier,
        fenrir_error::Error,
        futures::future::join_all,
        rand::{thread_rng, Rng},
        std::sync::Arc,
        tokio::test,
    };

    #[derive(Clone, Debug)]
    struct WaitForBarrierFutureGenerator {
        barrier: Arc<Barrier>,
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn one_pipeline_iteration(pipeline: Pipeline) {
        let frame = pipeline.push().await;

        assert!(frame.is_ok());

        let stage = frame.unwrap().await;

        assert!(stage.is_ok());

        assert_eq!(Stage::Present, stage.unwrap());
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn two_pipeline_iterations(pipeline: Pipeline) {
        let frames = join_all([pipeline.push(), pipeline.push()])
            .await
            .into_iter()
            .map(|frame| {
                assert!(frame.is_ok());
                frame.unwrap()
            });

        join_all(frames).await.into_iter().for_each(|stage| {
            assert!(stage.is_ok());
            assert_eq!(Stage::Present, stage.unwrap());
        });
    }

    impl WaitForBarrierFutureGenerator {
        #[inline]
        fn new() -> Self {
            Self {
                barrier: Arc::new(Barrier::new(2)),
            }
        }

        #[inline]
        #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
        async fn generate_future_impl(barrier: Arc<Barrier>) -> RResult<(), Error> {
            let _ = barrier.wait().await;

            ROk(())
        }
    }

    impl FutureGeneratorTrait for WaitForBarrierFutureGenerator {
        #[inline]
        fn generate_future(&mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>> {
            let barrier = self.barrier.clone();

            async_ffi::FutureExt::into_ffi(Self::generate_future_impl(barrier))
        }
    }

    #[inline]
    fn wait_for_barrier_future_generator() -> WaitForBarrierFutureGenerator {
        WaitForBarrierFutureGenerator::new()
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        let pipeline = Pipeline::new().await;

        assert!(pipeline.is_ok());
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(crate) async fn pipeline() -> Pipeline {
        let pipeline = Pipeline::new().await;

        assert!(pipeline.is_ok());

        pipeline.unwrap()
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn one_iteration() {
        let pipeline = Pipeline::new().await.unwrap();

        let frame_state_result = pipeline.push().await;

        assert!(frame_state_result.is_ok());

        let mut frame_state = frame_state_result.unwrap();

        let expected_stages = [
            Stage::Enqueue,
            Stage::User,
            Stage::Enqueue,
            Stage::Prepare,
            Stage::Enqueue,
            Stage::Render,
            Stage::Enqueue,
            Stage::Present,
        ];

        for expected_stage in expected_stages {
            let stage = frame_state.next_stage().await;

            assert!(stage.is_ok());

            assert_eq!(expected_stage, stage.unwrap());
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 2)]
    async fn multiple_iteratios() {
        let pipeline = Pipeline::new().await.unwrap();

        let expected_stages = [
            Stage::Enqueue,
            Stage::User,
            Stage::Enqueue,
            Stage::Prepare,
            Stage::Enqueue,
            Stage::Render,
            Stage::Enqueue,
            Stage::Present,
        ];

        for _ in 0..thread_rng().gen_range(2..RECOMMEND_MULTI_BUFFERING) {
            let frame_state_result = pipeline.push().await;

            assert!(frame_state_result.is_ok());

            let mut frame_state = frame_state_result.unwrap();

            for expected_stage in expected_stages {
                let stage = frame_state.next_stage().await;

                assert!(stage.is_ok());

                assert_eq!(expected_stage, stage.unwrap());
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 2)]
    async fn connection_one_iteration() {
        let wait_for_barrier_future_generator = wait_for_barrier_future_generator();

        let barrier = wait_for_barrier_future_generator.barrier.clone();

        let pipeline = Pipeline::new().await.unwrap();

        let connection = pipeline
            .connect(
                Stage::User,
                wait_for_barrier_future_generator,
                "connection_one_iteration",
            )
            .await;

        assert!(connection.is_ok());

        let frame_state_result = pipeline.push().await;

        assert!(frame_state_result.is_ok());

        let mut frame_state = frame_state_result.unwrap();

        {
            let expected_stages = [Stage::Enqueue, Stage::User];

            for expected_stage in expected_stages {
                let stage = frame_state.next_stage().await;

                assert!(stage.is_ok());

                assert_eq!(expected_stage, stage.unwrap());
            }

            let _ = barrier.wait().await;
        }

        let expected_stages = [
            Stage::Enqueue,
            Stage::Prepare,
            Stage::Enqueue,
            Stage::Render,
            Stage::Enqueue,
            Stage::Present,
        ];

        for expected_stage in expected_stages {
            let stage = frame_state.next_stage().await;

            assert!(stage.is_ok());

            assert_eq!(expected_stage, stage.unwrap());
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn connection_multiple_iteratios() {
        let wait_for_barrier_future_generator = wait_for_barrier_future_generator();

        let barrier = wait_for_barrier_future_generator.barrier.clone();

        let pipeline = Pipeline::new().await.unwrap();

        let connection = pipeline
            .connect(
                Stage::User,
                wait_for_barrier_future_generator,
                "connection_multiple_iteratios",
            )
            .await;

        assert!(connection.is_ok());

        for _ in 0..thread_rng().gen_range(2..RECOMMEND_MULTI_BUFFERING) {
            let frame_state_result = pipeline.push().await;

            assert!(frame_state_result.is_ok());

            let mut frame_state = frame_state_result.unwrap();

            {
                let expected_stages = [Stage::Enqueue, Stage::User];

                for expected_stage in expected_stages {
                    let stage = frame_state.next_stage().await;

                    assert!(stage.is_ok());

                    assert_eq!(expected_stage, stage.unwrap());
                }

                let _ = barrier.wait().await;
            }

            let expected_stages = [
                Stage::Enqueue,
                Stage::Prepare,
                Stage::Enqueue,
                Stage::Render,
                Stage::Enqueue,
                Stage::Present,
            ];

            for expected_stage in expected_stages {
                let stage = frame_state.next_stage().await;

                assert!(stage.is_ok());

                assert_eq!(expected_stage, stage.unwrap());
            }
        }
    }
}
