use {
    crate::{
        pipeline::filter::callback::stable_abi::FutureGeneratorTrait,
        texture::manager::{render_task::handle::Handle, Data},
    },
    abi_stable::std_types::RResult,
    alloc::sync::Arc,
    async_ffi::FfiFuture,
    async_lock::Mutex,
    fenrir_error::{connection_shutdown, state_not_recoverable, Error},
    futures::{
        channel::mpsc::{Receiver, Sender},
        SinkExt, StreamExt,
    },
};

extern crate alloc;

pub(crate) mod handle;
pub(crate) mod subscription;

type Service = crate::subscription::Service<(), ()>;

pub type Subscription = crate::subscription::Subscription<(), ()>;

#[derive(Debug)]
pub(super) struct RenderTask {
    data_receiver: Arc<Mutex<Receiver<Data>>>,
    data_sender: Sender<Data>,
    handle: Handle,
}

impl RenderTask {
    #[inline]
    pub(super) fn new(
        data_receiver: Arc<Mutex<Receiver<Data>>>,
        data_sender: Sender<Data>,
        runtime: sleipnir::runtime::Handle,
    ) -> Self {
        let handle = Handle::new(runtime);

        Self {
            data_receiver,
            data_sender,
            handle,
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn callback(
        data_receiver: Arc<Mutex<Receiver<Data>>>,
        service: Handle,
        mut data_sender: Sender<Data>,
    ) -> Result<(), Error> {
        let data = Self::get_data(data_receiver).await?;

        match service.next().await {
            Some(item) => item.await,
            None => Err(state_not_recoverable(
                "constructing a new service item must not fail",
            )),
        }?;

        let send_result = data_sender.send(data).await;

        match send_result {
            Ok(()) => Ok(()),
            Err(_) => Err(connection_shutdown()),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_data(receiver: Arc<Mutex<Receiver<Data>>>) -> Result<Data, Error> {
        (receiver.lock().await.next().await).map_or_else(|| Err(connection_shutdown()), Ok)
    }

    #[inline]
    pub(crate) fn handle(&self) -> Handle {
        self.handle.clone()
    }
}

impl FutureGeneratorTrait for RenderTask {
    #[inline]
    fn generate_future(&mut self) -> FfiFuture<RResult<(), Error>> {
        let data_receiver = self.data_receiver.clone();

        let handle = self.handle.clone();

        let data_sender = self.data_sender.clone();

        async_ffi::FutureExt::into_ffi(async move {
            Self::callback(data_receiver, handle, data_sender)
                .await
                .into()
        })
    }
}
