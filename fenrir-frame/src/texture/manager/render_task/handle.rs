use {
    crate::texture::manager::render_task::{
        handle::stable_abi::{Trait, Trait_TO},
        subscription::service::Item,
        Service, Subscription,
    },
    abi_stable::{
        sabi_trait::TD_Opaque,
        std_types::{RArc, ROption},
        StableAbi,
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::RwLock,
    futures::StreamExt,
};

extern crate alloc;

mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::texture::manager::render_task::{subscription::service::Item, Subscription},
        abi_stable::{sabi_trait, std_types::ROption},
        async_ffi::BorrowingFfiFuture,
    };

    #[sabi_trait]
    pub(super) trait Trait: Clone + Debug + Send + Sync + 'static {
        fn next(&self) -> BorrowingFfiFuture<'_, ROption<Item>>;

        fn subscribe(&self) -> BorrowingFfiFuture<'_, Subscription>;
    }
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Handle {
    inner: Trait_TO<RArc<()>>,
}

#[derive(Debug)]
struct Inner {
    service: RwLock<Service>,
}

impl Handle {
    #[inline]
    pub(super) fn new(runtime: sleipnir::runtime::Handle) -> Self {
        let service = RwLock::new(Service::new(runtime));

        let inner = Trait_TO::from_ptr(RArc::new(Inner { service }), TD_Opaque);

        Self { inner }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub(super) async fn next(&self) -> Option<Item> {
        self.inner.next().await.into()
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn subscribe(&self) -> Subscription {
        self.inner.subscribe().await
    }
}

impl Clone for Inner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!()
    }
}

impl Trait for Inner {
    #[inline]
    fn next(&self) -> BorrowingFfiFuture<'_, ROption<Item>> {
        async move { self.service.write().await.next().await.into() }.into_ffi()
    }

    #[inline]
    fn subscribe(&self) -> BorrowingFfiFuture<'_, Subscription> {
        async move { self.service.read().await.subscribe() }.into_ffi()
    }
}

unsafe impl Send for Handle {}
unsafe impl Sync for Handle {}
