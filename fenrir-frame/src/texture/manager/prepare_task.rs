pub use crate::texture::manager::prepare_task::subscription::Subscription;
use {
    crate::{
        pipeline::filter::callback::stable_abi::FutureGeneratorTrait,
        texture::{
            manager::{prepare_task::handle::Handle, Data, Message},
            state::State,
        },
    },
    abi_stable::std_types::{
        RResult,
        RResult::{RErr, ROk},
    },
    alloc::sync::Arc,
    async_ffi::FfiFuture,
    async_lock::{Mutex, RwLockWriteGuard},
    core::{mem::swap, ops::Range},
    fenrir_error::{
        argument_out_of_domain, connection_shutdown, invalid_argument, state_not_recoverable, Error,
    },
    fenrir_hal::texture::{Metadata, Texture},
    futures::{
        channel::mpsc::{Receiver, Sender},
        stream::{self, select},
        FutureExt, SinkExt, StreamExt,
    },
};

extern crate alloc;

pub mod handle;
pub mod subscription;

#[derive(Clone, Debug)]
pub(super) struct PrepareTask {
    receiver: Arc<Mutex<Receiver<Data>>>,
    sender: Sender<Data>,
    handle: Handle,
}

impl PrepareTask {
    #[inline]
    pub(super) fn new(
        receiver: Arc<Mutex<Receiver<Data>>>,
        sender: Sender<Data>,
        runtime: sleipnir::runtime::Handle,
    ) -> Self {
        let handle = Handle::new(runtime);

        Self {
            receiver,
            sender,
            handle,
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn callback(
        receiver: Arc<Mutex<Receiver<Data>>>,
        handle: Handle,
        mut sender: Sender<Data>,
    ) -> Result<(), Error> {
        let mut subscription_service_item = (handle.next().await).map_or_else(
            || {
                Err(state_not_recoverable(
                    "getting the next subscription service item must not fail",
                ))
            },
            Ok,
        )?;

        let mut data = Self::get_data(receiver).await?;

        {
            enum MessageWrapper {
                ManagerMessage(crate::texture::manager::Message),
                SubscriptionMessage(crate::texture::manager::prepare_task::subscription::Message),
            }

            let mut data_receiver = data.receiver.lock().await;

            let mut cache = Vec::new();

            swap(&mut data.cache, &mut cache);

            while let Some(maybe_message) = data_receiver.next().now_or_never() {
                match maybe_message {
                    None => {
                        return Err(connection_shutdown());
                    }
                    Some(message) => {
                        cache.push(message);
                    }
                }
            }

            let mut textures = data.textures.write().await;

            {
                let (service_message_sender, subscription_message_stream) =
                    subscription_service_item
                        .sender_and_stream()
                        .expect("sender_and_stream() must be called only once");

                let mut service_message_sender = Some(service_message_sender);

                let mut messages = select(
                    #[allow(clippy::iter_with_drain)]
                    stream::iter(cache.drain(..)).map(MessageWrapper::ManagerMessage),
                    subscription_message_stream.map(MessageWrapper::SubscriptionMessage),
                );

                while let Some(message_wrapper) = messages.next().await {
                    match message_wrapper {
                        MessageWrapper::ManagerMessage(message) => {
                            Self::process_manager_message(
                                message,
                                &mut textures,
                                &data,
                                &mut service_message_sender,
                            )
                            .await?;
                        }
                        MessageWrapper::SubscriptionMessage(message) => {
                            Self::process_subscription_message(
                                message,
                                &textures,
                                data.index,
                                &mut service_message_sender,
                            )
                            .await;
                        }
                    }
                }
            }

            data.cache = cache;
        }

        subscription_service_item.await?;

        let sender_result = sender.send(data).await;

        match sender_result {
            Ok(()) => Ok(()),
            Err(_) => Err(connection_shutdown()),
        }
    }

    #[inline]
    fn check_unfailable_result<E>(result: &Result<(), E>) {
        if result.is_err() {
            unreachable!();
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_data(receiver: Arc<Mutex<Receiver<Data>>>) -> Result<Data, Error> {
        (receiver.lock().await.next().await).map_or_else(|| Err(connection_shutdown()), Ok)
    }

    #[inline]
    fn get_texture(textures: &[Option<Texture>], index: usize) -> Result<&Texture, Error> {
        match textures.get(index) {
            None => Err(argument_out_of_domain(
                Range {
                    start: 0.into(),
                    end: textures.len().into(),
                },
                index.into(),
            )),
            Some(None) => Err(invalid_argument(&format!(
                "textures at {index} doesn't contain an texture"
            ))),
            Some(Some(texture)) => Ok(texture),
        }
    }

    #[inline]
    pub(super) fn handle(&self) -> Handle {
        self.handle.clone()
    }

    #[inline]
    fn insert_texture(
        textures: &mut RwLockWriteGuard<'_, Vec<Option<Texture>>>,
        texture: Texture,
    ) -> usize {
        if let Some(index) = textures.iter_mut().position(|texture_| texture_.is_none()) {
            textures[index] = Some(texture);

            index
        } else {
            let index = textures.len();

            textures.push(Some(texture));

            index
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_texture(
        textures: &mut RwLockWriteGuard<'_, Vec<Option<Texture>>>,
        metadata: Metadata,
    ) -> Result<(usize, Texture), Error> {
        let device = metadata.device();

        match device.new_texture(metadata).await {
            Err(error) => Err(error),
            Ok(texture) => Ok(
                if let Some(index) = textures.iter_mut().position(|texture_| texture_.is_none()) {
                    textures[index] = Some(texture.clone());

                    (index, texture)
                } else {
                    let index = textures.len();

                    {
                        textures.push(Some(texture.clone()));

                        (index, texture)
                    }
                },
            ),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn process_manager_message<'a>(
        message: crate::texture::manager::Message,
        textures: &mut RwLockWriteGuard<'_, Vec<Option<Texture>>>,
        data: &Data,
        _subscription_service_message_sender: &mut Option<
            crate::subscription::service::item::Sender<
                'a,
                crate::texture::manager::prepare_task::subscription::service::Message,
            >,
        >,
    ) -> Result<(), Error> {
        match message {
            Message::New(metadata, sender, texture_manager) => {
                let mut indices = [0; 2];

                let (index, texture) = Self::new_texture(textures, metadata.clone()).await?;

                indices[data.index] = index;

                Self::check_unfailable_result(&data.sender.unbounded_send(
                    Message::NewContinuation(indices, sender, texture_manager, texture),
                ));
            }
            Message::NewContinuation(mut indices, mut sender, texture_manager, texture) => {
                indices[data.index] = Self::insert_texture(textures, texture);

                Self::check_unfailable_result(
                    &sender.send(State::new(texture_manager, indices)).await,
                );
            }
            Message::Metadata(indices, mut sender) => {
                let texture = Self::get_texture(textures, indices[data.index]);

                Self::check_unfailable_result(&match texture {
                    Ok(image) => sender.send(Ok(image.metadata())).await,
                    Err(error) => sender.send(Err(error)).await,
                });
            }
            Message::DropImage(indices) => {
                textures[indices[data.index]] = None;

                Self::check_unfailable_result(
                    &data
                        .sender
                        .unbounded_send(Message::DropImageContinuation(indices)),
                );
            }
            Message::DropImageContinuation(indices) => {
                textures[indices[data.index]] = None;
            }
        };

        Ok(())
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn process_subscription_message<'a>(
        message: crate::texture::manager::prepare_task::subscription::Message,
        textures: &RwLockWriteGuard<'_, Vec<Option<Texture>>>,
        data_index: usize,
        subscription_service_message_sender: &mut Option<
            crate::subscription::service::item::Sender<
                'a,
                crate::texture::manager::prepare_task::subscription::service::Message,
            >,
        >,
    ) {
        match message {
            crate::texture::manager::prepare_task::
            subscription::Message::QueryTexture(state, mut sender) => {
                let index = state.indices()[data_index];

                let get_texture_result = Self::get_texture(textures, index);

                let texture_result = match get_texture_result {
                    Ok(texture) => ROk(texture.clone()),
                    Err(error) => RErr(error),
                };

                Self::check_unfailable_result(&sender.send(texture_result).await);
            },
                crate::texture::manager::prepare_task::subscription::Message::CloseServiceMessageStream => {

                *subscription_service_message_sender = None;
            }
        }
    }
}

impl FutureGeneratorTrait for PrepareTask {
    #[inline]
    fn generate_future(&mut self) -> FfiFuture<RResult<(), Error>> {
        let receiver = self.receiver.clone();

        let handle = self.handle.clone();

        let sender = self.sender.clone();

        async_ffi::FutureExt::into_ffi(async move {
            Self::callback(receiver, handle, sender).await.into()
        })
    }
}
