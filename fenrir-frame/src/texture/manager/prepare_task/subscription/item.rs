use {
    crate::texture::State,
    abi_stable::StableAbi,
    fenrir_error::{connection_shutdown, Error},
    fenrir_hal::texture::Texture,
    futures::{SinkExt, StreamExt},
    sleipnir::ffi::sync::mpsc::channel,
};

type Inner = crate::subscription::item::Item<
    crate::texture::manager::prepare_task::subscription::service::Message,
    crate::texture::manager::prepare_task::subscription::Message,
>;

#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<'a> {
    inner: crate::subscription::item::Sender<
        'a,
        crate::texture::manager::prepare_task::subscription::Message,
    >,
}

type Stream<'a> = crate::subscription::item::Stream<
    'a,
    crate::texture::manager::prepare_task::subscription::service::Message,
>;

#[repr(C)]
#[derive(StableAbi)]
pub struct Item {
    inner: Inner,
}

impl Item {
    #[inline]
    pub(super) const fn new(inner: Inner) -> Self {
        Self { inner }
    }

    #[inline]
    pub fn sender_and_stream(&mut self) -> Option<(Sender<'_>, Stream<'_>)> {
        self.inner
            .sender_and_stream()
            .map(|(inner, stream)| (Sender { inner }, stream))
    }
}

impl<'a> Sender<'a> {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn close_service_message_stream(&mut self) -> Result<(), Error> {
        self
            .inner
            .send(
                crate::texture::manager::prepare_task::subscription::Message::CloseServiceMessageStream
            )
            .await
            .map_or_else(|_| Err(connection_shutdown()), Ok)
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn query_texture(&mut self, state: State) -> Result<Texture, Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = self
            .inner
            .send(
                crate::texture::manager::prepare_task::subscription::Message::QueryTexture(
                    state, sender,
                ),
            )
            .await;

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => (receiver.next().await)
                .map_or_else(|| Err(connection_shutdown()), core::convert::Into::into),
        }
    }
}
