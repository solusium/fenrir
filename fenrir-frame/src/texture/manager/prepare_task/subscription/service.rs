use abi_stable::StableAbi;

pub type Item = crate::subscription::service::Item<
    Message,
    crate::texture::manager::prepare_task::subscription::Message,
>;

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub enum Message {
    ModifiedTexture,
}
