pub use crate::texture::manager::prepare_task::subscription::item::Item;
use {
    abi_stable::{std_types::RResult, StableAbi},
    core::{
        pin::{pin, Pin},
        task::{Context, Poll},
    },
    fenrir_error::Error,
    fenrir_hal::texture::Texture,
    futures::Stream,
    sleipnir::ffi::sync::mpsc::Sender,
};

pub mod item;
pub mod service;

pub type Service = crate::subscription::Service<
    crate::texture::manager::prepare_task::subscription::service::Message,
    Message,
>;

type Inner = crate::subscription::Subscription<
    crate::texture::manager::prepare_task::subscription::service::Message,
    crate::texture::manager::prepare_task::subscription::Message,
>;

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub enum Message {
    QueryTexture(
        crate::texture::state::State,
        Sender<RResult<Texture, Error>>,
    ),
    CloseServiceMessageStream,
}

#[repr(C)]
#[derive(Debug, StableAbi)]
pub struct Subscription {
    inner: Inner,
}

impl Subscription {
    #[inline]
    pub(super) const fn new(inner: Inner) -> Self {
        Self { inner }
    }
}

impl Stream for Subscription {
    type Item = Result<Item, Error>;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        match pin!(&mut this.inner).poll_next(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(maybe_item_inner_result) => maybe_item_inner_result.map_or_else(
                || Poll::Ready(None),
                |item_inner_result| match item_inner_result {
                    Err(error) => Poll::Ready(Some(Err(error))),
                    Ok(item_inner) => Poll::Ready(Some(Ok(Item::new(item_inner)))),
                },
            ),
        }
    }
}
