use {
    crate::texture::manager::Manager,
    abi_stable::{std_types::RArc, StableAbi},
};

#[repr(C)]
#[derive(Debug, StableAbi)]
struct Implementation {
    manager: Manager,
    indices: [usize; 2],
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct State {
    implementation: RArc<Implementation>,
}

impl State {
    #[inline]
    pub(crate) fn new(manager: Manager, indices: [usize; 2]) -> Self {
        let implementation = RArc::new(Implementation { manager, indices });

        Self { implementation }
    }

    #[inline]
    #[must_use]
    #[allow(dead_code)]
    pub(crate) fn indices(&self) -> [usize; 2] {
        self.implementation.indices
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        if self.manager.drop_texture(self.indices).is_err() {
            unreachable!();
        }
    }
}
