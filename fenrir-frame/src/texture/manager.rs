use {
    crate::{
        pipeline::{
            filter::{callback::stable_abi::FutureGeneratorTrait, connection::Connection},
            Pipeline, Stage,
        },
        texture::{
            manager::{
                prepare_task::PrepareTask,
                render_task::RenderTask,
                stable_abi::{Trait, Trait_TO},
            },
            state::State,
        },
    },
    abi_stable::{
        sabi_trait,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
        },
        StableAbi,
    },
    alloc::sync::Arc,
    async_ffi::FfiFuture,
    async_lock::{Mutex, RwLock},
    core::mem::take,
    fenrir_error::{connection_shutdown, Error},
    fenrir_hal::texture::{Metadata, Texture},
    futures::{
        channel::mpsc::{channel, unbounded, Receiver, Sender, UnboundedReceiver, UnboundedSender},
        future::join_all,
        stream, FutureExt, SinkExt, StreamExt,
    },
    sleipnir::runtime::Handle,
    smallvec::SmallVec,
    std::sync::mpsc::sync_channel,
};

extern crate alloc;

pub(crate) mod prepare_task;
pub(crate) mod render_task;

#[derive(Clone, Debug)]
enum Message {
    New(Metadata, Sender<State>, Manager),
    NewContinuation([usize; 2], Sender<State>, Manager, Texture),
    Metadata([usize; 2], Sender<Result<Metadata, Error>>),
    DropImage([usize; 2]),
    DropImageContinuation([usize; 2]),
}

mod stable_abi {

    #![allow(
        clippy::cast_ptr_alignment,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::texture::{manager::Manager, state::State},
        abi_stable::{sabi_trait, std_types::RResult},
        async_ffi::FfiFuture,
        fenrir_error::Error,
        fenrir_hal::texture::Metadata,
    };

    #[sabi_trait]
    pub(super) trait Trait: Clone + Debug + Send + Sync + 'static {
        fn drop_texture(&self, first_index: usize, second_index: usize) -> RResult<(), Error>;

        fn metadata(
            &self,
            first_index: usize,
            second_index: usize,
        ) -> FfiFuture<RResult<Metadata, Error>>;

        fn new_texture(
            &self,
            metadata: Metadata,
            this: Manager,
        ) -> FfiFuture<RResult<State, Error>>;

        fn prepare_task(&self) -> crate::texture::manager::prepare_task::handle::Handle;

        fn render_task(&self) -> crate::texture::manager::render_task::handle::Handle;
    }
}

#[derive(Clone, Debug)]
pub(crate) struct Data {
    receiver: Arc<Mutex<UnboundedReceiver<Message>>>,
    cache: Vec<Message>,
    textures: Arc<RwLock<Vec<Option<Texture>>>>,
    sender: UnboundedSender<Message>,
    index: usize,
}

#[derive(Debug)]
struct Inner {
    message_sender: UnboundedSender<Message>,
    data_receivers: [Arc<Mutex<Receiver<Data>>>; 2],
    prepare_task_handle: crate::texture::manager::prepare_task::handle::Handle,
    render_task_handle: crate::texture::manager::render_task::handle::Handle,
    connections: Arc<Mutex<Option<[Connection; 2]>>>,
    runtime: Handle,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Manager {
    inner: Trait_TO<RArc<()>>,
}

impl Data {
    const fn new(
        receiver: Arc<Mutex<UnboundedReceiver<Message>>>,
        textures: Arc<RwLock<Vec<Option<Texture>>>>,
        sender: UnboundedSender<Message>,
        index: usize,
    ) -> Self {
        Self {
            receiver,
            cache: Vec::new(),
            textures,
            sender,
            index,
        }
    }
}

impl Inner {
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new(pipeline: Pipeline) -> Self {
        let mut data_channels = (0..2)
            .map(|_| channel(2))
            .collect::<SmallVec<[(Sender<Data>, Receiver<Data>); 2]>>();

        let (message_sender, message_receiver) = unbounded();

        let message_receiver_arc = Arc::new(Mutex::new(message_receiver));

        let images = [
            Arc::new(RwLock::new(Vec::new())),
            Arc::new(RwLock::new(Vec::new())),
        ];

        {
            let first_data_sender_ref = &data_channels[0].0;

            let message_receiver_ref = &message_receiver_arc;
            let message_sender_ref = &message_sender;

            let bootstrap_data_futures =
                images.iter().enumerate().map(|(index, images)| async move {
                    first_data_sender_ref
                        .clone()
                        .send(Data::new(
                            message_receiver_ref.clone(),
                            images.clone(),
                            message_sender_ref.clone(),
                            index,
                        ))
                        .await
                });

            let bootstrap_data_results = join_all(bootstrap_data_futures).await;

            bootstrap_data_results
                .into_iter()
                .try_fold((), |(), current| match current {
                    Ok(()) => Ok(()),
                    Err(_) => Err(connection_shutdown()),
                }).expect("channel can't be closed since both we contain both components in data_channels");
        }

        data_channels.reverse();

        let (first_data_sender, first_data_receiver) = data_channels
            .pop()
            .expect("we constructed data_channels with 2 elements");
        let (second_data_sender, second_data_receiver) = data_channels
            .pop()
            .expect("we constructed data_channels with 2 elements");

        let data_receivers = [
            Arc::new(Mutex::new(first_data_receiver)),
            Arc::new(Mutex::new(second_data_receiver)),
        ];

        let runtime = Handle::current();

        let prepare_task = PrepareTask::new(
            data_receivers[0].clone(),
            second_data_sender,
            runtime.clone(),
        );
        let render_task = RenderTask::new(
            data_receivers[1].clone(),
            first_data_sender,
            runtime.clone(),
        );

        let prepare_task_handle = prepare_task.handle();

        let render_task_handle = render_task.handle();

        let connections = Arc::new(Mutex::new(Some([
            Self::connect_to_pipeline(&pipeline, Stage::Prepare, prepare_task)
                .await
                .expect("'Prepare' must be a valid pipeline stage"),
            Self::connect_to_pipeline(&pipeline, Stage::Render, render_task)
                .await
                .expect("'Render' must be a valid pipeline stage"),
        ])));

        Self {
            message_sender,
            data_receivers,
            prepare_task_handle,
            render_task_handle,
            connections,
            runtime,
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn connect_to_pipeline<FG>(
        pipeline: &Pipeline,
        stage: Stage,
        future_generator: FG,
    ) -> Result<Connection, Error>
    where
        FG: FutureGeneratorTrait + 'static,
    {
        let connect_result = pipeline
            .connect(
                stage,
                future_generator,
                "fenrir_frame::texture::manager::Manager",
            )
            .await;

        match connect_result {
            ROk(connection) => Ok(connection),
            RErr(error) => Err(error),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop(
        connections: Arc<Mutex<Option<[Connection; 2]>>>,
        data_receivers: [Arc<Mutex<Receiver<Data>>>; 2],
        sender: std::sync::mpsc::SyncSender<()>,
    ) {
        let mut maybe_connections = connections.lock().await;

        match take(&mut *maybe_connections) {
            None => unreachable!(),

            Some(connections) => {
                drop(maybe_connections);

                for connection in connections {
                    let _disconnect_result = connection.disconnect().await;
                }
            }
        }

        Self::process_remaining_messages(Self::get_datas(data_receivers).await).await;

        if sender.send(()).is_err() {
            unreachable!();
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn get_datas(data_receivers: [Arc<Mutex<Receiver<Data>>>; 2]) -> SmallVec<[Data; 2]> {
        stream::iter(data_receivers)
            .map(|data_receiver| async move {
                let mut datas = SmallVec::<[Data; 2]>::new();

                let mut data_receiver_lock = data_receiver.lock().await;

                while let Some(Some(data)) = data_receiver_lock.next().now_or_never() {
                    datas.push(data);
                }

                datas
            })
            .fold(SmallVec::<[Data; 2]>::new(), |current, datas| async move {
                let mut next = current.clone();

                next.append(&mut datas.await);

                next
            })
            .await
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn metadata(
        message_sender: UnboundedSender<Message>,
        indices: [usize; 2],
    ) -> Result<Metadata, Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = message_sender.unbounded_send(Message::Metadata(indices, sender));

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => (receiver.next().await).map_or_else(
                || Err(connection_shutdown()),
                |metadata_result| metadata_result,
            ),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn new_texture(
        message_sender: UnboundedSender<Message>,
        metadata: Metadata,
        this: Manager,
    ) -> Result<State, Error> {
        let (sender, mut receiver) = channel(1);

        let send_result = message_sender.unbounded_send(Message::New(metadata, sender, this));

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => (receiver.next().await).map_or_else(|| Err(connection_shutdown()), Ok),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn process_remaining_messages(datas: SmallVec<[Data; 2]>) {
        debug_assert_eq!(2, datas.len());

        let message_stream_arc = datas[0].receiver.clone();

        let mut message_stream = message_stream_arc.lock().await;

        let mut all_images = [
            datas[0].textures.write().await,
            datas[1].textures.write().await,
        ];

        let mut drop_images = |indices: [usize; 2]| {
            all_images
                .iter_mut()
                .zip(indices)
                .for_each(|(images, index)| {
                    if let Some(image) = images.get_mut(index) {
                        *image = None;
                    }
                });
        };

        while let Some(maybe_message) = message_stream.next().now_or_never() {
            if let Some(
                Message::NewContinuation(indices, _, _, _)
                | Message::DropImage(indices)
                | Message::DropImageContinuation(indices),
            ) = maybe_message
            {
                drop_images(indices);
            }
        }
    }
}

impl Manager {
    /// Construct a new texture manager connected to pipeline.
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new(pipeline: Pipeline) -> Self {
        let inner =
            Trait_TO::from_ptr(RArc::new(Inner::new(pipeline).await), sabi_trait::TD_Opaque);

        Self { inner }
    }

    /// Drop the texture represented by indices.
    ///
    /// # Errors
    ///
    /// If an internal channel shuts down, expected because of a panic, this function returns
    /// [`ConnectionShutdown`](fenrir_error::Error::ConnectionShutdown).
    #[inline]
    pub(crate) fn drop_texture(&self, indices: [usize; 2]) -> Result<(), Error> {
        self.inner.drop_texture(indices[0], indices[1]).into()
    }

    /// Get the metadata of a texture represented by indices.
    ///
    /// # Errors
    ///
    /// This function returns [`InvalidArgument`](fenrir_error::Error::InvalidArgument) if indices
    /// don't represent a texture.
    /// If an internal channel shuts down, expected because of a panic, this function returns
    /// [`ConnectionShutdown`](fenrir_error::Error::ConnectionShutdown).
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn metadata(&self, indices: [usize; 2]) -> Result<Metadata, Error> {
        self.inner.metadata(indices[0], indices[1]).await.into()
    }

    /// Constructs a new texture based on metadata.
    ///
    /// # Errors
    ///
    /// This function forwards errors thrown by the underlying graphics API for constructing a new
    /// texture.
    /// If an internal channel shuts down, expected because of a panic, this function returns
    /// [`ConnectionShutdown`](fenrir_error::Error::ConnectionShutdown).
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    pub async fn new_texture(&self, metadata: Metadata) -> Result<State, Error> {
        self.inner.new_texture(metadata, self.clone()).await.into()
    }

    #[inline]
    #[must_use]
    pub fn prepare_task(&self) -> crate::texture::manager::prepare_task::handle::Handle {
        self.inner.prepare_task()
    }

    #[inline]
    #[must_use]
    pub fn render_task(&self) -> crate::texture::manager::render_task::handle::Handle {
        self.inner.render_task()
    }
}

impl Clone for Inner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!()
    }
}

impl Drop for Inner {
    #[inline]
    fn drop(&mut self) {
        let connections = self.connections.clone();
        let data_receivers = self.data_receivers.clone();

        let (sender, receiver) = sync_channel(1);

        let _join_handle = self.runtime.spawn(async move {
            Self::drop(connections, data_receivers, sender).await;
        });

        if receiver.recv().is_err() {
            unreachable!()
        }
    }
}

impl Trait for Inner {
    #[inline]
    fn drop_texture(&self, first_index: usize, second_index: usize) -> RResult<(), Error> {
        let indices = [first_index, second_index];

        let drop_result = self
            .message_sender
            .unbounded_send(Message::DropImage(indices));

        drop_result.map_or_else(|_| RErr(connection_shutdown()), ROk)
    }

    #[inline]
    fn metadata(
        &self,
        first_index: usize,
        second_index: usize,
    ) -> FfiFuture<RResult<Metadata, Error>> {
        let message_sender = self.message_sender.clone();

        let indices = [first_index, second_index];

        async_ffi::FutureExt::into_ffi(async move {
            Self::metadata(message_sender, indices).await.into()
        })
    }

    #[inline]
    fn new_texture(&self, metadata: Metadata, this: Manager) -> FfiFuture<RResult<State, Error>> {
        let message_sender = self.message_sender.clone();

        async_ffi::FutureExt::into_ffi(async move {
            Self::new_texture(message_sender, metadata, this)
                .await
                .into()
        })
    }

    #[inline]
    fn prepare_task(&self) -> crate::texture::manager::prepare_task::handle::Handle {
        self.prepare_task_handle.clone()
    }

    #[inline]
    fn render_task(&self) -> crate::texture::manager::render_task::handle::Handle {
        self.render_task_handle.clone()
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use {
        crate::{
            pipeline::{
                tests::{pipeline, two_pipeline_iterations},
                Pipeline, Stage,
            },
            texture::{manager::Manager, state::State},
        },
        fenrir_hal::{device::Device, texture::Metadata},
        fenrir_render_graph::test::instances,
        rand::{rng, Rng},
        sleipnir::runtime::Handle,
    };

    pub(crate) fn new_metadata(
        device: Device,
        depth: usize,
        height: usize,
        width: usize,
        name: &str,
    ) -> Metadata {
        Metadata::new(device, width, height, depth, Vec::new(), name.to_string())
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn new_texture() {
        let name = "texture::manager::tests::new_texture";

        let pipeline = pipeline().await;

        let texture_manager = Manager::new(pipeline.clone()).await;

        let mut rng = rng();

        let runtime = Handle::current();

        for instance in instances(name).await {
            let devices = instance.devices();

            assert!(devices.is_ok());

            for device in devices.unwrap() {
                let width = 2usize.pow(rng.random_range(0..4));
                let height = 2usize.pow(rng.random_range(0..4));
                let depth = rng.random_range(1..4);

                let metadata = new_metadata(device, depth, height, width, name);

                let texture_manager_clone = texture_manager.clone();

                let image_state_task =
                    runtime.spawn(async move { texture_manager_clone.new_texture(metadata).await });

                two_pipeline_iterations(pipeline.clone()).await;

                {
                    let image_state_task_result = image_state_task.await;

                    assert!(image_state_task_result.is_ok());

                    let image_state_result = image_state_task_result.unwrap();

                    assert!(image_state_result.is_ok());
                }
            }
        }
    }

    async fn generate_texture_state(
        texture_manager: Manager,
        metadata: Metadata,
        pipeline: Pipeline,
    ) -> State {
        let texture_state_task =
            Handle::current().spawn(async move { texture_manager.new_texture(metadata).await });

        two_pipeline_iterations(pipeline).await;

        let image_state_task_result = texture_state_task.await;

        assert!(image_state_task_result.is_ok());

        let image_state_result = image_state_task_result.unwrap();

        assert!(image_state_result.is_ok());

        image_state_result.unwrap()
    }

    async fn get_metadata(
        texture_manager: Manager,
        image_state: State,
        pipeline: Pipeline,
    ) -> Metadata {
        let indices = image_state.indices();

        let task = Handle::current().spawn(async move { texture_manager.metadata(indices).await });

        let frame = pipeline.push().await;

        assert!(frame.is_ok());

        let stage = frame.unwrap().await;

        assert!(stage.is_ok());

        assert_eq!(Stage::Present, stage.unwrap());

        let task_result = task.await;

        assert!(task_result.is_ok());

        let metadata_result = task_result.unwrap();

        assert!(metadata_result.is_ok());

        metadata_result.unwrap()
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn texture_queries() {
        let mut rng = rng();

        let pipeline = pipeline().await;

        let texture_manager = Manager::new(pipeline.clone()).await;

        for instance in instances("texture::manager::tests::texture_queries").await {
            let devices = instance.devices();

            assert!(devices.is_ok());

            for device in devices.unwrap() {
                let width = 2usize.pow(rng.random_range(0..4));
                let height = 2usize.pow(rng.random_range(0..4));
                let depth = rng.random_range(1..4);

                let metadata = new_metadata(
                    device,
                    depth,
                    height,
                    width,
                    "texture::manager::tests::texture_queries",
                );

                let image_state =
                    generate_texture_state(texture_manager.clone(), metadata, pipeline.clone())
                        .await;

                let metadata =
                    get_metadata(texture_manager.clone(), image_state, pipeline.clone()).await;

                assert_eq!(depth, metadata.depth());

                assert_eq!(height, metadata.height());

                assert_eq!(width, metadata.width());
            }
        }
    }
}
