use {
    crate::{
        config::RECOMMEND_MULTI_BUFFERING,
        pipeline::{
            filter::{
                callback::{Adder, FutureGenerator},
                connection::Connection,
            },
            Action, Frame, Message, Stage,
        },
    },
    abi_stable::std_types::RResult::{RErr, ROk},
    fenrir_error::{connection_shutdown, Error},
    futures::{
        channel::mpsc::{channel, Receiver, Sender},
        future::{join_all, select, Either},
        lock::Mutex,
        SinkExt, StreamExt,
    },
    sleipnir::{pin, runtime::Handle, task::JoinHandle},
    std::{
        fmt::Debug,
        mem::take,
        ops::DerefMut,
        sync::{mpsc::sync_channel, Arc},
    },
    tracing::{enabled, error, Level},
};

pub mod callback;
pub mod connection;

#[cfg(test)]
mod tests;

pub(crate) struct Callback {
    pub future_generator: FutureGenerator,
    pub name: String,
}

#[derive(Clone, Debug)]
pub(crate) struct Filter {
    task: Arc<Mutex<JoinHandle<Result<(), Error>>>>,
    input_sender: Option<Sender<Message>>,
    shutdown_sender: Arc<Mutex<Option<futures::channel::oneshot::Sender<()>>>>,
    stage: Stage,
    runtime: Handle,
}

impl Filter {
    #[inline]
    pub(crate) fn new(runtime: Handle, stage: Stage, output_sender: Sender<Message>) -> Self {
        let (input_sender, input_receiver) = channel(RECOMMEND_MULTI_BUFFERING);

        let (shutdown_sender_, shutdown_receiver) = futures::channel::oneshot::channel();

        let shutdown_sender = Arc::new(Mutex::new(Some(shutdown_sender_)));

        let task = Arc::new(Mutex::new(runtime.spawn(Self::task(
            stage,
            input_receiver,
            output_sender,
            shutdown_receiver,
        ))));

        Self {
            task,
            input_sender: Some(input_sender),
            shutdown_sender,
            stage,
            runtime,
        }
    }

    #[inline]
    pub(crate) async fn connect(
        &mut self,
        future_generator: FutureGenerator,
        name_: &str,
    ) -> Result<Connection, Error> {
        let mut input_sender = self.input_sender().clone();

        let name = name_.to_string();

        let (sender, mut receiver) = channel(1);

        let send_result = input_sender
            .send(Message::Connect(Adder {
                callback: Callback {
                    future_generator,
                    name,
                },
                sender,
            }))
            .await;

        match send_result {
            Err(_) => Err(connection_shutdown()),
            Ok(()) => match receiver.next().await {
                None => Err(connection_shutdown()),
                Some(index) => Ok(Connection::new(self.runtime.clone(), input_sender, index)),
            },
        }
    }

    #[inline]
    pub(crate) fn input_sender(&mut self) -> &mut Sender<Message> {
        self.input_sender.as_mut().map_or_else(
            || unreachable!("input_sender should contain a value until dropping self"),
            |input_sender| input_sender,
        )
    }

    #[inline]
    pub(crate) async fn push(&mut self, mut frame: Frame) -> Result<(), Error> {
        match frame.set_stage(Stage::Enqueue).await {
            Err(error) => Err(error),
            Ok(()) => match self.input_sender().send(Message::Frame(frame)).await {
                Err(_) => Err(connection_shutdown()),
                Ok(()) => Ok(()),
            },
        }
    }

    #[inline]
    #[cfg(test)]
    pub(crate) const fn stage(&self) -> Stage {
        self.stage
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop_impl(
        _stage: Stage,
        sender: Arc<Mutex<Option<futures::channel::oneshot::Sender<()>>>>,
        task: Arc<Mutex<JoinHandle<Result<(), Error>>>>,
        _input_sender: Sender<Message>,
    ) {
        let mut sender_lock = sender.lock().await;

        if sender_lock.as_mut().is_some() {
            let _send_result = take(sender_lock.deref_mut()).map_or_else(
                || unreachable!("the shutdown_sender shouldn't be taken already"),
                |shutdown_sender| shutdown_sender.send(()),
            );

            let _task_result = task.lock().await.deref_mut().await;
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn iteration(
        stage: Stage,
        input_receiver: &mut Receiver<Message>,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
        callbacks: &mut Vec<Option<Callback>>,
        output_sender: &mut Sender<Message>,
    ) -> Result<Action, Error> {
        match select(input_receiver.next(), shutdown_receiver).await {
            Either::Left((maybe_message, shutdown_future)) => match maybe_message {
                None => Err(connection_shutdown()),
                Some(message) => match message {
                    Message::Connect(adder) => {
                        Self::add_callback(callbacks, adder, shutdown_future).await
                    }
                    Message::Disconnect(index, maybe_sender) => {
                        callbacks[index] = None;

                        match maybe_sender {
                            None => Ok(Action::Continue(shutdown_future)),
                            Some(mut sender) => match sender.send(()).await {
                                Ok(()) => Ok(Action::Continue(shutdown_future)),
                                Err(_) => Err(connection_shutdown()),
                            },
                        }
                    }
                    Message::Frame(frame) => {
                        Self::process_frame(stage, frame, callbacks, output_sender, shutdown_future)
                            .await
                    }
                },
            },
            Either::Right((_, _)) => Ok(Action::Shutdown),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn task(
        stage: Stage,
        mut input_receiver: Receiver<Message>,
        mut output_sender: Sender<Message>,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
    ) -> Result<(), Error> {
        let mut callbacks = Vec::new();

        let mut next_action = Self::iteration(
            stage,
            &mut input_receiver,
            shutdown_receiver,
            &mut callbacks,
            &mut output_sender,
        )
        .await?;

        while let Action::Continue(shutdown_future) = next_action {
            next_action = Self::iteration(
                stage,
                &mut input_receiver,
                shutdown_future,
                &mut callbacks,
                &mut output_sender,
            )
            .await?;
        }

        Ok(())
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn add_callback(
        callbacks: &mut Vec<Option<Callback>>,
        mut adder: Adder,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
    ) -> Result<Action, Error> {
        let index = if let Some(index_) = callbacks.iter().position(Option::is_none) {
            callbacks[index_] = Some(adder.callback);

            index_
        } else {
            let index_ = callbacks.len();

            callbacks.push(Some(adder.callback));

            index_
        };

        let send_future = adder.sender.send(index);

        pin!(send_future);

        match select(send_future, shutdown_receiver).await {
            Either::Left((result, shutdown_future)) => {
                result.map_or_else(|_| Err(connection_shutdown()), Ok)?;

                Ok(Action::Continue(shutdown_future))
            }
            Either::Right((_, shutdown_future)) => {
                (shutdown_future.await).map_or_else(|_| Err(connection_shutdown()), Ok)?;

                Ok(Action::Shutdown)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn execute_callbacks(
        callbacks: &mut [Option<Callback>],
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
        stage: Stage,
    ) -> Result<Action, (Error, futures::channel::oneshot::Receiver<()>)> {
        let join_callbacks = join_all(callbacks.iter_mut().filter_map(|callback_option| {
            callback_option
                .as_mut()
                .map(|callback| callback.future_generator.generate_future())
        }));

        match select(join_callbacks, shutdown_receiver).await {
            Either::Left((results, shutdown_future)) => {
                let result = results.iter().try_fold((), |(), result| match result {
                    ROk(()) => Ok(()),
                    RErr(error) => Err(error.clone()),
                });

                match result {
                    Ok(()) => Ok(Action::Continue(shutdown_future)),
                    Err(error) => {
                        if enabled!(Level::ERROR) {
                            error!("failed to execute callbacks in filter '{stage:?}': {error:?}");

                            let results_by_name = callbacks
                                .iter()
                                .filter_map(Option::as_ref)
                                .zip(results.into_iter())
                                .map(|(callback, result)| {
                                    format!("{} -> {result:?}", callback.name)
                                })
                                .collect::<Vec<String>>();

                            error!(
                                "all callback results of filter '{stage:?}': {results_by_name:?}"
                            );
                        }

                        Err((error, shutdown_future))
                    }
                }
            }
            Either::Right((_, join_future)) => {
                join_future.await;

                Ok(Action::Shutdown)
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn process_frame(
        stage: Stage,
        mut frame: Frame,
        callbacks: &mut [Option<Callback>],
        output_sender: &mut Sender<Message>,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
    ) -> Result<Action, Error> {
        if Stage::Error == frame.stage() {
            Self::send_frame(output_sender, frame, shutdown_receiver).await
        } else {
            frame.set_stage(stage).await?;

            match Self::execute_callbacks(callbacks, shutdown_receiver, stage).await {
                Ok(action) => match action {
                    Action::Continue(shutdown_future) => {
                        if Stage::Present != stage {
                            frame.set_stage(Stage::Enqueue).await?;
                        }

                        Self::send_frame(output_sender, frame, shutdown_future).await
                    }
                    Action::Shutdown => Ok(Action::Shutdown),
                },
                Err((_, shutdown_future)) => {
                    frame.set_stage(Stage::Error).await?;

                    Self::send_frame(output_sender, frame, shutdown_future).await
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn send_frame(
        output_sender: &mut Sender<Message>,
        frame: Frame,
        shutdown_receiver: futures::channel::oneshot::Receiver<()>,
    ) -> Result<Action, Error> {
        let send_future = output_sender.send(Message::Frame(frame));

        pin!(send_future);

        match select(send_future, shutdown_receiver).await {
            Either::Left((send_result, shutdown_future)) => {
                send_result.map_or_else(|_| Err(connection_shutdown()), Ok)?;

                Ok(Action::Continue(shutdown_future))
            }
            Either::Right((_, shutdown_future)) => {
                (shutdown_future.await).map_or_else(|_| Err(connection_shutdown()), Ok)?;

                Ok(Action::Shutdown)
            }
        }
    }
}

impl Drop for Filter {
    #[inline]
    fn drop(&mut self) {
        let stage = self.stage;

        let shutdown_sender = self.shutdown_sender.clone();

        let task = self.task.clone();

        let input_sender = take(&mut self.input_sender).unwrap();

        let (sender, receiver) = sync_channel(1);

        let _join_handle = self.runtime.spawn(async move {
            Self::drop_impl(stage, shutdown_sender, task, input_sender).await;

            if sender.send(()).is_err() {
                unreachable!();
            }
        });

        if receiver.recv().is_err() {
            unreachable!();
        }
    }
}

#[cfg(test)]
unsafe impl Send for Filter {}
