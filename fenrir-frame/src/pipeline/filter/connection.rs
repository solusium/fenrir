#[allow(unused_imports)]
use {crate::pipeline::filter::callback::FutureGenerator, fenrir_error::Error::ConnectionShutdown};
use {
    crate::pipeline::{
        filter::connection::stable_abi::{Trait, Trait_TO},
        Message,
    },
    abi_stable::{
        sabi_trait,
        std_types::{RBox, RResult},
        StableAbi,
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    fenrir_error::{connection_shutdown, Error},
    futures::{
        channel::mpsc::{channel, Sender},
        SinkExt, StreamExt,
    },
    sleipnir::runtime::Handle,
    std::{fmt::Debug, mem::take},
};

mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::needless_lifetimes,
        clippy::ptr_as_ptr,
        clippy::unnecessary_cast,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        abi_stable::{sabi_trait, std_types::RResult},
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    pub(crate) trait Trait: Debug + Send + Sync {
        fn disconnect(mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>>;
    }
}

#[repr(C)]
#[derive(StableAbi)]
pub struct Connection {
    state: Trait_TO<'static, RBox<()>>,
}

#[derive(Debug)]
pub(crate) struct Implementation {
    runtime: Handle,
    sender: Option<Sender<Message>>,
    index: usize,
}

impl Connection {
    #[inline]
    pub(crate) fn new(runtime: Handle, sender: Sender<Message>, index: usize) -> Self {
        Self {
            state: Trait_TO::from_value(
                Implementation {
                    runtime,
                    sender: Some(sender),
                    index,
                },
                sabi_trait::TD_Opaque,
            ),
        }
    }

    /// Disconnects this [`Connection`] from the `Filter`, resulting in not calling the
    /// [`FutureGenerator`] anymore.
    ///
    /// A Connection also disconnects when dropped (see `Implementation::drop`).
    ///
    /// # Errors
    ///
    /// This function returns [`ConnectionShutdown`] if the `Filter` connected to
    /// `Implementation::sender` dropped.
    #[inline]
    pub async fn disconnect(self) -> Result<(), Error> {
        self.state.disconnect().await.into()
    }
}

impl Implementation {
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn disconnect(mut self) -> Result<(), Error> {
        match take(&mut self.sender) {
            None => Ok(()),
            Some(mut sender) => {
                let (disconnected_sender, mut disconnected_receiver) = channel(1);

                let send_result = sender
                    .send(Message::Disconnect(self.index, Some(disconnected_sender)))
                    .await;

                match send_result {
                    Err(_) => Err(connection_shutdown()),
                    Ok(()) => match disconnected_receiver.next().await {
                        Some(()) => Ok(()),
                        None => Err(connection_shutdown()),
                    },
                }
            }
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn drop(mut sender: Sender<Message>, index: usize) -> Result<(), Error> {
        (sender.send(Message::Disconnect(index, None)).await)
            .map_or_else(|_| Err(connection_shutdown()), Ok)
    }
}

impl Debug for Connection {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Connection")
            .field("state", &self.state)
            .finish()
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        if let Some(sender) = take(&mut self.sender) {
            let index = self.index;

            let _join_handle = self.runtime.spawn(async move {
                let _ = Self::drop(sender, index).await;
            });
        }
    }
}

impl Trait for Implementation {
    #[inline]
    fn disconnect(self) -> BorrowingFfiFuture<'static, RResult<(), Error>> {
        async move { self.disconnect().await.into() }.into_ffi()
    }
}

unsafe impl Send for Connection {}
unsafe impl Sync for Connection {}
