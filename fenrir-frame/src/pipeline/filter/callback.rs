use {
    super::Callback,
    crate::pipeline::filter::callback::stable_abi::{
        FutureGeneratorTrait, FutureGeneratorTrait_TO,
    },
    abi_stable::{
        sabi_trait,
        std_types::{RBox, RResult},
        StableAbi,
    },
    async_ffi::BorrowingFfiFuture,
    fenrir_error::Error,
    futures::channel::mpsc::Sender,
    std::fmt::{Debug, Formatter},
};

pub(crate) mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::needless_lifetimes,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        abi_stable::{sabi_trait, std_types::RResult},
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    pub trait FutureGeneratorTrait: Debug + Send + Sync {
        fn generate_future(&mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>>;
    }
}
#[derive(Debug)]
pub(crate) struct Adder {
    pub callback: Callback,
    pub sender: Sender<usize>,
}

#[repr(C)]
#[derive(Debug, StableAbi)]
pub struct FutureGenerator {
    inner: FutureGeneratorTrait_TO<'static, RBox<()>>,
}

impl FutureGenerator {
    #[inline]
    pub fn new<FG>(future_generator: FG) -> Self
    where
        FG: FutureGeneratorTrait + 'static,
    {
        Self {
            inner: FutureGeneratorTrait_TO::from_value(future_generator, sabi_trait::TD_Opaque),
        }
    }

    #[inline]
    pub(crate) fn generate_future(&mut self) -> BorrowingFfiFuture<'static, RResult<(), Error>> {
        self.inner.generate_future()
    }
}

impl Debug for Callback {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Callback")
    }
}
