use {
    crate::{
        config::RECOMMEND_MULTI_BUFFERING,
        pipeline::{
            filter::{
                callback::stable_abi::FutureGeneratorTrait, connection::Connection, Filter,
                FutureGenerator,
            },
            Frame, Message, Stage,
        },
    },
    abi_stable::std_types::RResult::{self, ROk},
    async_ffi::BorrowingFfiFuture,
    async_lock::{Barrier, Mutex, RwLock},
    fenrir_error::Error,
    futures::{
        channel::mpsc::{channel, unbounded, Receiver, UnboundedReceiver, UnboundedSender},
        future::join_all,
        stream, FutureExt, StreamExt,
    },
    rand::{thread_rng, Rng},
    sleipnir::runtime::Handle,
    smallvec::SmallVec,
    std::{
        ops::{Bound, Range, RangeBounds},
        sync::Arc,
    },
};

#[derive(Clone, Debug)]
struct WaitForBarrierFutureGenerator {
    barrier: Arc<Barrier>,
}

impl WaitForBarrierFutureGenerator {
    #[inline]
    fn new() -> Self {
        Self {
            barrier: Arc::new(Barrier::new(2)),
        }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn generate_future_impl(barrier: Arc<Barrier>) -> RResult<(), Error> {
        let _ = barrier.wait().await;

        ROk(())
    }
}

impl FutureGeneratorTrait for WaitForBarrierFutureGenerator {
    #[inline]
    fn generate_future(
        &mut self,
    ) -> BorrowingFfiFuture<'static, abi_stable::std_types::RResult<(), Error>> {
        let barrier = self.barrier.clone();

        async_ffi::FutureExt::into_ffi(Self::generate_future_impl(barrier))
    }
}

#[inline]
fn filter_and_receiver() -> (Arc<Mutex<Filter>>, Receiver<Message>) {
    let (output_sender, output_receiver) = channel(1);

    let mut rng = thread_rng();

    let stage = match rng.gen_range(0..4) {
        0 => Stage::User,
        1 => Stage::Prepare,
        2 => Stage::Render,
        _ => Stage::Present,
    };

    let runtime = Handle::current();

    let filter = Arc::new(Mutex::new(Filter::new(runtime, stage, output_sender)));

    (filter, output_receiver)
}

struct FrameData {
    pub receiver: UnboundedReceiver<Stage>,
    pub entity: usize,
    pub index: usize,
}

#[inline]
fn generate_frame_data<R: RangeBounds<usize>>(
    rng_frame_count_range: R,
) -> (Vec<FrameData>, Vec<UnboundedSender<Stage>>) {
    let mut rng = thread_rng();

    let range: Range<usize> = match rng_frame_count_range.start_bound() {
        Bound::Included(start_) => {
            let start = *start_;

            let end = match rng_frame_count_range.end_bound() {
                Bound::Included(end) => end + 1,
                Bound::Excluded(end) => *end,
                Bound::Unbounded => unreachable!(),
            };

            Range { start, end }
        }
        _ => unreachable!(),
    };

    let n = rng.gen_range(range);

    let indices = (0..n).map(|_| rng.gen_range(0..usize::MAX));

    (0..n)
        .zip(indices)
        .map(|(entity, index)| {
            let (sender, receiver) = unbounded();

            let frame_data = FrameData {
                receiver,
                entity,
                index,
            };

            (frame_data, sender)
        })
        .unzip()
}

#[inline]
#[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
async fn generate_and_push_frames<R: RangeBounds<usize> + Send>(
    filter: Arc<Mutex<Filter>>,
    rng_frame_count_range: R,
) -> Vec<FrameData> {
    let (frame_datas, senders) = generate_frame_data(rng_frame_count_range);

    join_all(
        stream::iter(frame_datas)
            .zip(stream::iter(senders))
            .map(|(frame_data, sender)| async {
                let filter_clone = filter.clone();

                let mut filter_ = filter_clone.lock().await;

                let mut frame = Frame::new(frame_data.entity, sender);

                frame.set_index(frame_data.index);

                assert!(filter_.push(frame).await.is_ok());

                drop(filter_);

                frame_data
            })
            .collect::<Vec<_>>()
            .await,
    )
    .await
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn one_iteration() {
    let (filter, mut output_receiver) = filter_and_receiver();

    let mut frame_data_vector = generate_and_push_frames(filter.clone(), 1..2).await;

    let frame_data = frame_data_vector.pop().unwrap();

    let message = output_receiver.next().await;

    assert!(message.is_some());

    match message.unwrap() {
        Message::Frame(frame) => {
            assert_eq!(frame_data.entity, frame.entity());
            assert_eq!(frame_data.index, frame.index());
        }
        _ => {
            unreachable!()
        }
    }
}

#[allow(unused_assignments)]
#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn fifo() {
    let (filter, output_receiver) = filter_and_receiver();

    output_receiver
        .zip(stream::iter(
            async { generate_and_push_frames(filter.clone(), 2..=RECOMMEND_MULTI_BUFFERING).await }
                .await,
        ))
        .for_each(|(message, frame_data)| async move {
            match message {
                Message::Frame(frame) => {
                    let entity = frame_data.entity;
                    let index = frame_data.index;

                    assert_eq!(entity, frame.entity());
                    assert_eq!(index, frame.index());
                }
                _ => {
                    unreachable!()
                }
            }
        })
        .await;
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn connection() {
    let (filter, mut output_receiver) = filter_and_receiver();

    let wait_for_barrier_future_generator = WaitForBarrierFutureGenerator::new();

    let barrier = wait_for_barrier_future_generator.barrier.clone();

    let connection = filter
        .lock()
        .await
        .connect(
            FutureGenerator::new(wait_for_barrier_future_generator),
            "connection",
        )
        .await;

    assert!(connection.is_ok());

    let frame_datas = generate_and_push_frames(filter.clone(), 2..=RECOMMEND_MULTI_BUFFERING).await;

    for mut frame_data in frame_datas {
        let expected_stages = [Stage::Enqueue, filter.lock().await.stage()];

        let receiver = &mut frame_data.receiver;

        for expected_stage in expected_stages {
            let next = receiver.next().await;

            assert!(next.is_some());
            assert_eq!(next.unwrap(), expected_stage);
        }

        assert_eq!(receiver.next().now_or_never(), None);

        let _ = barrier.wait().await;

        if Stage::Present != filter.lock().await.stage() {
            let next = receiver.next().await;

            assert!(next.is_some());
            assert_eq!(next.unwrap(), Stage::Enqueue);
        }

        assert_eq!(receiver.next().now_or_never(), None);

        let message = output_receiver.next().await;

        assert!(message.is_some());

        match message.unwrap() {
            Message::Frame(_) => {}
            _ => {
                unreachable!()
            }
        }
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn disconnect() {
    let (filter, output_receiver) = filter_and_receiver();

    let filter_ref = &filter;

    let output_receiver_arc = Arc::new(RwLock::new(output_receiver));
    let output_receiver_arc_ref = &output_receiver_arc;

    let (frame_datas, senders) = generate_frame_data(2..=RECOMMEND_MULTI_BUFFERING);

    let () = stream::iter(frame_datas)
        .zip(stream::iter(senders))
        .for_each(|(frame_data, sender)| async move {
            let local_frame_data = frame_data;

            let filter = filter_ref.clone();

            {
                let wait_for_barrier_future_generator = WaitForBarrierFutureGenerator::new();

                let connection = filter
                    .lock()
                    .await
                    .connect(
                        FutureGenerator::new(wait_for_barrier_future_generator),
                        "disconnect",
                    )
                    .await;

                assert!(connection.is_ok());

                assert!(connection.unwrap().disconnect().await.is_ok());
            }

            {
                let mut frame = Frame::new(local_frame_data.entity, sender);

                frame.set_index(local_frame_data.index);

                let push_result = filter.lock().await.push(frame).await;

                assert!(
                    push_result.is_ok(),
                    "failed to push frame: {:?}",
                    push_result.unwrap_err()
                );
            }

            let message = output_receiver_arc_ref.clone().write().await.next().await;

            assert!(message.is_some());

            match message.unwrap() {
                Message::Frame(_) => {}
                _ => {
                    unreachable!()
                }
            }
        })
        .await;
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn recommend_multi_buffering_frames_in_flight() {
    let (filter, mut output_receiver) = filter_and_receiver();

    let wait_for_barrier_future_generator = WaitForBarrierFutureGenerator::new();

    let barrier = wait_for_barrier_future_generator.barrier.clone();

    let connection = filter
        .lock()
        .await
        .connect(
            FutureGenerator::new(wait_for_barrier_future_generator),
            "recommend_multi_buffering_frames_in_flight",
        )
        .await;

    assert!(connection.is_ok());

    let mut frame_datas = generate_and_push_frames(
        filter.clone(),
        RECOMMEND_MULTI_BUFFERING..=RECOMMEND_MULTI_BUFFERING,
    )
    .await;

    let mut rng = thread_rng();

    let n = rng.gen_range(3..6);

    for iteration in 2..n {
        for frame_data in &mut frame_datas {
            let expected_stages = [Stage::Enqueue, filter.lock().await.stage()];

            let receiver = &mut frame_data.receiver;

            for expected_stage in expected_stages {
                let next = receiver.next().await;

                assert!(next.is_some());
                assert_eq!(next.unwrap(), expected_stage);
            }

            assert_eq!(receiver.next().now_or_never(), None);

            let _ = barrier.wait().await;

            if Stage::Present != filter.lock().await.stage() {
                let next = receiver.next().await;

                assert!(next.is_some());
                assert_eq!(next.unwrap(), Stage::Enqueue);
            }

            assert_eq!(receiver.next().now_or_never(), None);

            let message = output_receiver.next().await;

            assert!(message.is_some());

            match message.unwrap() {
                Message::Frame(frame) => {
                    if (n - 1) != iteration {
                        assert!(filter.lock().await.push(frame).await.is_ok());
                    }
                }
                _ => {
                    unreachable!()
                }
            }
        }
    }
}

async fn two_chained_filters_and_receiver() -> (SmallVec<[Arc<Mutex<Filter>>; 2]>, Receiver<Message>)
{
    let (second_filter, receiver) = filter_and_receiver();

    let first_filter = Arc::new(Mutex::new(Filter::new(
        Handle::current(),
        Stage::Render,
        second_filter.lock().await.input_sender().clone(),
    )));

    ([first_filter, second_filter].into(), receiver)
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn chained_filters() {
    let (filter, output_receiver) = two_chained_filters_and_receiver().await;

    output_receiver
        .zip(stream::iter(
            async {
                generate_and_push_frames(filter[0].clone(), 2..=RECOMMEND_MULTI_BUFFERING).await
            }
            .await,
        ))
        .for_each(|zip| async move {
            let message = zip.0;

            match message {
                Message::Frame(frame) => {
                    let frame_data = zip.1;

                    assert_eq!(frame_data.entity, frame.entity());
                    assert_eq!(frame_data.index, frame.index());
                }
                _ => {
                    unreachable!()
                }
            }
        })
        .await;
}

async fn connect_wait_for_barrier_future_generators(
    filter: &[Arc<Mutex<Filter>>],
) -> (Vec<Arc<Barrier>>, Vec<Connection>) {
    let wait_for_barrier_future_generators = [
        WaitForBarrierFutureGenerator::new(),
        WaitForBarrierFutureGenerator::new(),
    ];

    let barriers = join_all(
        wait_for_barrier_future_generators
            .into_iter()
            .zip(filter)
            .map(|(wait_for_barrier_future_generator, filter)| async move {
                let barrier = wait_for_barrier_future_generator.barrier.clone();

                let connection_result = filter
                    .lock()
                    .await
                    .connect(
                        FutureGenerator::new(wait_for_barrier_future_generator),
                        "connect_wait_for_barrier_future_generators",
                    )
                    .await;

                connection_result.map_or_else(|_| None, |connection| Some((barrier, connection)))
            }),
    )
    .await;

    assert!(barriers.iter().all(Option::is_some));

    barriers.into_iter().map(Option::unwrap).unzip()
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn chained_filters_connection() {
    let (filter, mut output_receiver) = two_chained_filters_and_receiver().await;

    let (barriers, _connections) = connect_wait_for_barrier_future_generators(&filter).await;

    let frame_datas =
        generate_and_push_frames(filter[0].clone(), 2..=RECOMMEND_MULTI_BUFFERING).await;

    for mut frame_data in frame_datas {
        let stage_receiver = &mut frame_data.receiver;

        for (filter, barrier) in filter.iter().zip(barriers.iter()) {
            let expected_stages = [Stage::Enqueue, filter.lock().await.stage()];

            for expected_stage in expected_stages {
                let next_stage = stage_receiver.next().await;

                assert!(next_stage.is_some());
                assert_eq!(expected_stage, next_stage.unwrap());
            }

            assert_eq!(None, stage_receiver.next().now_or_never());

            let _ = barrier.wait().await;
        }

        if Stage::Present != filter[1].lock().await.stage() {
            let next_stage = stage_receiver.next().await;

            assert!(next_stage.is_some());
            assert_eq!(Stage::Enqueue, next_stage.unwrap());
        }

        assert_eq!(None, stage_receiver.next().now_or_never());

        let message = output_receiver.next().await;

        assert!(message.is_some());

        match message.unwrap() {
            Message::Frame(frame) => {
                assert_eq!(frame_data.entity, frame.entity());
                assert_eq!(frame_data.index, frame.index());
            }
            _ => {
                unreachable!()
            }
        }
    }
}
