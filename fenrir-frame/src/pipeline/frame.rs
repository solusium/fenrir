use {
    crate::pipeline::Stage,
    fenrir_error::{connection_shutdown, Error},
    futures::{channel::mpsc::UnboundedSender, SinkExt},
};

#[derive(Clone, Debug)]
pub(crate) struct Frame {
    sender: UnboundedSender<Stage>,
    stage: Stage,
    entity: usize,
    index: usize,
}

impl Frame {
    #[inline]
    pub(crate) const fn new(entity: usize, sender: UnboundedSender<Stage>) -> Self {
        Self {
            sender,
            stage: Stage::Unknown,
            entity,
            index: 0,
        }
    }

    #[inline]
    pub(crate) const fn entity(&self) -> usize {
        self.entity
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn index(&self) -> usize {
        self.index
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn set_index(&mut self, index: usize) {
        self.index = index;
    }

    #[inline]
    pub(crate) async fn set_stage(&mut self, stage: Stage) -> Result<(), Error> {
        self.stage = stage;

        if self.sender.send(stage).await.is_ok() {
            Ok(())
        } else {
            Err(connection_shutdown())
        }
    }

    #[inline]
    pub(crate) const fn stage(&self) -> Stage {
        self.stage
    }
}
