use std::fmt::{Display, Formatter};

#[derive(Clone, Debug)]
pub enum Flags {
    Graphics,
    Compute,
    Transfer,
    SparseBinding,
    ProtectedMemory,
    VideoDecode,
    VideoEncode,
    OpticalFlow,
    Unknown,
}

impl Display for Flags {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let flag = match self {
            Self::Graphics => "graphics",
            Self::Compute => "compute",
            Self::Transfer => "transfer",
            Self::SparseBinding => "sparse binding",
            Self::ProtectedMemory => "protected memory",
            Self::VideoDecode => "video decode",
            Self::VideoEncode => "video encode",
            Self::OpticalFlow => "optical flow",
            Self::Unknown => "unknown",
        };

        writeln!(f, "{flag}")
    }
}
