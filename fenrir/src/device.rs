use {
    crate::{
        extension::Extension,
        queue::{family::Flags as QueueFamilyFlags, Family as QueueFamily},
        texture::{Builder as TextureBuilder, Metadata as TextureMetadata},
        version::Version,
        Error,
    },
    bytemuck::{Pod, Zeroable},
    core::{
        fmt::{Display, Formatter},
        marker::PhantomData,
        slice::from_raw_parts,
    },
    fenrir_frame::texture::manager::Manager as TextureManager,
    fenrir_hal::{
        device::{Device as HalDevice, Type as HalDeviceType},
        queue::family::Flags as HalQueueFamilyFlags,
    },
    smallvec::SmallVec,
};

#[derive(Clone, Copy)]
pub enum Type {
    Other,
    IntegratedGPU,
    DiscreteGPU,
    VirtualGPU,
    CPU,
}

#[derive(Debug)]
pub struct Device<'a> {
    backend: HalDevice,
    texture_manager: TextureManager,
    _phantom_data: PhantomData<&'a ()>,
}

impl Device<'_> {
    #[inline]
    pub(crate) const fn new(backend: HalDevice, texture_manager: TextureManager) -> Self {
        Device {
            backend,
            texture_manager,
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    #[must_use]
    pub fn build_texture(&self, metadata: &TextureMetadata) -> TextureBuilder {
        TextureBuilder::new(self, metadata, self.texture_manager.clone())
    }

    #[must_use]
    pub fn device_type(&self) -> Type {
        let server_type = self.backend.device_type();

        match server_type {
            HalDeviceType::Other => Type::Other,
            HalDeviceType::IntegratedGPU => Type::IntegratedGPU,
            HalDeviceType::DiscreteGPU => Type::DiscreteGPU,
            HalDeviceType::VirtualGPU => Type::VirtualGPU,
            HalDeviceType::CPU => Type::CPU,
        }
    }

    /// Get the driver version of this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn driver_version(&self) -> Result<Version, Error> {
        Ok(Version::new(self.backend.driver_version()?))
    }

    /// Get the extension of the underlying API supported by this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn extensions(&self) -> Result<Vec<Extension>, Error> {
        match self.backend.extensions() {
            Err(error) => Err(error),
            Ok(extensions) => Ok(extensions.into_iter().map(Into::into).collect()),
        }
    }

    #[inline]
    #[must_use]
    pub fn name(&self) -> &str {
        self.backend.name()
    }

    #[must_use]
    pub fn queue_families(&self) -> SmallVec<[QueueFamily; 4]> {
        self.backend
            .queue_families()
            .into_iter()
            .map(|queue_familiy| {
                let (array, size) = queue_familiy.flags.into();

                let flags = unsafe { from_raw_parts(array.as_ptr(), size) };

                QueueFamily::new(
                    queue_familiy.index,
                    queue_familiy.length,
                    &flags
                        .iter()
                        .map(|flag| match flag {
                            HalQueueFamilyFlags::Graphics => QueueFamilyFlags::Graphics,
                            HalQueueFamilyFlags::Compute => QueueFamilyFlags::Compute,
                            HalQueueFamilyFlags::Transfer => QueueFamilyFlags::Transfer,
                            HalQueueFamilyFlags::SparseBinding => QueueFamilyFlags::SparseBinding,
                            HalQueueFamilyFlags::ProtectedMemory => {
                                QueueFamilyFlags::ProtectedMemory
                            }
                            HalQueueFamilyFlags::VideoDecode => QueueFamilyFlags::VideoDecode,
                            HalQueueFamilyFlags::VideoEncode => QueueFamilyFlags::VideoEncode,
                            HalQueueFamilyFlags::OpticalFlow => QueueFamilyFlags::OpticalFlow,
                            HalQueueFamilyFlags::Unknown => QueueFamilyFlags::Unknown,
                        })
                        .collect::<SmallVec<[QueueFamilyFlags; 8]>>(),
                )
            })
            .collect()
    }

    #[must_use]
    pub(crate) fn backend(&self) -> fenrir_hal::device::Device {
        self.backend.clone()
    }
}

impl Display for Type {
    #[inline]
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Other => write!(f, "other"),
            Self::IntegratedGPU => write!(f, "integrated gpu"),
            Self::DiscreteGPU => write!(f, "discrete gpu"),
            Self::VirtualGPU => write!(f, "virtual gpu"),
            Self::CPU => write!(f, "cpu"),
        }
    }
}

unsafe impl Pod for Type {}

unsafe impl Zeroable for Type {
    fn zeroed() -> Self {
        Self::Other
    }
}
