use {
    crate::version::Version,
    core::fmt::{Display, Formatter},
};

pub struct Extension {
    name: String,
    version: Version,
}

impl Extension {
    #[must_use]
    pub fn name(&self) -> &str {
        &self.name
    }

    #[must_use]
    pub const fn version(&self) -> Version {
        self.version
    }
}

impl Display for Extension {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        let name = self.name();
        let version = self.version();

        writeln!(f, "{name} ({version})")
    }
}

impl From<fenrir_hal::Extension> for Extension {
    fn from(extension: fenrir_hal::Extension) -> Self {
        Self {
            name: extension.name.into(),
            version: Version::new(extension.version),
        }
    }
}
