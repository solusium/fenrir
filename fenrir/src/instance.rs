use {
    crate::{
        allocator::Backend as AllocatorBackend, device::Device, frame::Frame,
        render_graph::RenderGraph, shader::compiler::Backend as ShaderCompilerBackend, Error,
    },
    abi_stable::std_types::RResult::{RErr, ROk},
    core::mem::take,
    fenrir_core::Core,
    fenrir_hal::{
        allocator::Backend as HalAllocatorBackend, instance::Metadata,
        shader::compiler::Backend as HalShaderCompilerBackend,
    },
    std::path::PathBuf,
    tracing::Level,
};

pub enum Backend {
    Ash,
}

pub struct Builder {
    backend: crate::instance::Backend,
    search_paths: Vec<PathBuf>,
    metadata: Metadata,
    allocator_backend: AllocatorBackend,
    shader_compiler_backend: Option<ShaderCompilerBackend>,
}

pub struct Instance {
    core: Core,
}

impl Builder {
    #[inline]
    pub(crate) fn new() -> Self {
        Self {
            backend: Backend::Ash,
            search_paths: Vec::new(),
            metadata: Metadata::default(),
            allocator_backend: AllocatorBackend::GPUAllocator,
            shader_compiler_backend: None,
        }
    }

    pub async fn build(&mut self) -> Result<Instance, Error> {
        let backend = match self.backend {
            Backend::Ash => "ash",
        };

        let metadata = take(&mut self.metadata);

        let allocator_backend = match self.allocator_backend {
            AllocatorBackend::GPUAllocator => HalAllocatorBackend::GPUAllocator,
        };

        let maybe_shader_compiler_backend = self.shader_compiler_backend.map(
            |shader_compiler_backend| match shader_compiler_backend {
                ShaderCompilerBackend::Naga => HalShaderCompilerBackend::Naga,
            },
        );

        let server = Core::new(
            &mut self.search_paths,
            backend.to_string(),
            metadata,
            allocator_backend,
            maybe_shader_compiler_backend,
        )
        .await?;

        Ok(Instance::new(server))
    }

    #[inline]
    pub fn name(&mut self, name: &str) -> &mut Self {
        self.metadata.name = String::from(name).into();

        self
    }

    #[inline]
    pub fn backend_tracing_level(&mut self, level: Level) -> &mut Self {
        self.metadata.tracing_level = match level {
            Level::ERROR => 1,
            Level::WARN => 2,
            Level::INFO => 3,
            Level::DEBUG => 4,
            Level::TRACE => 5,
        };

        self
    }

    #[inline]
    pub fn major(&mut self, major: usize) -> &mut Self {
        self.metadata.major = major;

        self
    }

    #[inline]
    pub fn minor(&mut self, minor: usize) -> &mut Self {
        self.metadata.minor = minor;

        self
    }

    #[inline]
    pub fn patch(&mut self, patch: usize) -> &mut Self {
        self.metadata.patch = patch;

        self
    }

    #[inline]
    pub fn backend(&mut self, backend: Backend) -> &mut Self {
        self.backend = backend;

        self
    }
}

impl Instance {
    #[inline]
    pub(crate) const fn new(core: Core) -> Self {
        Self { core }
    }

    #[inline]
    #[must_use]
    pub fn builder() -> Builder {
        Builder::new()
    }

    #[inline]
    pub fn devices(&self) -> Result<Vec<Device>, Error> {
        match self.core.devices() {
            Err(error) => Err(error),
            Ok(mut backend_devices) => {
                backend_devices.reverse();

                let mut devices = Vec::with_capacity(backend_devices.len());

                while let Some(backend_device) = backend_devices.pop() {
                    devices.push(Device::new(
                        backend_device,
                        self.core.texture_manager.clone(),
                    ));
                }

                Ok(devices)
            }
        }
    }

    #[inline]
    pub async fn render(&self) -> Result<Frame, Error> {
        match self.core.pipeline.push().await {
            ROk(implementation) => Ok(Frame::new(implementation)),
            RErr(error) => Err(error),
        }
    }

    #[inline]
    #[must_use]
    pub fn render_graph(&self) -> RenderGraph {
        RenderGraph::new(self.core.render_graph_manager())
    }
}

unsafe impl Send for Instance {}
unsafe impl Sync for Instance {}
