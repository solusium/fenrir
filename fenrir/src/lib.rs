#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate
)]

pub mod allocator;
pub mod color;
pub mod config;
pub mod device;
pub mod extension;
pub mod frame;
pub mod geometry;
pub mod graphics;
pub mod instance;
pub mod pipeline;
pub mod queue;
pub mod render_graph;
pub mod shader;
pub mod texture;
pub mod version;

pub use fenrir_error::Error;

pub const RECOMMEND_MULTI_BUFFERING: usize = fenrir_frame::config::RECOMMEND_MULTI_BUFFERING;
