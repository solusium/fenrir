use strum::{EnumCount, EnumIter};

#[derive(Clone, Copy, EnumCount, EnumIter)]
pub enum Backend {
    Naga,
}
