use core::fmt::{Debug, Display, Formatter};

#[repr(C)]
#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Version {
    implementation: fenrir_hal::version::Version,
}

impl Version {
    #[inline]
    pub(crate) const fn new(implementation: fenrir_hal::version::Version) -> Self {
        Self { implementation }
    }

    #[inline]
    #[must_use]
    pub const fn major(&self) -> usize {
        self.implementation.major()
    }

    #[inline]
    #[must_use]
    pub const fn minor(&self) -> usize {
        self.implementation.minor()
    }

    #[inline]
    #[must_use]
    pub const fn patch(&self) -> usize {
        self.implementation.patch()
    }
}

impl Debug for Version {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let implementation = self.implementation;

        writeln!(f, "{implementation:?}")
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let (major, minor, patch) = (self.major(), self.minor(), self.patch());

        write!(f, "{major}.{minor}.{patch}")
    }
}
