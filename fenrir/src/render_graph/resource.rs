pub use fenrir_render_graph::resource::{ClearValue, Operation};
use {
    crate::{texture::Texture, Error},
    core::{marker::PhantomData, mem::take},
    fenrir_error::invalid_argument,
    fenrir_frame::render_graph::{resource::state::State, Manager},
    fenrir_render_graph::resource::Metadata,
};

#[derive(Debug)]
pub enum Data<'a> {
    Texture(&'a Texture<'a>),
    None(None),
}

pub struct Builder<'a> {
    manager: Manager,
    data: Data<'a>,
    begin_rendering_operation: Option<Operation>,
    end_rendering_operation: Option<Operation>,
    clear_value: Option<ClearValue>,
}

#[derive(Debug)]
pub struct None {
    _phantom_data: PhantomData<()>,
}

#[allow(dead_code)]
pub struct Resource<'a> {
    state: State,
    _phantom_data: PhantomData<&'a ()>,
}

impl<'a> Builder<'a> {
    #[inline]
    #[must_use]
    pub(crate) const fn new(manager: Manager, data: Data<'a>) -> Self {
        let begin_rendering_operation = None;

        let end_rendering_operation = None;

        let clear_value = None;

        Self {
            manager,
            data,
            begin_rendering_operation,
            end_rendering_operation,
            clear_value,
        }
    }

    /// # Errors
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if the variant of
    /// [`Data`] doesn't support a operation at the beginning of rendering.
    #[inline]
    pub fn begin_rendering_operation(mut self, operation: Operation) -> Result<Self, Error> {
        if let Data::Texture(_) = self.data {
            self.begin_rendering_operation = Some(operation);

            Ok(self)
        } else {
            Err(invalid_argument(&format!(
                "an operation at the beginning of rendering might be only needed by resources of \
                type fenrir::render_graph::resource::Data::Texture, found: {:?}",
                self.data,
            )))
        }
    }

    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if the runtime
    /// shutdown, probably due to an error, before we could construct the resource.
    #[inline]
    pub async fn build(mut self) -> Result<Resource<'a>, Error> {
        let metadata = Metadata::new(
            take(&mut self.begin_rendering_operation),
            take(&mut self.end_rendering_operation),
            take(&mut self.clear_value),
        );

        let data = match self.data {
            Data::None(_) => unreachable!(),
            Data::Texture(texture) => {
                fenrir_frame::render_graph::resource::data::State::Texture(texture.state())
            }
        };

        Ok(Resource::new(
            self.manager.new_resource(metadata, data).await?,
        ))
    }

    /// # Errors
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if the variant of
    /// [`Data`] doesn't support a clear value.
    #[inline]
    pub fn clear_value(mut self, clear_value: ClearValue) -> Result<Self, Error> {
        if let Data::Texture(_) = self.data {
            self.clear_value = Some(clear_value);

            Ok(self)
        } else {
            Err(invalid_argument(&format!(
                "a clear value might only needed by resources of \
                fenrir::render_graph::resource::Data::Texture, found: {:?}",
                self.data,
            )))
        }
    }

    /// # Errors
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if the variant of
    /// [`Data`] doesn't support a operation at the end of rendering.
    #[inline]
    pub fn end_rendering_operation(mut self, operation: Operation) -> Result<Self, Error> {
        if let Data::Texture(_) = self.data {
            self.end_rendering_operation = Some(operation);

            Ok(self)
        } else {
            Err(invalid_argument(&format!(
                "an operation at the end of rendering might only needed by resources of \
                fenrir::render_graph::resource::Data::Texture, found: {:?}",
                self.data,
            )))
        }
    }
}

impl Resource<'_> {
    #[inline]
    pub(crate) const fn new(state: State) -> Self {
        Self {
            state,
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    pub(crate) fn state(&self) -> State {
        self.state.clone()
    }
}

impl Default for Data<'_> {
    #[inline]
    fn default() -> Self {
        Self::None(None {
            _phantom_data: PhantomData,
        })
    }
}
