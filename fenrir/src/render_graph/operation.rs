use {
    crate::{
        geometry::Rect2D, graphics::pipeline::Builder as GraphicsPipelineBuilder,
        render_graph::resource::Resource, Error,
    },
    core::marker::PhantomData,
    fenrir_frame::render_graph::{operation::state::State, Manager},
    fenrir_hal::{device::Device, pipeline::BindPoint, render_pass::Metadata},
};

pub enum Type {
    Graphics(Rect2D),
    Compute,
}

pub struct Builder<'a> {
    manager: Manager,
    device: Device,
    operation_type: Type,
    layer_count: usize,
    view_mask: usize,
    _phantom_data: PhantomData<&'a ()>,
}

pub struct Operation<'a> {
    state: State,
    _phantom_data: PhantomData<&'a ()>,
}

impl<'a> Builder<'a> {
    #[inline]
    pub(crate) const fn new(manager: Manager, device: Device, operation_type: Type) -> Self {
        Self {
            manager,
            device,
            operation_type,
            layer_count: 1,
            view_mask: 0,
            _phantom_data: PhantomData,
        }
    }

    /// Construct a new [`Operation`].
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return an error if the API wrapped by the operation couldn't
    /// construct the wrapped structs.
    #[inline]
    pub async fn build(self) -> Result<Operation<'a>, Error> {
        match self.operation_type {
            Type::Compute => todo!(),
            Type::Graphics(render_area) => {
                let metadata = Metadata::new(
                    self.device.clone(),
                    Vec::new(),
                    render_area.into(),
                    self.layer_count,
                    self.view_mask,
                    BindPoint::Graphics,
                );

                let state = self.manager.new_operation(metadata).await?;

                Ok(Operation {
                    state,
                    _phantom_data: PhantomData,
                })
            }
        }
    }
}

impl<'a> Operation<'a> {
    #[inline]
    #[must_use]
    pub fn graphics_pipeline(&self) -> GraphicsPipelineBuilder {
        GraphicsPipelineBuilder::new(self.state.clone())
    }

    /// Get the [`Metadata`] of the underlying render pass.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function might also return [`fenrir_error::Error::InvalidArgument`] if operation doesn't
    /// represent a valid operation.
    #[inline]
    pub async fn render_pass_metadata(&self) -> Result<Metadata, Error> {
        self.state.metadata().await
    }

    /// Let this operation write to resource.
    ///
    /// # Errors
    ///
    /// This function will return [`fenrir_error::Error::ConnectionShutdown`] if some internal
    /// channel closed.
    /// This function will return [`fenrir_error::Error::InvalidArgument`] if resource or operation
    /// doesn't represent a valid resource or operation state, respectively.
    /// This function constructs new [`Operations`](fenrir_render_graph::operation::Operation) and
    /// might return an error if the API wrapped by the operation couldn't construct the wrapped
    /// structs.
    /// This function might also return [`fenrir_error::Error::StateNotRecoverable`] if some
    /// internal data got dropped or another operation already writes to resource.
    #[inline]
    pub async fn write<'b>(&self, resource: &Resource<'b>) -> Result<(), Error> {
        self.state.write(resource.state()).await
    }
}
