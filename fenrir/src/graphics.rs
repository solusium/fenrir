pub mod pipeline;

pub type Pipeline<'a> = crate::graphics::pipeline::Pipeline<'a>;
