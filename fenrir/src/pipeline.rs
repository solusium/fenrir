#[derive(Debug)]
pub enum BindPoint {
    Graphics,
    Compute,
    RayTracing,
}

#[allow(clippy::from_over_into)]
impl Into<fenrir_hal::pipeline::BindPoint> for BindPoint {
    #[inline]
    fn into(self) -> fenrir_hal::pipeline::BindPoint {
        match self {
            Self::Graphics => fenrir_hal::pipeline::BindPoint::Graphics,
            Self::Compute => fenrir_hal::pipeline::BindPoint::Compute,
            Self::RayTracing => fenrir_hal::pipeline::BindPoint::RayTracing,
        }
    }
}
