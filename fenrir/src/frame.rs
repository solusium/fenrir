use {
    crate::Error,
    abi_stable::StableAbi,
    core::{
        pin::Pin,
        task::{Context, Poll},
    },
    futures::Future,
    sleipnir::pin,
};

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Stage {
    Enqueue,
    User,
    Prepare,
    Render,
    Present,
    Error,
    Unknown,
}

pub struct Frame {
    implementation: fenrir_frame::Frame,
}

impl Frame {
    #[inline]
    pub(crate) const fn new(implementation: fenrir_frame::Frame) -> Self {
        Self { implementation }
    }

    pub async fn stage(&mut self) -> Result<Stage, Error> {
        match self.implementation.current_stage().await {
            Err(error) => Err(error),
            Ok(stage) => Ok(match stage {
                fenrir_frame::pipeline::Stage::Enqueue => Stage::Enqueue,
                fenrir_frame::pipeline::Stage::User => Stage::User,
                fenrir_frame::pipeline::Stage::Prepare => Stage::Prepare,
                fenrir_frame::pipeline::Stage::Render => Stage::Render,
                fenrir_frame::pipeline::Stage::Present => Stage::Present,
                fenrir_frame::pipeline::Stage::Error => Stage::Error,
                fenrir_frame::pipeline::Stage::Unknown => Stage::Unknown,
            }),
        }
    }
}

impl Future for Frame {
    type Output = Result<Stage, Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut_self = self.get_mut();

        let implementation = &mut mut_self.implementation;

        pin!(implementation);

        match implementation.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(stage_result) => match stage_result {
                Err(error) => Poll::Ready(Err(error)),
                Ok(stage) => Poll::Ready(Ok(match stage {
                    fenrir_frame::pipeline::Stage::Enqueue => Stage::Enqueue,
                    fenrir_frame::pipeline::Stage::User => Stage::User,
                    fenrir_frame::pipeline::Stage::Prepare => Stage::Prepare,
                    fenrir_frame::pipeline::Stage::Render => Stage::Render,
                    fenrir_frame::pipeline::Stage::Present => Stage::Present,
                    fenrir_frame::pipeline::Stage::Error => Stage::Error,
                    fenrir_frame::pipeline::Stage::Unknown => Stage::Unknown,
                })),
            },
        }
    }
}
