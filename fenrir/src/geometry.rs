#[derive(Clone, Copy, Debug)]
pub struct Extent2D {
    width: usize,
    height: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct Point2D {
    x: isize,
    y: isize,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Rect2D {
    offset: Point2D,
    extent: Extent2D,
}

impl Extent2D {
    #[inline]
    #[must_use]
    pub const fn new(width: usize, height: usize) -> Self {
        Self { width, height }
    }

    #[inline]
    #[must_use]
    pub const fn height(&self) -> usize {
        self.height
    }

    #[inline]
    #[must_use]
    pub const fn width(&self) -> usize {
        self.width
    }
}

impl Point2D {
    #[inline]
    #[must_use]
    pub const fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    #[inline]
    #[must_use]
    pub const fn x(&self) -> isize {
        self.x
    }

    #[inline]
    #[must_use]
    pub const fn y(&self) -> isize {
        self.y
    }
}

impl Rect2D {
    #[inline]
    #[must_use]
    pub const fn new(offset: Point2D, extent: Extent2D) -> Self {
        Self { offset, extent }
    }

    #[inline]
    #[must_use]
    pub const fn offset(&self) -> Point2D {
        self.offset
    }

    #[inline]
    #[must_use]
    pub const fn extent(&self) -> Extent2D {
        self.extent
    }
}

#[allow(clippy::from_over_into)]
impl Into<fenrir_hal::geometry::Extent2D> for Extent2D {
    fn into(self) -> fenrir_hal::geometry::Extent2D {
        fenrir_hal::geometry::Extent2D::new(self.width, self.height)
    }
}

#[allow(clippy::from_over_into)]
impl Into<fenrir_hal::geometry::Point2D> for Point2D {
    fn into(self) -> fenrir_hal::geometry::Point2D {
        fenrir_hal::geometry::Point2D::new(self.x, self.y)
    }
}

#[allow(clippy::from_over_into)]
impl Into<fenrir_hal::geometry::Rect2D> for Rect2D {
    fn into(self) -> fenrir_hal::geometry::Rect2D {
        fenrir_hal::geometry::Rect2D::new(self.offset.into(), self.extent.into())
    }
}
