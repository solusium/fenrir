use {
    crate::shader::Module,
    core::marker::PhantomData,
    fenrir_frame::{graphics::pipeline::State, render_graph::operation::State as OperationState},
    fenrir_hal::graphics::pipeline::Metadata,
    fenrir_shader_compiler::Error as ShaderCompilerError,
    smallvec::SmallVec,
};

pub struct Builder<'a, 'b, 'c> {
    operation: OperationState,
    shaders: SmallVec<[Module<'b, 'c>; 6]>,
    _phantom_data: PhantomData<&'a ()>,
}

pub struct Pipeline<'a> {
    state: State,
    _phantom_data: PhantomData<&'a ()>,
}

impl<'a, 'b, 'c> Builder<'a, 'b, 'c> {
    #[inline]
    pub(crate) fn new(operation: OperationState) -> Self {
        Self {
            operation,
            shaders: SmallVec::new(),
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    pub async fn build(self) -> Result<Pipeline<'a>, ShaderCompilerError> {
        let device = match self.operation.metadata().await {
            Ok(metadata) => Ok(metadata),
            Err(error) => Err(ShaderCompilerError::FenrirError(error)),
        }?
        .device();

        let mut metadata = Metadata::builder(device);

        for shader in self.shaders {
            metadata = metadata.add_shader(shader.r#static())
        }

        let state = self
            .operation
            .manager()
            .new_graphics_pipeline(self.operation, metadata.build())
            .await?;

        Ok(Pipeline {
            state,
            _phantom_data: PhantomData,
        })
    }

    #[inline]
    #[must_use]
    pub fn shaders(mut self, modules: impl IntoIterator<Item = Module<'b, 'c>>) -> Self {
        self.shaders = modules.into_iter().collect();

        self
    }
}
