pub mod operation;
pub mod resource;

use {
    crate::{
        device::Device,
        render_graph::{self, operation::Type, resource::Data},
    },
    fenrir_frame::render_graph::Manager,
    std::marker::PhantomData,
};

pub type Operation<'a> = crate::render_graph::operation::Operation<'a>;
pub type Resource<'a> = crate::render_graph::resource::Resource<'a>;

pub struct RenderGraph<'a> {
    manager: Manager,
    _phantom_data: PhantomData<&'a ()>,
}

impl RenderGraph<'_> {
    #[inline]
    pub(crate) const fn new(manager: Manager) -> Self {
        Self {
            manager,
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    #[must_use]
    pub fn operation<'b>(
        &'b self,
        device: &Device<'b>,
        operation_type: Type,
    ) -> render_graph::operation::Builder<'b> {
        render_graph::operation::Builder::new(
            self.manager.clone(),
            device.backend(),
            operation_type,
        )
    }

    #[inline]
    #[must_use]
    pub fn resource<'b>(&'b self, data: Data<'b>) -> render_graph::resource::Builder<'b> {
        render_graph::resource::Builder::new(self.manager.clone(), data)
    }
}
