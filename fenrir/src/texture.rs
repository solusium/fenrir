use {
    crate::{device::Device, Error},
    core::marker::PhantomData,
    fenrir_frame::texture::{state::State, Manager as TextureManager},
    fenrir_hal::texture::Metadata as HalTextureMetadata,
};

pub type Usage = fenrir_hal::texture::Usage;

pub struct Builder<'a> {
    metadata: HalTextureMetadata,
    texture_manager: TextureManager,
    _phantom_data: PhantomData<&'a ()>,
}

pub struct Metadata {
    pub height: usize,
    pub width: usize,
}

#[derive(Debug)]
pub struct Texture<'a> {
    state: State,
    _phantom_data: PhantomData<&'a ()>,
}

impl<'a> Builder<'a> {
    #[inline]
    pub(crate) fn new(
        device: &Device<'a>,
        metadata: &Metadata,
        texture_manager: TextureManager,
    ) -> Self {
        let depth = 1;

        let metadata = HalTextureMetadata::new(
            device.backend(),
            metadata.width,
            metadata.height,
            depth,
            Vec::new(),
            String::new(),
        );

        Builder {
            metadata,
            texture_manager,
            _phantom_data: PhantomData,
        }
    }

    /// Try to build a [`Texture`] from the parameters provided to this [`Builder`].
    ///
    /// # Errors
    ///
    /// This function forwards errors thrown by the underlying graphics API for constructing a new
    /// texture.
    /// If an internal channel shuts down, expected because of a panic, this function returns
    /// [`ConnectionShutdown`](fenrir_error::Error::ConnectionShutdown).
    #[inline]
    pub async fn build(self) -> Result<Texture<'a>, Error> {
        Ok(Texture::new(
            self.texture_manager
                .new_texture(self.metadata.clone())
                .await?,
        ))
    }

    #[inline]
    pub fn depth(&mut self, depth: usize) -> &mut Self {
        self.metadata.set_depth(depth);

        self
    }

    #[inline]
    pub fn height(&mut self, height: usize) -> &mut Self {
        self.metadata.set_height(height);

        self
    }

    #[inline]
    pub fn name(&mut self, name: String) -> &mut Self {
        self.metadata.set_name(name);

        self
    }

    #[inline]
    pub fn usages(&mut self, usages: impl IntoIterator<Item = Usage>) -> &mut Self {
        self.metadata.set_usage(usages);

        self
    }

    #[inline]
    pub fn width(&mut self, width: usize) -> &mut Self {
        self.metadata.set_width(width);

        self
    }
}

impl Texture<'_> {
    #[inline]
    #[must_use]
    pub(crate) const fn new(state: State) -> Self {
        Texture {
            state,
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    pub(crate) fn state(&self) -> State {
        self.state.clone()
    }
}
