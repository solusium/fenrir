pub mod compiler;

pub type Module<'a, 'b> = fenrir_hal::shader::Module<'a, 'b>;

pub type Language = fenrir_shader_compiler::Language;

pub type Stage = fenrir_hal::shader::Stage;
