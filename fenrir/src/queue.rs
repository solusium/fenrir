pub mod family;

use {smallvec::SmallVec, std::marker::PhantomData};

pub struct Family<'a> {
    index: usize,
    length: usize,
    flags: SmallVec<[family::Flags; 8]>,
    _phantom_data: PhantomData<&'a ()>,
}

impl Family<'_> {
    pub(crate) fn new(index: usize, length: usize, flags: &[family::Flags]) -> Self {
        Family {
            index,
            length,
            flags: flags.into(),
            _phantom_data: PhantomData,
        }
    }

    #[must_use]
    pub fn flags(&self) -> &[family::Flags] {
        &self.flags
    }

    #[must_use]
    pub const fn index(&self) -> usize {
        self.index
    }

    #[must_use]
    pub const fn is_empty(&self) -> bool {
        0 == self.length
    }

    #[must_use]
    pub const fn len(&self) -> usize {
        self.length
    }
}
