use {
    arr_macro::arr,
    fenrir::{
        color::RGBA,
        frame::{Frame, Stage},
        geometry::{Extent2D, Point2D, Rect2D},
        instance::{Backend::Ash, Instance},
        render_graph::{
            operation::Type,
            resource::{ClearValue, Data, Operation},
        },
        shader::{Language, Module, Stage as ShaderStage},
        texture::Metadata,
        Error, RECOMMEND_MULTI_BUFFERING,
    },
    futures::{
        channel::oneshot::{channel, Receiver},
        future::{select, Either},
        stream, StreamExt,
    },
    sleipnir::runtime::Handle,
    std::{mem::take, sync::Arc},
    tracing_subscriber::{filter::LevelFilter, fmt::format::FmtSpan},
};

static SHADER_CODES: [&str; 1] = ["#version 450\n\
     \n\
     vec2 positions[3] = vec2[](\n\
       vec2(0.0, -0.5),\n\
       vec2(0.5, 0.5),\n\
       vec2(-0.5, 0.5)\n\
    );\n\
    \n\
    void main() {\n\
      gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
    }"];

async fn render_loop(instance: Arc<Instance>, mut shutdown_receiver: Receiver<()>) {
    enum AwaitResult {
        Frame(Frame),
        ShutdownReceiver(Receiver<()>),
    }

    let check_frame_stage_result = |frame_stage_result: Result<Stage, Error>| {
        assert!(frame_stage_result.is_ok());

        assert_eq!(
            Stage::Present,
            frame_stage_result.expect("failed to get the frame stage")
        );
    };

    let await_futures = |frame: Frame, shutdown_future: Receiver<()>| async move {
        match select(frame, shutdown_future).await {
            Either::Left((frame_stage_result, shutdown_receiver)) => {
                check_frame_stage_result(frame_stage_result);

                AwaitResult::ShutdownReceiver(shutdown_receiver)
            }
            Either::Right((_, frame)) => AwaitResult::Frame(frame),
        }
    };

    // Start the rendering process for the first frames.
    assert_eq!(5, RECOMMEND_MULTI_BUFFERING);
    let mut frames =
        arr![Some(instance.render().await.expect("failed to start rendering a frame")); 5];

    'outer: loop {
        for frame_option in frames.iter_mut() {
            let frame = take(frame_option).expect("failed to retrieve the frame");

            // Await the oldest frame finished rendering or the shutdown signal.
            match await_futures(frame, shutdown_receiver).await {
                AwaitResult::ShutdownReceiver(shutdown_receiver_) => {
                    // If we rendered a frame with success, enqueue the next frame
                    // and start the next loop-iteration.
                    *frame_option = Some(
                        instance
                            .render()
                            .await
                            .expect("failed to start rendering a frame"),
                    );

                    shutdown_receiver = shutdown_receiver_;
                }
                AwaitResult::Frame(frame) => {
                    // If we received the shutdown signal. Leave the loop and
                    // finish the task.

                    check_frame_stage_result(frame.await);

                    stream::iter(
                        frames
                            .iter_mut()
                            .filter(|maybe_frames| maybe_frames.is_some()),
                    )
                    .for_each(|frame_option| async move {
                        let frame = take(frame_option).expect("failed to retrieve the frame");

                        check_frame_stage_result(frame.await);
                    })
                    .await;

                    break 'outer;
                }
            }
        }
    }
}

#[sleipnir::main]
async fn main(handle: Handle) {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_max_level(LevelFilter::TRACE)
        .init();

    let instance = Arc::new(
        Instance::builder()
            .backend(Ash)
            .backend_tracing_level(tracing::Level::TRACE)
            .build()
            .await
            .expect("failed to construct an instance"),
    );

    let (shutdown_sender, shutdown_receiver) = channel();

    let render_task = handle.spawn(render_loop(instance.clone(), shutdown_receiver));

    let device = instance
        .devices()
        .expect("failed to get the devices")
        .pop()
        .expect("vector of devices is empty");

    let height = 128;
    let width = 128;

    let texture = device
        .build_texture(&Metadata { height, width })
        .build()
        .await
        .expect("failed to construct a texture");

    let render_graph = instance.render_graph();

    let render_area = Rect2D::new(Point2D::new(0, 0), Extent2D::new(width, height));

    let operation = render_graph
        .operation(&device, Type::Graphics(render_area))
        .build()
        .await
        .expect("failed to construct a render graph operation");

    let shader_modules = SHADER_CODES
        .iter()
        .zip([ShaderStage::Vertex, ShaderStage::Fragment])
        .map(|(shader_code, shader_stage)| {
            Module::new(shader_stage, shader_code.as_bytes(), Language::GLSL, "main")
        })
        .collect::<Vec<_>>();

    let _pipeline = operation
        .graphics_pipeline()
        .shaders(shader_modules)
        .build()
        .await
        .expect("failed to construct a graphics pipeline");

    let _resource = render_graph
        .resource(Data::Texture(&texture))
        .begin_rendering_operation(Operation::Clear)
        .expect("failed to set the begin rendering operation")
        .clear_value(ClearValue::FColor(RGBA::new(0f32, 0f32, 0f32, 1f32)))
        .expect("failed to set the clear value")
        .build()
        .await
        .expect("failed to construct a render graph resource from a texture");

    shutdown_sender
        .send(())
        .expect("failed to send the shutdown signal to the render task");

    render_task.await.expect("failed to join the render task");
}
