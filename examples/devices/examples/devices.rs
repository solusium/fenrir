use {
    fenrir::instance::{Backend::Ash, Instance},
    sleipnir::runtime::Handle,
};

#[sleipnir::main]
async fn main(_: Handle) {
    let instance = Instance::builder()
        .backend(Ash)
        .build()
        .await
        .expect("failed to construct an instance");

    let devices = instance.devices().expect("failed to get the devices");

    for iterator in devices.iter().enumerate() {
        let index = iterator.0;

        let device = iterator.1;

        let name = device.name();
        let driver_version = device
            .driver_version()
            .expect("failed to get the device driver version");
        let device_type = device.device_type();

        println!(
            "Device {index}:\n\
                \u{20}\u{20}name = {name}\n\
                \u{20}\u{20}driver version = {driver_version}\n\
                \u{20}\u{20}type = {device_type}\n\
                \u{20}\u{20}queue families:"
        );

        for queue_family in device.queue_families() {
            println!(
                "\u{20}\u{20}\u{20}\u{20}queue family #{}:\n\
                    \u{20}\u{20}\u{20}\u{20}\u{20}\u{20}queue count: {}\n\
                    \u{20}\u{20}\u{20}\u{20}\u{20}\u{20}flags: {:?}",
                queue_family.index(),
                queue_family.len(),
                queue_family.flags()
            );
        }

        let extensions = device
            .extensions()
            .expect("failed to get the device extensions");

        println!("\u{20}\u{20}extensions:");

        for extension in extensions {
            print!("\u{20}\u{20}\u{20}\u{20}{extension}");
        }
    }
}
