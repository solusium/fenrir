# 2. Rust as the programming language

Date: 2024-11-09

## Status

Accepted

## Context

We need to choose a programming language for fenrir. Most if not all
applications using a rendering engine have high performance and reliability
requirements on said engine.

## Decision

We choose Rust as the programming language for this project.

## Considerations

Historically, C and C++ dominated the field of computer graphics. The low-level
access to memory and efficient mapping to machine instructions makes C a primary
choice for performance critical applications. C++ builds on top of C with,
amongst other things, zero cost abstraction to allow higher level programming
with object oriented and functional paradigms while preserving the possibility
for efficient code.

Yet, those programming languages come with several tradeoffs. C, requiring
manual memory management and offering no built-in bound checking of memory
access, becomes prone to memory leaks, buffer overflows and other bugs. C++
mitigates those issues via, e.g.,
[RAII](https://en.cppreference.com/w/cpp/language/raii) and
[ranged-based for loops](https://en.cppreference.com/w/cpp/language/range-for).
Still, C++ doesn't prevent the programmer to use unsafe C code. Furthermore,
the standard library allowing for (a lot of) undefined behavior requires strict
and enforced coding guidelines, increasing the mental overhead on the
programmers side. On top of that, due to the age of both languages, they miss
some more quality of life features like a (built-in) package manager and build
system, worsen the developer experience and on occasions increase the
development overhead.

Rust gained signification traction in recent years by addressing the above
mentioned problems while still allowing for high performance and efficient code:

- The
  [borrow checker](https://doc.rust-lang.org/1.8.0/book/references-and-borrowing.html)
  prevents a developer from common oversight errors like memory leaks and data
  races.
- Rust divides itself into the two categories of
  [safe and unsafe](https://doc.rust-lang.org/nomicon/meet-safe-and-unsafe.html).
  Rust defaults to safe Rust, checked by the above mentioned borrow checker and
  avoiding undefined behavior. If for example we need access to C libraries like
  OS APIs with inherent unsafe behaviour, we can revert to
  [unsafe](https://doc.rust-lang.org/nomicon/working-with-unsafe.html) scopes in
  Rust. Marking unsafe code explicitly in Rust offers a great advantage, e.g.
  in code reviews, compared to C and C++, where we need to consider every code
  as unsafe.
- A unified package manager and build system, with a, from personal experience,
  better developer experience. Furthermore, the ability to
  [extend](https://doc.rust-lang.org/book/ch14-05-extending-cargo.html) build
  system with custom commands, e.g. [nextest](https://nexte.st/) for improved
  test integration, improves the experience even further.
- Compatibility with C and, to some extent, with C++ via e.g.
  [cbindgen](https://github.com/mozilla/cbindgen) allows using existing
  libraries, though requiring unsafe Rust as mentioned above. Popular libraries
  have existing bindings, e.g. [ash](https://github.com/ash-rs/ash) for
  Vulkan.

Now, Rust does not come without its own drawbacks:

- Guaranteeing no undefined behavior and other bugs in safe Rust limits the
  expressiveness of the language. E.g. the language limits and discourages the
  use of recursion (non-deterministic ones in the first case and at all in the
  second one).
- The above mentioned limitations, the borrow checker in general and relying
  heavily on functional and data driven paradigms instead of object-oriented
  programming as most mainstream programming languages do results in a steep
  learning curve for developers new to Rust.

Still, the advantages outweigh the disadvantages in our opinion, resulting in
the choice of Rust.

## Consequences

- A steeper learning curve for new contributors not already proficient in Rust
  compared to other programming languages.
- Potential fewer bugs due to the strict type system and compiler checks.
