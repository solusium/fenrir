# 5. async/.await

Date: 2024-11-15

## Status

Accepted

## Context

Modern graphics APIs like Vulkan, Metal, etc. operate asynchronous by nature.
We record commands, commit them via a queue and wait for them via fences and
semaphores. This workflow maps well to the
[async/.await](https://rust-lang.github.io/async-book/01_getting_started/01_chapter.html)
pattern.

## Decision

We design the project around using async/.await.

## Consequences

- As mentioned, async/.await matches well with the workflow of graphics APIs.
- Debugging async code provides a worse experience than synchronous code. For
  example, dead locks tend to break inside runtime code without a trace to the
  user code. Still, the community works on that issue, for example with
  [tokio-console](https://github.com/tokio-rs/console) giving a detailed
  overview of all async tasks and their state.
- Developing runtime agnostic code poses difficulties. We should investigate
  this in the future.
- With
  [4. dynamic loading of components](./0004-dynamic-loading-of-components.md)
  and the potential of futures over
  [FFI](https://doc.rust-lang.org/nomicon/ffi.html) boundaries requires
  libraries like [async-ffi](https://github.com/oxalica/async-ffi).
- Tasks, providing reduced CPU and memory overhead compared to threads and
  available in virtually unlimited quantities, allows us to design our code in a
  modular way and, if using a multithreaded runtime, scales without our
  intervention.
