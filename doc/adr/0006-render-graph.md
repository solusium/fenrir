# 6. Render Graph

Date: 2024-11-20

## Status

Accepted

## Context

Rendering describes a complex process: Allocating resources, ensuring the
correct state and synchronization to name just a some examples. An abstraction
might simplify the user experience.

## Decision

We design the project around a
[render graph](https://www.gdcvault.com/play/1024612/FrameGraph-Extensible-Rendering-Architecture-in).

## Considerations

We want an abstraction that manages the "boring" (and error prone) parts of the
rendering process like synchronization, memory management and image layer
transition while keeping the "interesting" parts like pipelines, textures and
buffers in the users responsibility. Render graphs provide an abstraction which
seems to hit that balance.

## Consequences

- Reduces the potential for bugs in user code.
- Initial development of a render graph might result in overhead.
- Ahead of time knowledge of the rendering process allows for optimization.
- Render graph provide a good visualization of the rendering process where we
  could build tools around.
