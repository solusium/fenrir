# 4. dynamic loading of components

Date: 2024-11-15

## Status

Accepted

## Context

A library abstracting the 3D graphics rendering process consists of different
components:

- An abstraction over the graphics API.
- An allocator, either self-implemented or using a library.
- A shader compiler.
- An API for users of the library.

With well-considered interfaces, components don't need to know about each other.
For example, the graphics API abstraction implements some traits and the user
API uses those traits. Neither component interacts directly with the other.
Rust, at the time of writing, compiles solely into static libraries or
executables, containing all dependencies. We might wish to avoid leaking
dependencies into unrelated components.

## Decision

We design the project around dynamic loading/runtime linking of components.

## Consequences

- Enforces well-considered interfaces/traits without leaking implementation
  details or dependencies to other components.
- Typical, software supporting dynamic loading of components store those in
  dynamic libraries. Objects used by other parts of a library. No stable ABI in
  Rust requires to fall back to the C ABI `#[repr(C)]` with all its
  restrictions. Libraries like
  [libloading](https://github.com/nagisa/rust_libloading/),
  [abi_stable](https://github.com/rodrimati1992/abi_stable_crates/) and
  [stabby](https://github.com/ZettaScaleLabs/stabby) provide tools for dynamic
  loading of libraries.
- Dynamic loading adds requirements to the library like knowing the location of
  loadable libraries, adding complexity and sources of bugs to the project.
- We should still consider supporting both dynamic loading and static linking in
  the future.
- Dynamic loading should improve the addition of new features in the future.
