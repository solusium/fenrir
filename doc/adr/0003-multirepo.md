# 3. Multirepo

Date: 2024-11-13

## Status

Accepted

## Context

Most projects start as a single repository containing all the code. As the
project grows, distinct components of the codebase manifest and the question of
keeping the code in one single or splitting up into different repositories
emerges.

## Decision

We split the codebase into different repositories.

## Consequences

- Reduced scope of each repository, allowing for an easier overview of the
  content of a repository. Furthermore, we limit the effect of changes to the
  required repositories.
- Added complexity and overhead to the workflow. Requiring a CI/CD for each
  repository and syncing repositories on overarching changes.
- Leads more well considered interfaces and contracts between repositories,
  reducing coupling of components.
