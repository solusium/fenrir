# 7. Pipeline Pattern

Date: 2024-11-22

## Status

Accepted

## Context

With the growth of the project, we see the need arise for an architecture
pattern to structure the software while maintaining performance requirements.

## Decision

We design the project around the
[pipeline pattern](<https://en.wikipedia.org/wiki/Pipeline_(software)>).

## Consequences

- We consider to integrate [7. Pipeline Pattern](./0007-pipeline-pattern.md)
  heavily into the pipeline.
- We divide the rendering process into a series of parallel executable stages,
  allowing for an increased throughput.
- With parallel execution, we need to consider double buffering and
  synchronization, for example when we read a resource in one stage and write to
  it in another stage.
- With [5. async/.await](./0005-async-await.md) we hope that the user doesn't
  need to know about the details of the pipeline.
