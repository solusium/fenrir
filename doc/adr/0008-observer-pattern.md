# 8. Observer Pattern

Date: 2024-11-22

## Status

Accepted

## Context

Even single components of the project have different responsibilities. Managing
those tasks in a monolithic way won't scale. For example, the iterative process
of adding features might slow down when the library grows.

## Decision

We design the project around the
[observer pattern](https://en.wikipedia.org/wiki/Observer_pattern).

## Consequences

- We propose to build on top of our implementation of the
  [7. Pipeline Pattern](./0007-pipeline-pattern.md): Each stage provides a
  subject which notifies all observers on each iteration of said stage. Those
  observers provide a customization point to implement the features required by
  this project in a standardized way.
- [5. async/.await](./0005-async-await.md) requires our implementation of the
  observer pattern to support async/.await.
